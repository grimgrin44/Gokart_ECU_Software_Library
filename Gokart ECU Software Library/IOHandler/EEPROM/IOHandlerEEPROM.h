/**
 * @file	IOHandlerEEPROM.h
 * @authors	Gabor Kormendy
 * @date	2015.06.09.
 * @brief	This file contains function and variable declarations for the EEPROM module.
 */

#ifndef IOHANDLER_EEPROM_IOHANDLEREEPROM_H_
#define IOHANDLER_EEPROM_IOHANDLEREEPROM_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------


#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include "inc/hw_can.h"
#include "inc/hw_ints.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "inc/hw_eeprom.h"

#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom_map.h"
#include "driverlib/interrupt.h"
#include "driverlib/eeprom.h"

#include "../../ErrorHandler/ErrorHandler.h"

#include "../../ECUConfiguration.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

#define IOHANDLER_EEPROM_PERIPHERAL		ECU_CONFIGURATION_PERIHP_EEPROM

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

/**
 * @brief	Structure for reading and writing EEPROM data
 */
typedef struct IOHandlerEEPROMData {
	/// Data address in the EEPROM
	uint32_t iAddress_u32;
	/// Data length
	uint32_t iLength_u32;
	/// Data buffer, must point to at least iLength_u32 bytes of available memory
	uint32_t* iData_pu32;
} IOHandlerEEPROMData;

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------

void ioHandlerInitEEPROM(void);
int32_t ioHandlerEEPROMReadData(IOHandlerEEPROMData* pData_pst);
int32_t ioHandlerEEPROMWriteData(IOHandlerEEPROMData* pData_pst);
int32_t ioHandlerEEPROMErase(void);
uint32_t ioHandlerEEPROMGetSize(void);
void ioHandlerOnEEPROMInterrupt(void);



#endif /* IOHANDLER_EEPROM_IOHANDLEREEPROM_H_ */
