/**
 * @file	IOHandlerEEPROM.c
 * @authors	Gabor Kormendy
 * @date	2015.06.09.
 * @brief	This file contains function and variable definitions for EEPROM module.
 */


// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include "IOHandlerEEPROM.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Variable definitions.
// ----------------------------------------------------------------------------

static bool sgEEPROMInitialized_b = false;
static uint32_t sgEEPROMSize_u32 = 0;

// ----------------------------------------------------------------------------
// Function definitions.
// ----------------------------------------------------------------------------

/**
 * @brief	This function initializes the EEPROM module and enables the error
 * 			interrupt.
 *
 * @return	none
 */
void ioHandlerInitEEPROM(void) {
	// Return value of the EEPROM functions
	uint32_t lReturnValue_u32 = 0;
	SysCtlPeripheralEnable(IOHANDLER_EEPROM_PERIPHERAL);
	lReturnValue_u32 = EEPROMInit();
	if (EEPROM_INIT_OK == lReturnValue_u32) {
		sgEEPROMInitialized_b = true;
		sgEEPROMSize_u32 = EEPROMSizeGet();
		//EEPROMIntEnable(EEPROM_EEINT_INT);
	} else {
		sgEEPROMInitialized_b = false;
	}
}

/**
 * @brief	This function reads data from the EEPROM.
 *
 * @param	pData_pst	Structure with the data buffer, the EEPROM address and byte count
 *
 * @return	It returns 0 on succes or with an error code. (Note: it always returns 0 if the EEPROM initialized successfully!)
 */
int32_t ioHandlerEEPROMReadData(IOHandlerEEPROMData* pData_pst) {
	// Check EEPROM initialization
	if(false == sgEEPROMInitialized_b) {
		return ERROR_HANDLER_EEPROM_UNINITIALIZED;
	}

	if(pData_pst->iAddress_u32 > EEPROMSizeGet()) {
		return -1;
	}

	// Read data
	EEPROMRead(pData_pst->iData_pu32, pData_pst->iAddress_u32, pData_pst->iLength_u32);
	return 0;
}

/**
 * @brief	This function writes data to the EEPROM
 *
 * @param	pData_pst	Structure with the data buffer, the EEPROM address and byte count
 *
 * @return	It returns 0 on succes or with an error code
 */
int32_t ioHandlerEEPROMWriteData(IOHandlerEEPROMData* pData_pst) {
	// Return value of the EEPROM function
	uint32_t lReturnValue_u32 = 0;

	if(false == sgEEPROMInitialized_b) {
		return ERROR_HANDLER_EEPROM_UNINITIALIZED;
	}

	// Write data into EEPROM
	lReturnValue_u32 = EEPROMProgram(pData_pst->iData_pu32, pData_pst->iAddress_u32, pData_pst->iLength_u32);
	if (0 != lReturnValue_u32) {
		return ERROR_HANDLER_EEPROM_WRITE_ERROR;
	}
	return lReturnValue_u32;
}

/**
 * @brief	This function erases the entire EEPROM
 *
 * @return	It returns 0 on success or with an error code
 */
int32_t ioHandlerEEPROMErase(void) {
	// Return value of the EEPROM function
	uint32_t lReturnValue_u32 = 0;

	lReturnValue_u32 = EEPROMMassErase();

	if(0 == lReturnValue_u32) {
		return NULL;
	} else {
		return ERROR_HANDLER_EEPROM_ERASE_ERROR;
	}
}

/**
 * @brief	This function returns with the EEPROM memory size
 *
 * @return	The EEPROM memory size in bytes
 */
uint32_t ioHandlerEEPROMGetSize(void) {
	return sgEEPROMSize_u32;
}

/**
 * @brief	This function handles the EEPROM interrupt. It must be called inside the interrupt handler.
 *
 * @return	none
 */
void ioHandlerOnEEPROMInterrupt(void) {

}
