/**
 * @file	IOScheduler.c
 * @authors	Gabor Kormendy
 * @date	2016 jan. 25
 * @brief	
 */

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include "IOScheduler.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

/// The IO scheduler task list
static IOSchedulerTask sgIOSchedulerTasks_pst[IOHANDLER_IOSCHEDULER_TASK_LIST_SIZE];

// ----------------------------------------------------------------------------
// Function definitions.
// ----------------------------------------------------------------------------

void ioHandlerInitIOScheduler(void) {

#if (ECU_CONFIGURATION_ENABLE_CAN == TRUE)
	ioHandlerIOSchedulerAddIOTask(ioHandlerCANHandleRxMessages, 0xFFFFFFFF);
	ioHandlerIOSchedulerAddIOTask(ioHandlerCANHandleTxMessages, 0x00000001);
#endif

#if (ECU_CONFIGURATION_ENABLE_ANALOG == TRUE)
	ioHandlerIOSchedulerAddIOTask(ioHandlerAnalogBeginConversion, 0x55555555);
#endif

}

/**
 * @brief	This function handles the IO tasks
 *
 * @param	pCurrentMask_cu32	The current scheduler mask
 *
 * @return	none
 */
void ioHandlerIOSchedulerRun(const uint32_t pCurrentMask_cu32) {
	uint32_t lIndex_u32 = 0;
	for(lIndex_u32 = 0; lIndex_u32 < IOHANDLER_IOSCHEDULER_TASK_LIST_SIZE; lIndex_u32++) {
		if(sgIOSchedulerTasks_pst[lIndex_u32].iMask_u32 & pCurrentMask_cu32) {
			__ASSERT(NULL == sgIOSchedulerTasks_pst[lIndex_u32].iIOSchedulerTask_pf);
			sgIOSchedulerTasks_pst[lIndex_u32].iIOSchedulerTask_pf();
		}
	}
}


int32_t ioHandlerIOSchedulerAddIOTask(IOSchedulerTaskFunction pIOTask_pf, uint32_t pMask_u32) {
	static uint32_t slIndex_u32 = 0;

	if(slIndex_u32 < IOHANDLER_IOSCHEDULER_TASK_LIST_SIZE) {
		sgIOSchedulerTasks_pst[slIndex_u32].iIOSchedulerTask_pf = pIOTask_pf;
		sgIOSchedulerTasks_pst[slIndex_u32++].iMask_u32 = pMask_u32;
		return 0;
	}

	return -1;
}



