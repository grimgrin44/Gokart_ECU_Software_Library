/**
 * @file	IOScheduler.h
 * @authors	Grimgrin
 * @date	2016 jan. 25
 * @brief	
 */
#ifndef IOHANDLER_IOSCHEDULER_IOSCHEDULER_H_
#define IOHANDLER_IOSCHEDULER_IOSCHEDULER_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "../CAN/IOHandlerCAN.h"
#include "../UART/IOHandlerUART.h"
#include "../Analog/IOHandlerAnalog.h"

#include "../../ECUConfiguration.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

#define IOHANDLER_IOSCHEDULER_TASK_LIST_SIZE		16

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

typedef void (*IOSchedulerTaskFunction)(void);

typedef struct IOSchedulerTask {
	/// The IO task mask
	uint32_t iMask_u32;
	/// The IO task function
	IOSchedulerTaskFunction iIOSchedulerTask_pf;
} IOSchedulerTask;

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------

GOKART_API	void	ioHandlerInitIOScheduler(void);
GOKART_API	int32_t	ioHandlerIOSchedulerAddIOTask(IOSchedulerTaskFunction pIOTask_pf, uint32_t pMask_u32);
GOKART_API	void	ioHandlerIOSchedulerRun(const uint32_t pCurrentMask_cu32);


#endif /* IOHANDLER_IOSCHEDULER_IOSCHEDULER_H_ */
