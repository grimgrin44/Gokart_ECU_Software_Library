/**
 * File: 			IOHandlerTmr.h
 * File Version:	1.0
 * Project: 		GoKart
 * Project Version: 1.0
 * Folder: 			IOHandler/TMR
 * Creators:			-
 * Last Mod. Date:	2015.05.12.
 * Reviewers:		-
 * Last Rev. Date:	-
 *
 * Contains the function and variable definitions for the timer functions.
 */

#ifndef IOHANDLER_GENERALTIMER_IOHANDLERTIMER_H_
#define IOHANDLER_GENERALTIMER_IOHANDLERTIMER_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include "../../ErrorHandler/ErrorHandler.h"

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_timer.h"
#include "inc/hw_ints.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"

#include "../../ECUConfiguration.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

/// The timer peripheral
#define IOHANDLER_TIMER_PERIPHERAL		ECU_CONFIGURATION_TIMER_PERIPHERAL
/// The base address of the timer
#define IOHANDLER_TIMER_BASE			ECU_CONFIGURATION_TIMER_BASE
/// The timer submodule base address
#define IOHANDLER_TIMER_SUBMODULE_BASE	ECU_CONFIGURATION_TIMER_SUBMODULE_BASE
/// The interrupt of the timer
#define IOHANDLER_TIMER_INTERRUPT		ECU_CONFIGURATION_TIMER_INTERRUPT
/// The divider of the system clock
#define IOHANDLER_TIMER_DIVIDER			ECU_CONFIGURATION_TIMER_DIVIDER
/// The maximum number of active timer tasks
#define IOHANDLER_TIMER_MAX_TASK_COUNT	ECU_CONFIGURATION_TIMER_MAX_TASK_COUNT

/// Return value to disable the timer task
#define IOHANDLER_TIMER_DISABLE_TASK	0
/// Return value to keep alive the timer task
#define IOHANDLER_TIMER_KEEP_ALIVE		1


// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------


typedef uint32_t (*TimerTask)(void);

typedef struct IOHandlerTimerTask {
	/// Signs that the task active or not
	bool iActive_b;
	/// Milliseconds counter
	uint32_t iMilliseconds_u32;
	/// Reload value of the milliseconds counter
	uint32_t iCounter_u32;
	/// Function pointer to the timer function
	TimerTask iTimerTask_pf;
} IOHandlerTimerTask;

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------

void ioHandlerInitTimer(void);
int32_t ioHandlerTimerRegisterTask(TimerTask pTimerTask_pf, uint32_t pMilliseconds_u32);
int32_t ioHandlerTimerKillTask(uint16_t pTaskId_u16);
//void ioHandlerOnTimerInterrupt(void);


#endif /* IOHANDLER_GENERALTIMER_IOHANDLERTIMER_H_ */
