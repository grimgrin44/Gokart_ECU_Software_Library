/**
 * @file	IOHandlerTimer.c
 * Contains the function and variable declarations for the
 * system part of the IO handler functions.
 */

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------
#include "IOHandlerTimer.h"

#include "../../ECUConfiguration.h"

// ----------------------------------------------------------------------------
// Variable definitions.
// ----------------------------------------------------------------------------

/// Array for timer tasks
static IOHandlerTimerTask sgTimerTaskPool_pst[IOHANDLER_TIMER_MAX_TASK_COUNT];
/// Counter of the active tasks
static uint32_t sgActiveTasks_u32 = 0;

// ----------------------------------------------------------------------------
// Function definitions.
// ----------------------------------------------------------------------------

/**
 * @brief	The timer interrupt handler. It must be called in the interrupt routine.
 *
 * @return	none
 */
static void ioHandlerOnTimerInterrupt(void) {
	// The index variable
	uint32_t lIndex_u32 = 0;
	// The task return variable
	uint32_t lReturnValue_u32 = 0;
	// Interrupt status
	uint32_t lInterruptStatus_u32 = 0;

	lInterruptStatus_u32 = TimerIntStatus(IOHANDLER_TIMER_BASE, true);
	TimerIntClear(IOHANDLER_TIMER_BASE, lInterruptStatus_u32);
	TimerIntClear(IOHANDLER_TIMER_BASE, TIMER_TIMA_TIMEOUT);

	if (0 == sgActiveTasks_u32) {
		// Optimalization :)
		return;
	}

	for (lIndex_u32 = 0; lIndex_u32 < IOHANDLER_TIMER_MAX_TASK_COUNT; lIndex_u32++) {
		if (true == sgTimerTaskPool_pst[lIndex_u32].iActive_b) {

			if (0 == sgTimerTaskPool_pst[lIndex_u32].iCounter_u32) {
				lReturnValue_u32 = sgTimerTaskPool_pst[lIndex_u32].iTimerTask_pf();
				if (IOHANDLER_TIMER_KEEP_ALIVE != lReturnValue_u32) {
					sgTimerTaskPool_pst[lIndex_u32].iActive_b = false;
					sgActiveTasks_u32--;
				} else {
					sgTimerTaskPool_pst[lIndex_u32].iCounter_u32 = sgTimerTaskPool_pst[lIndex_u32].iMilliseconds_u32;
				}

			} else {
				sgTimerTaskPool_pst[lIndex_u32].iCounter_u32--;
			}
		}
	}
}

/**
 * @brief	This function initializes the general timer for small tasks.
 *
 * @return	none
 */
void ioHandlerInitTimer(void) {
	// Enable the Timer0.
	SysCtlPeripheralEnable(IOHANDLER_TIMER_PERIPHERAL);

	// Configure Timer as a 16-bit periodic timer.
	TimerConfigure(IOHANDLER_TIMER_BASE, TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PERIODIC);

	// Set the Timer load value.
	TimerLoadSet(IOHANDLER_TIMER_BASE, IOHANDLER_TIMER_SUBMODULE_BASE, (SysCtlClockGet() / IOHANDLER_TIMER_DIVIDER) / 2);

	// Set the Timer prescaler
	TimerPrescaleSet(IOHANDLER_TIMER_BASE, IOHANDLER_TIMER_SUBMODULE_BASE, 4);

	// Configure the Timer interrupt for timer timeout.
	TimerIntEnable(IOHANDLER_TIMER_BASE, TIMER_TIMA_TIMEOUT);

	// Enable the Timer interrupt on the processor (NVIC).
	IntEnable(IOHANDLER_TIMER_INTERRUPT);

#if (ECU_CONFIGURATION_ENABLE_INT_HANDLER_REGISTRATION == TRUE)
	TimerIntRegister(IOHANDLER_TIMER_BASE, IOHANDLER_TIMER_SUBMODULE_BASE, ioHandlerOnTimerInterrupt);
#endif

	// Enable Timer.
	TimerEnable(IOHANDLER_TIMER_BASE, IOHANDLER_TIMER_SUBMODULE_BASE);

}

/**
 * @brief	This function registers a small timer task
 *
 * @param	pTimerTask_pf		Function pointer to the timer task function
 * @param	pMilliseconds_u32	The delay in milliseconds before the interrupt handler calls the timer task function
 *
 * @return	It returns the timer task id on success or with an error code
 */
int32_t ioHandlerTimerRegisterTask(TimerTask pTimerTask_pf, uint32_t pMilliseconds_u32) {
	// The index variable
	uint32_t lIndex_u32 = 0;

	if (NULL == pTimerTask_pf) {
		return ERROR_HANDLER_TIMER_TASK_FUNCTION_IS_NULL;
	}

	for (lIndex_u32 = 0; lIndex_u32 < IOHANDLER_TIMER_MAX_TASK_COUNT; lIndex_u32++) {
		if (false == sgTimerTaskPool_pst[lIndex_u32].iActive_b) {
			sgTimerTaskPool_pst[lIndex_u32].iMilliseconds_u32 = pMilliseconds_u32;
			sgTimerTaskPool_pst[lIndex_u32].iCounter_u32 = pMilliseconds_u32;
			sgTimerTaskPool_pst[lIndex_u32].iTimerTask_pf = pTimerTask_pf;
			sgTimerTaskPool_pst[lIndex_u32].iActive_b = true;
			sgActiveTasks_u32++;
			return (int32_t) lIndex_u32;
		}
	}

	return ERROR_HANDLER_TIMER_TASK_POOL_IS_FULL;
}


/**
 * @brief	This function kills the timer task
 *
 * @param	pTaskId_u16
 *
 * @return	It returns 0 on success or with an error code
 */
int32_t ioHandlerTimerKillTask(uint16_t pTaskId_u16) {
	if(pTaskId_u16 < IOHANDLER_TIMER_MAX_TASK_COUNT && sgTimerTaskPool_pst[pTaskId_u16].iActive_b == true) {
		sgTimerTaskPool_pst[pTaskId_u16].iActive_b = false;
		return 0;
	}
	return -1;
}

