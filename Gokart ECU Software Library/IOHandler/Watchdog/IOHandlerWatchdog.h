/**
 * @file	IOHandlerWatchdog.h
 * @author	Gabor Kormendy
 * @version	1.0
 * @date	2015.05.24.
 * @brief	Contains variable and function declaration for the Watchdog module.
 */

#ifndef IOHANDLER_WATCHDOG_IOHANDLERWATCHDOG_H_
#define IOHANDLER_WATCHDOG_IOHANDLERWATCHDOG_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include "../Systick/IOHandlerSystick.h"
#include "../EEPROM/IOHandlerEEPROM.h"

#include "../../ECUConfiguration.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

/// The Watchdog peripheral
#define IOHANDLER_PERIPH_WATCHDOG				ECU_CONFIGURATION_PERIPH_WATCHDOG
/// The base address of the Watchdog module
#define IOHANDLER_WATCHDOG_BASE					ECU_CONFIGURATION_WATCHDOG_BASE

#define IOHANDLER_WATCHDOG_INTERRUPT			ECU_CONFIGURATION_WATCHDOG_INTERRUPT
/// Reload value at the first timeout
#define IOHANDLER_WATCHDOG_LOAD_VALUE			ECU_CONFIGURATION_WATCHDOG_LOAD_VALUE
/// Enables the logging into the EEPROM
#define IOHANDLER_WATCHDOG_LOG_EEPROM			ECU_CONFIGURATION_WATCHDOG_LOG_EEPROM

#define IOHANDLER_WATCHDOG_LOG_EEPROM_ADDR 		ECU_CONFIGURATION_WATCHDOG_LOG_EEPROM_ADDR
/// Enables the Watchdog clear in the scheduler
#define IOHANDLER_WATCHDOG_SCHEDULER_CLEAR		ECU_CONFIGURATION_WATCHDOG_SCHEDULER_CLEAR

#define IOHANDLER_WATCHDOG_SCHEDULER_TASK_ID	ECU_CONFIGURATION_WATCHDOG_SCHEDULER_TASK_ID

#define IOHANDLER_WATCHDOG_SCHEDULER_MASK		ECU_CONFIGURATION_WATCHDOG_SCHEDULER_MASK
/// Enables the debug mode (watchdog will stop with the CPU)
#define IOHANDLER_WATCHDOG_DEBUG_MODE_ENABLED	ECU_CONFIGURATION_WATCHDOG_DEBUG_MODE_ENABLED
/// Enables the reset generation
#define IOHANDLER_WATCHDOG_RESET_ENABLED		ECU_CONFIGURATION_WATCHDOG_RESET_ENABLED

#if (IOHANDLER_WATCHDOG_SCHEDULER_CLEAR == TRUE)
#define IOHANDLER_WATCHDOG_CLEAR_TASK	IOHANDLER_SYSTICK_TASK_LIST_ENTRY(IOHANDLER_WATCHDOG_SCHEDULER_TASK_ID, ioHandlerWatchdogClear, 0x001, true)
#endif

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

typedef struct IOHandlerWatchdogLogData {
	uint32_t iActiveTaskId_u32;
	uint32_t iSchedulerMask_u32;
	uint64_t iLifeTime_u64;
} IOHandlerWatchdogLogData;

// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------

GOKART_API	void 	ioHandlerInitWatchdog(void);
GOKART_API	void 	ioHandlerWatchdogEnable(void);
GOKART_API	void 	ioHandlerWatchdogClear(void);
GOKART_API	void 	ioHandlerWatchdogClearLog(void);
GOKART_API	void 	ioHandlerOnWatchdogFirstTimeout(void);





#endif /* IOHANDLER_WATCHDOG_IOHANDLERWATCHDOG_H_ */
