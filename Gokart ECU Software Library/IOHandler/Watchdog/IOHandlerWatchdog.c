/*
 * IOHandlerWatchdog.c
 *
 *  Created on: 2015 okt. 19
 *      Author: Grimgrin
 */

#include "IOHandlerWatchdog.h"

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_watchdog.h"
#include "inc/hw_ints.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/watchdog.h"

void ioHandlerInitWatchdog(void) {
	SysCtlPeripheralEnable(IOHANDLER_PERIPH_WATCHDOG);
	if (true == WatchdogLockState(IOHANDLER_WATCHDOG_BASE)) {
		WatchdogUnlock(IOHANDLER_WATCHDOG_BASE);
	}

	WatchdogReloadSet(IOHANDLER_WATCHDOG_BASE, IOHANDLER_WATCHDOG_LOAD_VALUE);

#if (IOHANDLER_WATCHDOG_RESET_ENABLED == TRUE)
	WatchdogResetEnable(IOHANDLER_WATCHDOG_BASE);
#else
	WatchdogResetDisable(IOHANDLER_WATCHDOG_BASE);
#endif

#if (IOHANDLER_WATCHDOG_DEBUG_MODE_ENABLED == TRUE)
	WatchdogStallEnable(IOHANDLER_WATCHDOG_BASE);
#else
	WatchdogStallDisable(IOHANDLER_WATCHDOG_BASE);
#endif
}

void ioHandlerWatchdogEnable(void) {
	if (true == WatchdogLockState(IOHANDLER_WATCHDOG_BASE)) {
		WatchdogUnlock(IOHANDLER_WATCHDOG_BASE);
	}
	WatchdogEnable(IOHANDLER_WATCHDOG_BASE);
	WatchdogIntEnable(IOHANDLER_WATCHDOG_BASE);
	IntEnable(IOHANDLER_WATCHDOG_INTERRUPT);
}

void ioHandlerWatchdogClear(void) {
	uint32_t lIntStatus_u32 = 0;
	lIntStatus_u32 = WatchdogIntStatus(IOHANDLER_WATCHDOG_BASE, true);
	if (true == WatchdogLockState(IOHANDLER_WATCHDOG_BASE)) {
		WatchdogUnlock(IOHANDLER_WATCHDOG_BASE);
	}
	WatchdogIntClear(lIntStatus_u32);
	WatchdogReloadSet(IOHANDLER_WATCHDOG_BASE, IOHANDLER_WATCHDOG_LOAD_VALUE);
	WatchdogLock(IOHANDLER_WATCHDOG_BASE);
}

void ioHandlerOnWatchdogFirstTimeout(void) {
	IOHandlerWatchdogLogData lLogData_st = { 0 };
	IOHandlerEEPROMData lEEPROMData_st = { 0 };

	WatchdogIntClear(IOHANDLER_WATCHDOG_BASE);

	lLogData_st.iActiveTaskId_u32 = ioHandlerSystickGetActiveTaskId();
	lLogData_st.iSchedulerMask_u32 = ioHandlerSystickGetMask();
	lLogData_st.iLifeTime_u64 = ioHandlerSystickGetLifetime();

	lEEPROMData_st.iData_pu32 = (uint32_t*) &lLogData_st;
	lEEPROMData_st.iAddress_u32 = IOHANDLER_WATCHDOG_LOG_EEPROM_ADDR;
	lEEPROMData_st.iLength_u32 = sizeof(IOHandlerWatchdogLogData);

	ioHandlerEEPROMWriteData(&lEEPROMData_st);
}

void ioHandlerWatchdogClearLog(void) {
	IOHandlerWatchdogLogData lLogData_st = { 0 };
	IOHandlerEEPROMData lEEPROMData_st = { 0 };

	lEEPROMData_st.iData_pu32 = (uint32_t*) &lLogData_st;
	lEEPROMData_st.iAddress_u32 = IOHANDLER_WATCHDOG_LOG_EEPROM_ADDR;
	lEEPROMData_st.iLength_u32 = sizeof(IOHandlerWatchdogLogData);

	ioHandlerEEPROMWriteData(&lEEPROMData_st);
}
