/**
 * @file	IOHandlerSPI.c
 * @author	Gabor Kormendy
 * @version	1.0
 */

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------
#include "IOHandlerSPI.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Variable definitions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Function definitions.
// ----------------------------------------------------------------------------

void ioHandlerInitSPI(void) {
	// Enable peripherals
	SysCtlPeripheralEnable(IOHANDLER_SPI_PERIPHERAL_BASE);
	SysCtlPeripheralEnable(IOHANDLER_SPI_PERIPH_GPIO_RX);
	SysCtlPeripheralEnable(IOHANDLER_SPI_PERIPH_GPIO_TX);
	SysCtlPeripheralEnable(IOHANDLER_SPI_PERIPH_GPIO_SCK);
	// Configure ports
	GPIOPinTypeSSI(IOHANDLER_SPI_GPIO_PORT_BASE_RX, IOHANDLER_SPI_GPIO_PIN_RX);
	GPIOPinTypeSSI(IOHANDLER_SPI_GPIO_PORT_BASE_TX, IOHANDLER_SPI_GPIO_PIN_TX);
	GPIOPinTypeSSI(IOHANDLER_SPI_GPIO_PORT_BASE_SCK, IOHANDLER_SPI_GPIO_PIN_SCK);
	// Pin mux
	GPIOPinConfigure(IOHANDLER_SPI_GPIO_RX_CONFIG);
	GPIOPinConfigure(IOHANDLER_SPI_GPIO_TX_CONFIG);
	GPIOPinConfigure(IOHANDLER_SPI_GPIO_SCK_CONFIG);
	// Configure SPI module
	SSIConfigSetExpClk(IOHANDLER_SPI_BASE, SysCtlClockGet(), IOHANDLER_SPI_PROTOCOL, IOHANDLER_SPI_MODE, IOHANDLER_SPI_BITRATE, IOHANDLER_SPI_DATA_WIDTH);
	// Enable SPI module
	SSIEnable(IOHANDLER_SPI_BASE);
}

int32_t ioHandlerSPIExecuteTransfer(IOHandlerSPITransfer* pTransfer_pst) {
	// Index variable
	uint32_t lIndex_u32 = 0;
	// Data to be sent and read
	uint32_t lData_u32 = 0;
	// Safety counter
	uint32_t lCounter_u32 = 0;
	// Return value
	int32_t lReturnValue_i32 = 0;

	// Drop null transfers
	if (NULL == pTransfer_pst) {
		return -1;
	}

	// Select slave
	pTransfer_pst->iSelector_pf(true);

	for (lIndex_u32 = 0; lIndex_u32 < pTransfer_pst->iDLC_u32; lIndex_u32++) {
		lData_u32 = pTransfer_pst->iTxData_pu8[lIndex_u32];
		SSIDataPut(IOHANDLER_SPI_BASE, lData_u32);
		lCounter_u32 = 0xFFFF;
		while (SSIBusy(IOHANDLER_SPI_BASE) & lCounter_u32 != 0) {
			// Waiting for transmission complete
			lCounter_u32--;
		}
		if(true == SSIBusy(IOHANDLER_SPI_BASE)) {
			// something went wrong
			lReturnValue_i32 = -1;
			break;
		}
		SSIDataGet(IOHANDLER_SPI_BASE, &lData_u32);
		if (false == pTransfer_pst->iDiscardRxData_b) {
			pTransfer_pst->iRxData_pu8[lIndex_u32] = lData_u32;
		}
	}

	// Release slave
	pTransfer_pst->iSelector_pf(false);

	return lReturnValue_i32;
}

