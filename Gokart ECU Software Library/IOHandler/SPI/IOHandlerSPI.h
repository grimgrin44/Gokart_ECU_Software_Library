/**
 * File: 			IOHandlerSPI.h
 * File Version:	1.0
 * Project: 		GoKart
 * Project Version: 1.0
 * Folder: 			IOHandler/SPI
 * Creators:		-
 * Last Mod. Date:	2015.05.18.
 * Reviewers:		-
 * Last Rev. Date:	-
 *
 * Contains the function and variable declaration for the low-level
 * SPI I/O functions. At the moment it only supports SPI0 and SPI1
 */

#ifndef IOHANDLER_SPI_IOHANDLERSPI_H_
#define IOHANDLER_SPI_IOHANDLERSPI_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_can.h"
#include "inc/hw_ints.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom_map.h"
#include "driverlib/gpio.h"
#include "driverlib/ssi.h"
#include "driverlib/interrupt.h"

#include "../IOHandlerDefs.h"
#include "../../ErrorHandler/ErrorHandler.h"

#include "../../ECUConfiguration.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

#define IOHANDLER_SPI_BUFFERED				FALSE

/// Base address of the SPI module
#define IOHANDLER_SPI_BASE					ECU_CONFIGURATION_SPI_BASE
/// Peripheral base of the SPI module
#define IOHANDLER_SPI_PERIPHERAL_BASE		ECU_CONFIGURATION_SPI_PERIPHERAL_BASE

/// SPI receive pin port peripheral
#define IOHANDLER_SPI_PERIPH_GPIO_RX		ECU_CONFIGURATION_SPI_PERIPH_GPIO_RX
/// SPI transmit pin port peripheral
#define IOHANDLER_SPI_PERIPH_GPIO_TX		ECU_CONFIGURATION_SPI_PERIPH_GPIO_TX
/// SPI clock pin port peripheral
#define IOHANDLER_SPI_PERIPH_GPIO_SCK		ECU_CONFIGURATION_SPI_PERIPH_GPIO_SCK

/// SPI receive pin port base
#define IOHANDLER_SPI_GPIO_PORT_BASE_RX		ECU_CONFIGURATION_SPI_GPIO_PORT_BASE_RX
/// SPI transmit pin port base
#define IOHANDLER_SPI_GPIO_PORT_BASE_TX		ECU_CONFIGURATION_SPI_GPIO_PORT_BASE_TX
/// SPI clock pin port base
#define IOHANDLER_SPI_GPIO_PORT_BASE_SCK	ECU_CONFIGURATION_SPI_GPIO_PORT_BASE_SCK

/// SPI receive pin port config
#define IOHANDLER_SPI_GPIO_RX_CONFIG		ECU_CONFIGURATION_SPI_GPIO_RX_CONFIG
/// SPI transmit pin port config
#define IOHANDLER_SPI_GPIO_TX_CONFIG		ECU_CONFIGURATION_SPI_GPIO_TX_CONFIG
/// SPI clock pin port config
#define IOHANDLER_SPI_GPIO_SCK_CONFIG		ECU_CONFIGURATION_SPI_GPIO_SCK_CONFIG

/// SPI receive pin number
#define IOHANDLER_SPI_GPIO_PIN_RX			ECU_CONFIGURATION_SPI_GPIO_PIN_RX
/// SPI transmit pin number
#define IOHANDLER_SPI_GPIO_PIN_TX			ECU_CONFIGURATION_SPI_GPIO_PIN_TX
/// SPI clock pin number
#define IOHANDLER_SPI_GPIO_PIN_SCK			ECU_CONFIGURATION_SPI_GPIO_PIN_SCK

/// SPI interrupt
#define IOHANDLER_SPI_INTERRUPT				ECU_CONFIGURATION_SPI_INTERRUPT

/// SPI bitrate
#define IOHANDLER_SPI_BITRATE				ECU_CONFIGURATION_SPI_BITRATE
/// SPI mode
#define IOHANDLER_SPI_MODE					ECU_CONFIGURATION_SPI_MODE
/// SPI protocol
#define IOHANDLER_SPI_PROTOCOL				ECU_CONFIGURATION_SPI_PROTOCOL
/// SPI data width
#define IOHANDLER_SPI_DATA_WIDTH			ECU_CONFIGURATION_SPI_DATA_WIDTH


// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

/// Function pointer type for a SPI device selector function
typedef void (*DeviceSelectorFunction)(bool pSelect_b);

/// SPI Transfer structure
typedef struct IOHandlerSPITransfer {
	/// The data length counter
	uint32_t iDLC_u32;
	/// Buffer for the TX data
	uint8_t* iTxData_pu8;
	/// Buffer for the RX data
	uint8_t* iRxData_pu8;
	/// Discard RX data
	bool iDiscardRxData_b;
	/// Function to select / release device
	DeviceSelectorFunction iSelector_pf;
} IOHandlerSPITransfer;


// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------



// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------



void ioHandlerInitSPI(void);
int32_t ioHandlerSPIExecuteTransfer(IOHandlerSPITransfer* pTransfer_pst);


#endif /* IOHANDLER_SPI_IOHANDLERSPI_H_ */
