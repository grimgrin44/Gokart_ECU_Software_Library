/**
 * @file	IOHandlerAnalog.c
 * @authors	Grimgrin
 * @date	2016 febr. 4
 * @brief	
 */

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include "IOHandlerAnalog.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

#if (ECU_CONFIGURATION_ENABLE_ANALOG == TRUE)

/// The ADC sequencer number
#define IOHANDLER_ANALOG_ADC_SEQUENCER				1
/// The ADC trigger source
#define IOHANDLER_ANALOG_ADC_TRIGGER				ADC_TRIGGER_PROCESSOR
/// The channel number of the internal temp sensor
#define IOHANDLER_ANALOG_TEMP_SENSOR_ADC_CHANNEL	ADC_CTL_TS
/// The reference voltage
#define IOHANDLER_ANALOG_ADC_REF					ADC_REF_INT

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

static uint32_t sgConversionData_pu32[16] = { 0 };

static volatile bool sgConversionFinished_b = true;

// ----------------------------------------------------------------------------
// Function definitions.
// ----------------------------------------------------------------------------

static void ioHandlerAnalogInterruptHandler(void);

void ioHandlerInitAnalog(void) {

	SysCtlPeripheralEnable(IOHANDLER_ANALOG_ADC_MODULE_PERIPHERAL_BASE);

#if (IOHANDLER_ANALOG_INPUT_1_ENABLE == TRUE)
	SysCtlPeripheralEnable(IOHANDLER_ANALOG_INPUT_1_PORT_PERIPH_BASE);
	GPIOPinTypeADC(IOHANDLER_ANALOG_INPUT_1_PORT_BASE, IOHANDLER_ANALOG_INPUT_1_PIN);
#endif

#if (IOHANDLER_ANALOG_INPUT_2_ENABLE == TRUE)
	SysCtlPeripheralEnable(IOHANDLER_ANALOG_INPUT_2_PORT_PERIPH_BASE);
	GPIOPinTypeADC(IOHANDLER_ANALOG_INPUT_2_PORT_BASE, IOHANDLER_ANALOG_INPUT_2_PIN);
#endif

#if (IOHANDLER_ANALOG_INPUT_3_ENABLE == TRUE)
	SysCtlPeripheralEnable(IOHANDLER_ANALOG_INPUT_3_PORT_PERIPH_BASE);
	GPIOPinTypeADC(IOHANDLER_ANALOG_INPUT_3_PORT_BASE, IOHANDLER_ANALOG_INPUT_3_PIN);
#endif

	ADCReferenceSet(IOHANDLER_ANALOG_ADC_MODULE_BASE, IOHANDLER_ANALOG_ADC_REF);

	ADCSequenceConfigure(
			IOHANDLER_ANALOG_ADC_MODULE_BASE,
			IOHANDLER_ANALOG_ADC_SEQUENCER,
			IOHANDLER_ANALOG_ADC_TRIGGER,
			0);

	ADCSequenceStepConfigure(
			IOHANDLER_ANALOG_ADC_MODULE_BASE,
			IOHANDLER_ANALOG_ADC_SEQUENCER,
			0,
			IOHANDLER_ANALOG_INPUT_1_ADC_CHANNEL);

	ADCSequenceStepConfigure(
			IOHANDLER_ANALOG_ADC_MODULE_BASE,
			IOHANDLER_ANALOG_ADC_SEQUENCER,
			1,
			IOHANDLER_ANALOG_INPUT_2_ADC_CHANNEL);

	ADCSequenceStepConfigure(
			IOHANDLER_ANALOG_ADC_MODULE_BASE,
			IOHANDLER_ANALOG_ADC_SEQUENCER,
			2,
			IOHANDLER_ANALOG_INPUT_3_ADC_CHANNEL);

	ADCSequenceStepConfigure(
			IOHANDLER_ANALOG_ADC_MODULE_BASE,
			IOHANDLER_ANALOG_ADC_SEQUENCER,
			3,
			IOHANDLER_ANALOG_TEMP_SENSOR_ADC_CHANNEL | ADC_CTL_END | ADC_CTL_IE);

	ADCSequenceEnable(IOHANDLER_ANALOG_ADC_MODULE_BASE, IOHANDLER_ANALOG_ADC_SEQUENCER);

#if (ECU_CONFIGURATION_ENABLE_INT_HANDLER_REGISTRATION == TRUE)
	ADCIntRegister(IOHANDLER_ANALOG_ADC_MODULE_BASE, IOHANDLER_ANALOG_ADC_SEQUENCER, __INTERRUPT_HANDLER__ioHandlerOnAnalogInterrupt);
#endif

	ADCIntEnable(IOHANDLER_ANALOG_ADC_MODULE_BASE, IOHANDLER_ANALOG_ADC_SEQUENCER);
	ADCIntClear(IOHANDLER_ANALOG_ADC_MODULE_BASE, IOHANDLER_ANALOG_ADC_SEQUENCER);
}

int64_t	ioHandlerOnAnalogInterrupt(void) {
	uint32_t lInterruptStatus_u32 = 0;
	lInterruptStatus_u32 = ADCIntStatus(IOHANDLER_ANALOG_ADC_MODULE_BASE, IOHANDLER_ANALOG_ADC_SEQUENCER, true);

	ADCIntClear(IOHANDLER_ANALOG_ADC_MODULE_BASE, IOHANDLER_ANALOG_ADC_SEQUENCER);
	ADCSequenceDataGet(IOHANDLER_ANALOG_ADC_MODULE_BASE, IOHANDLER_ANALOG_ADC_SEQUENCER, sgConversionData_pu32);

	sgConversionFinished_b = true;

	return lInterruptStatus_u32;
}

void ioHandlerAnalogBeginConversion(void) {
	if(true == sgConversionFinished_b) {
		sgConversionFinished_b = false;
		ADCProcessorTrigger(IOHANDLER_ANALOG_ADC_MODULE_BASE, IOHANDLER_ANALOG_ADC_SEQUENCER);
	}
}

uint32_t ioHandlerAnalogGetValue(IOHandlerAnalogInput pAnalogInput_e) {
	__ASSERT(pAnalogInput_e > NUM_ANALOG_INPUT);
	return sgConversionData_pu32[pAnalogInput_e];
}

void __INTERRUPT_HANDLER__ioHandlerOnAnalogInterrupt(void) {
	ioHandlerOnAnalogInterrupt();
}

#endif
