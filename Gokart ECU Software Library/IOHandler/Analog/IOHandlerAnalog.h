/**
 * @file	IOHandlerAnalog.h
 * @authors	Grimgrin
 * @date	2016 febr. 4
 * @brief	
 */
#ifndef IOHANDLER_ANALOG_IOHANDLERANALOG_H_
#define IOHANDLER_ANALOG_IOHANDLERANALOG_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "driverlib/sysctl.h"
#include "driverlib/adc.h"
#include "driverlib/gpio.h"

#include "../../ECUConfiguration.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

#define IOHANDLER_ANALOG_ADC_MODULE_PERIPHERAL_BASE		ECU_CONFIGURATION_ANALOG_ADC_MODULE_PERIPHERAL_BASE

#define IOHANDLER_ANALOG_ADC_MODULE_BASE				ECU_CONFIGURATION_ANALOG_ADC_MODULE_BASE


#define IOHANDLER_ANALOG_INPUT_1_ENABLE					ECU_CONFIGURATION_ANALOG_INPUT_1_ENABLE

#define IOHANDLER_ANALOG_INPUT_2_ENABLE					ECU_CONFIGURATION_ANALOG_INPUT_2_ENABLE

#define IOHANDLER_ANALOG_INPUT_3_ENABLE					ECU_CONFIGURATION_ANALOG_INPUT_3_ENABLE

#define IOHANDLER_ANALOG_TEMP_SENSOR_ENABLE				ECU_CONFIGURATION_ANALOG_TEMP_SENSOR_ENABLE


#define IOHANDLER_ANALOG_INPUT_1_PORT_PERIPH_BASE		ECU_CONFIGURATION_ANALOG_INPUT_1_PORT_PERIPH_BASE

#define IOHANDLER_ANALOG_INPUT_1_PORT_BASE				ECU_CONFIGURATION_ANALOG_INPUT_1_PORT_BASE

#define IOHANDLER_ANALOG_INPUT_1_PIN					ECU_CONFIGURATION_ANALOG_INPUT_1_PIN

#define IOHANDLER_ANALOG_INPUT_1_ADC_CHANNEL			ECU_CONFIGURATION_ANALOG_INPUT_1_ADC_CHANNEL


#define IOHANDLER_ANALOG_INPUT_2_PORT_PERIPH_BASE		ECU_CONFIGURATION_ANALOG_INPUT_2_PORT_PERIPH_BASE

#define IOHANDLER_ANALOG_INPUT_2_PORT_BASE				ECU_CONFIGURATION_ANALOG_INPUT_2_PORT_BASE

#define IOHANDLER_ANALOG_INPUT_2_PIN					ECU_CONFIGURATION_ANALOG_INPUT_2_PIN

#define IOHANDLER_ANALOG_INPUT_2_ADC_CHANNEL			ECU_CONFIGURATION_ANALOG_INPUT_2_ADC_CHANNEL


#define IOHANDLER_ANALOG_INPUT_3_PORT_PERIPH_BASE		ECU_CONFIGURATION_ANALOG_INPUT_3_PORT_PERIPH_BASE

#define IOHANDLER_ANALOG_INPUT_3_PORT_BASE				ECU_CONFIGURATION_ANALOG_INPUT_3_PORT_BASE

#define IOHANDLER_ANALOG_INPUT_3_PIN					ECU_CONFIGURATION_ANALOG_INPUT_3_PIN

#define IOHANDLER_ANALOG_INPUT_3_ADC_CHANNEL			ECU_CONFIGURATION_ANALOG_INPUT_3_ADC_CHANNEL


// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

/// Analog input enumeration type definition
typedef enum IOHandlerAnalogInput {
	ANALOG_IN_1 = 0,
	ANALOG_IN_2,
	ANALOG_IN_3,
	ANALOG_TEMP_SENSOR,
	NUM_ANALOG_INPUT
} IOHandlerAnalogInput;

/// Analog sequencer enumeration type definition
typedef enum IOHandlerAnalogSequencer {
	ANALOG_SEQ_0 = 0,
	ANALOG_SEQ_1,
	ANALOG_SEQ_2,
	ANALOG_SEQ_3,
	NUM_ANALOG_SEQENCER
} IOHandlerAnalogSequencer;

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------

GOKART_API	void		ioHandlerInitAnalog(void);
GOKART_API	void		ioHandlerAnalogBeginConversion(void);
GOKART_API	uint32_t	ioHandlerAnalogGetValue(IOHandlerAnalogInput pAnalogInput_e);

GOKART_API	void 		__INTERRUPT_HANDLER__ioHandlerOnAnalogInterrupt(void);


#endif /* IOHANDLER_ANALOG_IOHANDLERANALOG_H_ */
