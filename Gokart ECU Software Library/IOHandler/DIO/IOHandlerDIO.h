/**
 * @file	IOHandlerDIO.h
 * @authors	Gabor Kormendy
 * @date	2015.06.08.
 * @brief	This file contains function and variable declarations for the DIO module.
 */

#ifndef IOHANDLER_DIO_IOHANDLERDIO_H_
#define IOHANDLER_DIO_IOHANDLERDIO_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/sysctl.h"

#include "../../ECUConfiguration.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

/// Bitmask for pin 0
#define IOHANDLER_DIO_PIN_0			GPIO_PIN_0
/// Bitmask for pin 1
#define IOHANDLER_DIO_PIN_1			GPIO_PIN_1
/// Bitmask for pin 2
#define IOHANDLER_DIO_PIN_2			GPIO_PIN_2
/// Bitmask for pin 3
#define IOHANDLER_DIO_PIN_3			GPIO_PIN_3
/// Bitmask for pin 4
#define IOHANDLER_DIO_PIN_4			GPIO_PIN_4
/// Bitmask for pin 5
#define IOHANDLER_DIO_PIN_5			GPIO_PIN_5
/// Bitmask for pin 6
#define IOHANDLER_DIO_PIN_6			GPIO_PIN_6
/// Bitmask for pin 7
#define IOHANDLER_DIO_PIN_7			GPIO_PIN_7


/// GPIO A port base address
#define IOHANDLER_DIO_PORTA_BASE	GPIO_PORTA_BASE
/// GPIO B port base address
#define IOHANDLER_DIO_PORTB_BASE	GPIO_PORTB_BASE
/// GPIO C port base address
#define IOHANDLER_DIO_PORTC_BASE	GPIO_PORTC_BASE
/// GPIO D port base address
#define IOHANDLER_DIO_PORTD_BASE	GPIO_PORTD_BASE
/// GPIO E port base address
#define IOHANDLER_DIO_PORTE_BASE	GPIO_PORTE_BASE
/// GPIO F port base address
#define IOHANDLER_DIO_PORTF_BASE	GPIO_PORTF_BASE

/// The DIO output ports array
#define IOHANDLER_DIO_OUTPUT_PORTS					{ PORT_F, PORT_C, PORT_C, PORT_F }
/// The DIO output pin array. This must be sync with the array above.
#define IOHANDLER_DIO_OUTPUT_PINS					{ PIN_1, PIN_5, PIN_4, PIN_2 }
/// The DIO output pin enabled state array. This must be sync with the array above.
#define IOHANDLER_DIO_OUTPUT_PIN_ENABLED_STATE		{ DISABLED, DISABLED, DISABLED, DISABLED }

/// The DIO input ports array
#define IOHANDLER_DIO_INPUT_PORTS					{ PORT_C, PORT_C, PORT_D, PORT_D }
/// The DIO input pin array. This must be sync with the array above.
#define IOHANDLER_DIO_INPUT_PINS					{ PIN_1, PIN_5, PIN_4, PIN_2 }
/// The DIO input pin resistor configuration array. This must be sync with the array above.
#define IOHANDLER_DIO_INPUT_PIN_RES_CONF			{ RESISTOR_DISABLED, RESISTOR_DISABLED, RESISTOR_DISABLED, RESISTOR_DISABLED }
/// The DIO input pin interrupt configuration array. This must be sync with the array above.
#define IOHANDLER_DIO_INPUT_PIN_INT_CONF			{ INTERRUPT_DISABLED, INTERRUPT_DISABLED, INTERRUPT_DISABLED, INTERRUPT_DISABLED }
/// The DIO input pin enabled state array. This must be sync with the array above.
#define IOHANDLER_DIO_INPUT_PIN_ENABLED_STATE		{ DISABLED, DISABLED, DISABLED, DISABLED }


// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------


/// Enumeration type for the DIO inputs
typedef enum {
	INPUT_0 = 0,
	INPUT_1,
	INPUT_2,
	INPUT_3,
	NUM_INPUTS
} IOHandlerDIOInput;

/// Enumeration type for the DIO outputs
typedef enum {
	OUTPUT_0 = 0,
	OUTPUT_1,
	OUTPUT_2,
	OUTPUT_3,
	NUM_OUTPUTS
} IOHandlerDIOOutput;

/// Enumeration type for the digital IO values
typedef enum {
	LEVEL_LOW = 0,
	LEVEL_HIGH
} IOHandlerDIOValue;

/// Enumeration type for GPIO ports
typedef enum {
	PORT_A = 0,
	PORT_B,
	PORT_C,
	PORT_D,
	PORT_E,
	PORT_F,
	NUM_PORTS
} IOHandlerDIOPort;

/// Enumeration type for GPIO pins
typedef enum {
	PIN_0 = 0,
	PIN_1,
	PIN_2,
	PIN_3,
	PIN_4,
	PIN_5,
	PIN_6,
	PIN_7,
	NUM_PINS
} IOHandlerDIOPin;

/// Enumeration type for GPIO input resistor config
typedef enum {
	RESISTOR_DISABLED = 0,
	RESISTOR_PULLUP,
	RESISTOR_PULLDOWN
} IOHandlerDIOInputResistorConfig;

/// Enumeration type for GPIO input interrupt type config
typedef enum {
	INTERRUPT_DISABLED = 0,
	INTERRUPT_FALLING_EDGE,
	INTERRUPT_RISING_EDGE,
	INTERRUPT_BOTH_EDGE
} IOHandlerDIOInputInterruptConfig;

/// Enumeration type for DIO pin enabled status
typedef enum {
	ENABLED = 0,
	DISABLED
} IOHandlerDIOEnabledState;

/// Interrupt handler for any GPIO pin
typedef void (*IOHandlerDIOInterruptHandler)(IOHandlerDIOPort pPort_e, IOHandlerDIOPin pPin_e);

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------

GOKART_API	void 	ioHandlerInitDIO(void);
GOKART_API	void	ioHandlerDIORegisterCallback(IOHandlerDIOInput pInput_e, IOHandlerDIOInterruptHandler pHandler_pf);
GOKART_API	void 	ioHandlerDIOWrite(IOHandlerDIOOutput pOutput_e, IOHandlerDIOValue pValue_e);
GOKART_API	void	ioHandlerDIOToggle(IOHandlerDIOOutput pOutput_e);
GOKART_API	IOHandlerDIOValue 	ioHandlerDIORead(IOHandlerDIOInput pInput_e);
GOKART_API	int32_t		ioHandlerDIOOnPortInterrupt(IOHandlerDIOPort pPort_e);
GOKART_API	uint32_t	ioHandlerDIOGetPortAddress(IOHandlerDIOPort pPort_e);
GOKART_API	uint32_t	ioHandlerDIOGetPinMask(IOHandlerDIOPin pPin_e);

GOKART_API	void	ioHandlerDIOWritePin(IOHandlerDIOPort pPort_e, IOHandlerDIOPin pPin_e, IOHandlerDIOValue pValue_e);
GOKART_API	IOHandlerDIOValue 	ioHandlerDIOReadPin(IOHandlerDIOPort pPort_e, IOHandlerDIOPin pPin_e);
GOKART_API	void	ioHandlerDIOConfigureAsOutput(IOHandlerDIOPort pPort_e, IOHandlerDIOPin pPin_e);
GOKART_API	void	ioHandlerDIOConfigureAsInput(IOHandlerDIOPort pPort_e, IOHandlerDIOPin pPin_e);
GOKART_API	void	ioHandlerDIOConfigureInputResistor(IOHandlerDIOPort pPort_e, IOHandlerDIOPin pPin_e, IOHandlerDIOInputResistorConfig pResistorConfig_e);
GOKART_API	void	ioHandlerDIOConfigureInputInterrupt(IOHandlerDIOPort pPort_e, IOHandlerDIOPin pPin_e, IOHandlerDIOInputInterruptConfig pInterruptConfig_e);
GOKART_API	void	ioHandlerDIORegisterInterruptHandler(IOHandlerDIOPort pPort_e, IOHandlerDIOPin pPin_e, IOHandlerDIOInterruptHandler pHandler_pf);

GOKART_API	void 	__INTERRUPT_HANDLER__ioHandlerOnPortAInterruptHandler(void);
GOKART_API	void 	__INTERRUPT_HANDLER__ioHandlerOnPortBInterruptHandler(void);
GOKART_API	void 	__INTERRUPT_HANDLER__ioHandlerOnPortCInterruptHandler(void);
GOKART_API	void 	__INTERRUPT_HANDLER__ioHandlerOnPortDInterruptHandler(void);
GOKART_API	void 	__INTERRUPT_HANDLER__ioHandlerOnPortEInterruptHandler(void);
GOKART_API	void 	__INTERRUPT_HANDLER__ioHandlerOnPortFInterruptHandler(void);

#endif /* IOHANDLER_DIO_IOHANDLERDIO_H_ */
