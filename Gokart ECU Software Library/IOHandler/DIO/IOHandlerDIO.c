/**
 * @file	IOHandlerDIO.c
 * @authors	Gabor Kormendy
 * @date	2015.06.08.
 * @brief	This file contains function and variable definitions for the DIO module.
 */

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------
#include "IOHandlerDIO.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

/// DIO pin interrupt handler array
#define IOHANDLER_DIO_HANDLER_ARRAY					{ NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL }
/// Helper macro to get the GPIO port base address
#define IOHANDLER_DIO_PORT(info, num)				sgPortAddresses_pcu32[info.iPorts_e[num]]
/// Helper macro to get the GPIO pin mask
#define IOHANDLER_DIO_PIN(info, num)				sgPinMasks_pcu32[info.iPins_e[num]]
/// Helper macro to get both the GPIO port and the GPIO pin
#define IOHANDLER_DIO_PORT_AND_PIN(info, num)		IOHANDLER_DIO_PORT(info, num), IOHANDLER_DIO_PIN(info, num)


// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

/// Structure for the DIO outputs information
typedef struct IOHandlerDIOOutputData {
	/// The port array
	IOHandlerDIOPort iPorts_e[NUM_OUTPUTS];
	/// The pin array
	IOHandlerDIOPin iPins_e[NUM_OUTPUTS];
	/// The enabled state
	IOHandlerDIOEnabledState iEnabledState_e[NUM_OUTPUTS];
} IOHandlerDIOOutputData;

/// Structure for the DIO inputs information
typedef struct IOHandlerDIOInputData {
	/// The port array
	IOHandlerDIOPort iPorts_e[NUM_INPUTS];
	/// The pin array
	IOHandlerDIOPin iPins_e[NUM_INPUTS];
	/// The resistor config array
	IOHandlerDIOInputResistorConfig iResistorConfig_e[NUM_INPUTS];
	/// The interrupt config array
	IOHandlerDIOInputInterruptConfig iInterruptConfig_e[NUM_INPUTS];
	/// The enabled state
	IOHandlerDIOEnabledState iEnabledState_e[NUM_INPUTS];
} IOHandlerDIOInputData;

// ----------------------------------------------------------------------------
// Variable definitions.
// ----------------------------------------------------------------------------

/// Lookup table for GPIO port addresses
static const uint32_t sgPortAddresses_pcu32[NUM_PORTS] = {
		IOHANDLER_DIO_PORTA_BASE,
		IOHANDLER_DIO_PORTB_BASE,
		IOHANDLER_DIO_PORTC_BASE,
		IOHANDLER_DIO_PORTD_BASE,
		IOHANDLER_DIO_PORTE_BASE,
		IOHANDLER_DIO_PORTF_BASE
};

/// Lookup table for GPIO pins
static const uint32_t sgPinMasks_pcu32[NUM_PINS] = {
		IOHANDLER_DIO_PIN_0,
		IOHANDLER_DIO_PIN_1,
		IOHANDLER_DIO_PIN_2,
		IOHANDLER_DIO_PIN_3,
		IOHANDLER_DIO_PIN_4,
		IOHANDLER_DIO_PIN_5,
		IOHANDLER_DIO_PIN_6,
		IOHANDLER_DIO_PIN_7
};

/// Callback table for all GPIO pins
static IOHandlerDIOInterruptHandler sgPinInterruptHandlers_pf[NUM_PORTS][NUM_PINS] = {
		IOHANDLER_DIO_HANDLER_ARRAY,
		IOHANDLER_DIO_HANDLER_ARRAY,
		IOHANDLER_DIO_HANDLER_ARRAY,
		IOHANDLER_DIO_HANDLER_ARRAY,
		IOHANDLER_DIO_HANDLER_ARRAY,
		IOHANDLER_DIO_HANDLER_ARRAY
};

/// The DIO output information
static const IOHandlerDIOOutputData sgDIOOutputData_st = {
		.iPorts_e = IOHANDLER_DIO_OUTPUT_PORTS,
		.iPins_e = IOHANDLER_DIO_OUTPUT_PINS,
		.iEnabledState_e = IOHANDLER_DIO_OUTPUT_PIN_ENABLED_STATE
};

/// The DIO input information
static const IOHandlerDIOInputData sgDIOInputData_st = {
		.iPorts_e = IOHANDLER_DIO_INPUT_PORTS,
		.iPins_e = IOHANDLER_DIO_INPUT_PINS,
		.iResistorConfig_e = IOHANDLER_DIO_INPUT_PIN_RES_CONF,
		.iInterruptConfig_e = IOHANDLER_DIO_INPUT_PIN_INT_CONF,
		.iEnabledState_e = IOHANDLER_DIO_INPUT_PIN_ENABLED_STATE
};

/// The DIO output values
static IOHandlerDIOValue sgDIOOutputValue_pe[NUM_OUTPUTS] = { LEVEL_LOW };



// ----------------------------------------------------------------------------
// Function definitions.
// ----------------------------------------------------------------------------

/**
 * @brief	Interrupt handler for GPIO Port A
 * @return	none
 */
void __INTERRUPT_HANDLER__ioHandlerOnPortAInterruptHandler(void) {
	ioHandlerDIOOnPortInterrupt(PORT_A);
}

/**
 * @brief	Interrupt handler for GPIO Port B
 * @return	none
 */
void __INTERRUPT_HANDLER__ioHandlerOnPortBInterruptHandler(void) {
	ioHandlerDIOOnPortInterrupt(PORT_B);
}

/**
 * @brief	Interrupt handler for GPIO Port C
 * @return	none
 */
void __INTERRUPT_HANDLER__ioHandlerOnPortCInterruptHandler(void) {
	ioHandlerDIOOnPortInterrupt(PORT_C);
}

/**
 * @brief	Interrupt handler for GPIO Port D
 * @return	none
 */
void __INTERRUPT_HANDLER__ioHandlerOnPortDInterruptHandler(void) {
	ioHandlerDIOOnPortInterrupt(PORT_D);
}

/**
 * @brief	Interrupt handler for GPIO Port E
 * @return	none
 */
void __INTERRUPT_HANDLER__ioHandlerOnPortEInterruptHandler(void) {
	ioHandlerDIOOnPortInterrupt(PORT_E);
}

/**
 * @brief	Interrupt handler for GPIO Port F
 * @return	none
 */
void __INTERRUPT_HANDLER__ioHandlerOnPortFInterruptHandler(void) {
	ioHandlerDIOOnPortInterrupt(PORT_F);
}

/**
 * @brief	Initializes the DIO ports. Must be called before any other DIO functions.
 *
 * @return	none
 */
void ioHandlerInitDIO(void) {

	uint32_t lIndex_u32 = 0;

	for(lIndex_u32 = 0; lIndex_u32 < NUM_OUTPUTS; lIndex_u32++) {
		if(ENABLED == sgDIOOutputData_st.iEnabledState_e[lIndex_u32]) {
			ioHandlerDIOConfigureAsOutput(sgDIOOutputData_st.iPorts_e[lIndex_u32], sgDIOOutputData_st.iPins_e[lIndex_u32]);
		}
	}

	for(lIndex_u32 = 0; lIndex_u32 < NUM_INPUTS; lIndex_u32++) {
		if(ENABLED == sgDIOInputData_st.iEnabledState_e[lIndex_u32]) {
			ioHandlerDIOConfigureAsInput(sgDIOInputData_st.iPorts_e[lIndex_u32], sgDIOInputData_st.iPins_e[lIndex_u32]);
			ioHandlerDIOConfigureInputResistor(sgDIOInputData_st.iPorts_e[lIndex_u32], sgDIOInputData_st.iPins_e[lIndex_u32], sgDIOInputData_st.iResistorConfig_e[lIndex_u32]);
			ioHandlerDIOConfigureInputInterrupt(sgDIOInputData_st.iPorts_e[lIndex_u32], sgDIOInputData_st.iPins_e[lIndex_u32], sgDIOInputData_st.iInterruptConfig_e[lIndex_u32]);
		}
	}

#if (ECU_CONFIGURATION_ENABLE_INT_HANDLER_REGISTRATION == TRUE)
	GPIOIntRegister(GPIO_PORTA_BASE, __INTERRUPT_HANDLER__ioHandlerOnPortAInterruptHandler);
	GPIOIntRegister(GPIO_PORTB_BASE, __INTERRUPT_HANDLER__ioHandlerOnPortBInterruptHandler);
	GPIOIntRegister(GPIO_PORTC_BASE, __INTERRUPT_HANDLER__ioHandlerOnPortCInterruptHandler);
	GPIOIntRegister(GPIO_PORTD_BASE, __INTERRUPT_HANDLER__ioHandlerOnPortDInterruptHandler);
	GPIOIntRegister(GPIO_PORTE_BASE, __INTERRUPT_HANDLER__ioHandlerOnPortEInterruptHandler);
	GPIOIntRegister(GPIO_PORTF_BASE, __INTERRUPT_HANDLER__ioHandlerOnPortFInterruptHandler);
#endif

}

/**
 * @brief	This function writes the specified value to the output
 * @param	pOutput_e	The DIO output
 * @param	pValue_e	The DIO output value
 * @return	none
 */
void ioHandlerDIOWrite(IOHandlerDIOOutput pOutput_e, IOHandlerDIOValue pValue_e) {
	__ASSERT(pOutput_e >= NUM_OUTPUTS);

	GPIOPinWrite(IOHANDLER_DIO_PORT_AND_PIN(sgDIOOutputData_st, pOutput_e),
			(pValue_e == LEVEL_HIGH)?IOHANDLER_DIO_PIN(sgDIOOutputData_st, pOutput_e):0);

	sgDIOOutputValue_pe[pOutput_e] = pValue_e;
}

/**
 * @brief	This function toggles the output value on the DIO output
 * @param	pOutput_e	The DIO output
 * @return	none
 */
void ioHandlerDIOToggle(IOHandlerDIOOutput pOutput_e) {
	__ASSERT(pOutput_e >= NUM_OUTPUTS);

	if(LEVEL_LOW == sgDIOOutputValue_pe[pOutput_e]) {
		sgDIOOutputValue_pe[pOutput_e] = LEVEL_HIGH;
	} else {
		sgDIOOutputValue_pe[pOutput_e] = LEVEL_LOW;
	}

	ioHandlerDIOWrite(pOutput_e, sgDIOOutputValue_pe[pOutput_e]);
}

/**
 * @brief	This function reads the specified DIO input
 * @param	pInput_e	The DIO input
 * @return	The DIO input value
 */
IOHandlerDIOValue ioHandlerDIORead(IOHandlerDIOInput pInput_e) {
	__ASSERT(pInput_e >= NUM_INPUTS);

	uint32_t lValue_u32 = GPIOPinRead(IOHANDLER_DIO_PORT_AND_PIN(sgDIOInputData_st, pInput_e));

	if(0 == lValue_u32) {
		return LEVEL_LOW;
	} else {
		return LEVEL_HIGH;
	}

}

/**
 * @brief	This function returns the GPIO base address of the given port
 * @param	pPort_e		The GPIO port
 * @return	The GPIO base address
 */
uint32_t ioHandlerDIOGetPortAddress(IOHandlerDIOPort pPort_e) {
	__ASSERT(pPort_e >= NUM_PORTS);
	return sgPortAddresses_pcu32[pPort_e];
}

/**
 * @brief	This function returns the GPIO pin mask of the given pin
 * @param	pPin_e		The GPIO pin
 * @return	The GPIO pin mask
 */
uint32_t ioHandlerDIOGetPinMask(IOHandlerDIOPin pPin_e) {
	__ASSERT(pPin_e >= NUM_PINS);
	return sgPinMasks_pcu32[pPin_e];
}

/**
 * @brief	This function handles the GPIO interrupt
 * @param	pPort_e		The GPIO port
 * @return	It returns 0 on success or with an error code
 */
int32_t	ioHandlerDIOOnPortInterrupt(IOHandlerDIOPort pPort_e) {
	uint32_t lIndex_u32 = 0;
	uint32_t lInterruptStatus_u32 = GPIOIntStatus(sgPortAddresses_pcu32[pPort_e], true);
	GPIOIntClear(sgPortAddresses_pcu32[pPort_e], lInterruptStatus_u32);

	for(lIndex_u32 = 0; lIndex_u32 < NUM_PINS; lIndex_u32++) {
		if(lInterruptStatus_u32 & sgPinMasks_pcu32[lIndex_u32]) {
			if(NULL != sgPinInterruptHandlers_pf[pPort_e][lIndex_u32]) {
				sgPinInterruptHandlers_pf[pPort_e][lIndex_u32](pPort_e, (IOHandlerDIOPin) lIndex_u32);
			}
		}
	}

	return 0;
}

/**
 * @brief	This function registers the given callback to the DIO input
 * @param	pInput_e		The DIO input
 * @param	pHandler_pf		The interrupt handler function
 * @return	none
 */
void ioHandlerDIORegisterCallback(IOHandlerDIOInput pInput_e, IOHandlerDIOInterruptHandler pHandler_pf) {
	__ASSERT(pInput_e >= NUM_INPUTS);
	__ASSERT(NULL == pHandler_pf);

	IOHandlerDIOPort lPort_e = sgDIOInputData_st.iPorts_e[pInput_e];
	IOHandlerDIOPin lPin_e = sgDIOInputData_st.iPins_e[pInput_e];

	ioHandlerDIORegisterInterruptHandler(lPort_e, lPin_e, pHandler_pf);
}

/**
 * @brief	This function writes the GPIO pin
 * @param	pPort_e		The GPIO port
 * @param	pPin_e		The GPIO pin
 * @param	pValue_e	The GPIO value
 * @return	none
 */
void ioHandlerDIOWritePin(IOHandlerDIOPort pPort_e, IOHandlerDIOPin pPin_e, IOHandlerDIOValue pValue_e) {
	__ASSERT(pPort_e >= NUM_PORTS);
	__ASSERT(pPin_e >= NUM_PINS);

	GPIOPinWrite(sgPortAddresses_pcu32[pPort_e], sgPinMasks_pcu32[pPin_e], (pValue_e == LEVEL_HIGH)?sgPinMasks_pcu32[pPin_e]:0);
}

/**
 * @brief	This function reads the GPIO pin
 * @param	pPort_e		The GPIO port
 * @param	pPin_e		The GPIO pin
 * @return	The value of the GPIO pin (@see IOHandlerDIOValue)
 */
IOHandlerDIOValue ioHandlerDIOReadPin(IOHandlerDIOPort pPort_e, IOHandlerDIOPin pPin_e) {
	uint32_t lPinValue_u32 = 0;

	__ASSERT(pPort_e >= NUM_PORTS);
	__ASSERT(pPin_e >= NUM_PINS);

	lPinValue_u32 = GPIOPinRead(sgPortAddresses_pcu32[pPort_e], sgPinMasks_pcu32[pPin_e]);

	return (0 == lPinValue_u32)?LEVEL_LOW:LEVEL_HIGH;
}

/**
 * @brief	This function configures the GPIO pin as an output
 * @param	pPort_e		The GPIO port
 * @param	pPin_e		The GPIO pin
 * @return	none
 */
void ioHandlerDIOConfigureAsOutput(IOHandlerDIOPort pPort_e, IOHandlerDIOPin pPin_e) {
	__ASSERT(pPort_e >= NUM_PORTS);
	__ASSERT(pPin_e >= NUM_PINS);

	uint32_t lPortAddress_u32 = ioHandlerDIOGetPortAddress(pPort_e);
	uint32_t lPinMask_u32 = ioHandlerDIOGetPinMask(pPin_e);

	GPIOPinTypeGPIOOutput(lPortAddress_u32, lPinMask_u32);
}

/**
 * @brief	This function configures the GPIO pin as an input
 * @param	pPort_e		The GPIO port
 * @param	pPin_e		The GPIO pin
 * @return	none
 */
void ioHandlerDIOConfigureAsInput(IOHandlerDIOPort pPort_e, IOHandlerDIOPin pPin_e) {
	__ASSERT(pPort_e >= NUM_PORTS);
	__ASSERT(pPin_e >= NUM_PINS);

	uint32_t lPortAddress_u32 = ioHandlerDIOGetPortAddress(pPort_e);
	uint32_t lPinMask_u32 = ioHandlerDIOGetPinMask(pPin_e);

	GPIOPinTypeGPIOInput(lPortAddress_u32, lPinMask_u32);
}

/**
 * @brief	This function configures the input resistor
 * @param	pPort_e				The GPIO port
 * @param	pPin_e				The GPIO pin
 * @param	pResistorConfig_e	The resistor configuration
 * @return	none
 */
void ioHandlerDIOConfigureInputResistor(IOHandlerDIOPort pPort_e, IOHandlerDIOPin pPin_e, IOHandlerDIOInputResistorConfig pResistorConfig_e) {
	__ASSERT(pPort_e >= NUM_PORTS);
	__ASSERT(pPin_e >= NUM_PINS);

	switch(pResistorConfig_e) {

		case RESISTOR_PULLUP:
			GPIOPadConfigSet(sgPortAddresses_pcu32[pPort_e], sgPinMasks_pcu32[pPin_e], GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
			break;

		case RESISTOR_PULLDOWN:
			GPIOPadConfigSet(sgPortAddresses_pcu32[pPort_e], sgPinMasks_pcu32[pPin_e], GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPD);
			break;

		case RESISTOR_DISABLED:
			GPIOPadConfigSet(sgPortAddresses_pcu32[pPort_e], sgPinMasks_pcu32[pPin_e], GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD);
			break;

	}
}

/**
 * @brief	This function configures the input pin interrupt type
 * @param	pPort_e					The GPIO port
 * @param	pPin_e					The GPIO pin
 * @param	pInterruptConfig_e		The interrupt configuration
 * @return	none
 */
void ioHandlerDIOConfigureInputInterrupt(IOHandlerDIOPort pPort_e, IOHandlerDIOPin pPin_e, IOHandlerDIOInputInterruptConfig pInterruptConfig_e) {
	__ASSERT(pPort_e >= NUM_PORTS);
	__ASSERT(pPin_e >= NUM_PINS);

	switch(pInterruptConfig_e) {

		case INTERRUPT_FALLING_EDGE:
			GPIOIntTypeSet(sgPortAddresses_pcu32[pPort_e], sgPinMasks_pcu32[pPin_e], GPIO_FALLING_EDGE);
			GPIOIntEnable(sgPortAddresses_pcu32[pPort_e], sgPinMasks_pcu32[pPin_e]);
			break;

		case INTERRUPT_RISING_EDGE:
			GPIOIntTypeSet(sgPortAddresses_pcu32[pPort_e], sgPinMasks_pcu32[pPin_e], GPIO_RISING_EDGE);
			GPIOIntEnable(sgPortAddresses_pcu32[pPort_e], sgPinMasks_pcu32[pPin_e]);
			break;

		case INTERRUPT_BOTH_EDGE:
			GPIOIntTypeSet(sgPortAddresses_pcu32[pPort_e], sgPinMasks_pcu32[pPin_e], GPIO_BOTH_EDGES);
			GPIOIntEnable(sgPortAddresses_pcu32[pPort_e], sgPinMasks_pcu32[pPin_e]);
			break;

		case INTERRUPT_DISABLED:
			GPIOIntDisable(sgPortAddresses_pcu32[pPort_e], sgPinMasks_pcu32[pPin_e]);
			break;

	}

}

/**
 * @brief	This function registers the given interrupt handler function
 * @param	pPort_e			The GPIO port
 * @param	pPin_e			The GPIO pin
 * @param	pHandler_pf		The handler function
 * @return	none
 */
void ioHandlerDIORegisterInterruptHandler(IOHandlerDIOPort pPort_e, IOHandlerDIOPin pPin_e, IOHandlerDIOInterruptHandler pHandler_pf) {
	__ASSERT(pPort_e >= NUM_PORTS);
	__ASSERT(pPin_e >= NUM_PINS);
	__ASSERT(NULL == pHandler_pf);

	sgPinInterruptHandlers_pf[pPort_e][pPin_e] = pHandler_pf;
}

