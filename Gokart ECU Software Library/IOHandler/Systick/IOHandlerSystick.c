/**
 * @file	IOHandlerSystick.c
 * @author	Gabor Kormendy
 * @version	1.0
 * @brief	This file contains function and variable definitions for the
 * 			Systick module
 */

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------
#include "IOHandlerSystick.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

#define IOHANDLER_SYSTICK_TASK_QUEUE_SIZE	32

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

/// The lifetime counter
static uint64_t sgLifetimeCounter_u64 = 0;
/// The scheduler counter
static uint32_t sgSchedulerCounter_u32 = 0;
/// The active task id
static uint32_t sgActiveTaskId_u32 = 0;

static IOHandlerTask* sgTaskList_pst = NULL;
static uint32_t sgTaskListSize_u32 = 0;

static CircularBuffer sgTaskQueue_st = { 0 };

uint8_t sgTaskBuffer_u8[IOHANDLER_SYSTICK_TASK_QUEUE_SIZE];

static volatile bool sgRunScheduler_b = false;

static volatile bool sgShutdownScheduler_b = false;

// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------

/**
 * @brief	This is the main scheduler. It has to be called in the main loop.
 * @return	none
 * @todo	Replace the task array with circular buffer
 */
void scheduler(void) {
	// Index variable
	/*
	uint32_t lIndex_u32 = 0;
	for(lIndex_u32 = 0; lIndex_u32 < sgTaskListSize_u32; lIndex_u32++) {
		if((true == sgTaskList_pst[lIndex_u32].iCall_b) && (true == sgTaskList_pst[lIndex_u32].iActive_b)) {
			sgActiveTaskId_u32 = sgTaskList_pst[lIndex_u32].iTaskId_u32;
			sgTaskList_pst[lIndex_u32].iTask();
			sgTaskList_pst[lIndex_u32].iCall_b = false;
			sgActiveTaskId_u32 = 0;
		}
	}
	*/
	uint8_t lTaskIndex_u8 = 0;

	ioHandlerIOSchedulerRun(sgSchedulerCounter_u32);

	if(true == sgRunScheduler_b) {
		sgRunScheduler_b = false;
		while(circularBufferGet(&sgTaskQueue_st, &lTaskIndex_u8) != -1) {
			sgTaskList_pst[lTaskIndex_u8].iTask_pf();
		}
	}


}

/**
 * @brief	This function initializes the systick peripheral. It must be called before
 * 			initializating or running the scheduler functions.
 * @return	none
 */
void ioHandlerInitSystick(void) {
	SysTickPeriodSet(SysCtlClockGet() / IOHANDLER_SYSTICK_TICKS_PER_SECOND);
	SysTickEnable();
	SysTickIntEnable();

#if (ECU_CONFIGURATION_ENABLE_INT_HANDLER_REGISTRATION == TRUE)
	SysTickIntRegister(ioHandlerOnSystickInterrupt);
#endif

}

/**
 * @brief	This function initializes the scheduler. This function must be called only
 * 			once and before using any other scheduler functions.
 * @param	pTaskList_pst		The task definition array
 * @param	pTaskListSize_u32	Size of the task definition array
 * @return	none
 */
void ioHandlerInitScheduler(IOHandlerTask*  pTaskList_pst, uint32_t pTaskListSize_u32) {
	// Index variable
	uint32_t lIndex_u32 = 0;

	__ASSERT(pTaskList_pst == NULL);

	// Set starting mask
	sgSchedulerCounter_u32 = 1;

	// Assign variables
	sgTaskList_pst = pTaskList_pst;
	sgTaskListSize_u32 = pTaskListSize_u32;

	sgTaskQueue_st.iOverwriteProtection_b = true;
	sgTaskQueue_st.iBuffer_pu8 = sgTaskBuffer_u8;
	sgTaskQueue_st.iBufferSize_u32 = IOHANDLER_SYSTICK_TASK_QUEUE_SIZE;
	circularBufferInit(&sgTaskQueue_st);

	// Safety: loop through the tasks and reset their state
	for (lIndex_u32 = 0; lIndex_u32 < sgTaskListSize_u32; lIndex_u32++) {
		//sgTaskList_pst[lIndex_u32].iCall_b = false;
		// Safety check
		if (NULL == sgTaskList_pst[lIndex_u32].iTask_pf) {
			// Disable null tasks
			sgTaskList_pst[lIndex_u32].iActive_b = false;
		} else {

			if(NULL != sgTaskList_pst[lIndex_u32].iTaskInit_pf) {
				sgTaskList_pst[lIndex_u32].iTaskInit_pf();
			}

		}

	}

}

/**
 * @brief	This function shall be called in the systick interrupt handler.
 * @details	Increments the gLifetimeCounter_u64 and activates the tasks according to their
 * 			scheduler mask.
 * @return	none
 */
void ioHandlerOnSystickInterrupt(void) {
	// Index variable
	uint32_t lIndex_u32 = 0;
	// Increment the lifetime counter
	sgLifetimeCounter_u64++;

	if(false == sgShutdownScheduler_b) {

		for (lIndex_u32 = 0; lIndex_u32 < sgTaskListSize_u32; lIndex_u32++) {
			if (0 != (sgTaskList_pst[lIndex_u32].iSchedulerMask_u32 & sgSchedulerCounter_u32) && true == sgTaskList_pst[lIndex_u32].iActive_b) {
				// Activate task
				//sgTaskList_pst[lIndex_u32].iCall_b = true;
				circularBufferPut(&sgTaskQueue_st, lIndex_u32);
			}
		}

		// Set flag
		sgRunScheduler_b = true;
	}

	sgSchedulerCounter_u32 = sgSchedulerCounter_u32 << 1;

	if(0 == sgSchedulerCounter_u32 || sgSchedulerCounter_u32 > IOHANDLER_SYSTICK_MAX_COUNTER) {
		// Initialize the scheduler counter
		sgSchedulerCounter_u32 = 1;
	}


}

/**
 * @brief	Returns with the value of the gLifetimeCounter_u64.
 * @return	Value of gLifetimeCounter_u64.
 */
uint64_t ioHandlerSystickGetLifetime(void) {
	return sgLifetimeCounter_u64;
}

/**
 * @brief	Disables task based on pTaskId_u32.
 * @param	pTaskId_u32	The task ID.
 * @return	It returns 0 on succes or ERROR_HANDLER_SCHEDULER_NO_SUCH_TASK error code.
 */
int32_t ioHandlerDisableTask(uint32_t pTaskId_u32) {
	// The index variable
	uint32_t lIndex_u32 = 0;

	for (lIndex_u32 = 0; lIndex_u32 < sgTaskListSize_u32; lIndex_u32++) {
		if (pTaskId_u32 == sgTaskList_pst[lIndex_u32].iTaskId_u32) {
			sgTaskList_pst[lIndex_u32].iActive_b = false;
			return 0;
		}
	}
	return ERROR_HANDLER_SCHEDULER_NO_SUCH_TASK;
}

/**
 * @brief	Enables task based on pTaskId_u32.
 * @param	pTaskId_u32	The task ID.
 * @return	It returns 0 on success or ERROR_HANDLER_SCHEDULER_NO_SUCH_TASK error code.
 */
int32_t ioHandlerEnableTask(uint32_t pTaskId_u32) {
	// The index variable
	uint32_t lIndex_u32 = 0;

	for (lIndex_u32 = 0; lIndex_u32 < sgTaskListSize_u32; lIndex_u32++) {
		if (pTaskId_u32 == sgTaskList_pst[lIndex_u32].iTaskId_u32) {
			sgTaskList_pst[lIndex_u32].iActive_b = true;
			return 0;
		}
	}
	return ERROR_HANDLER_SCHEDULER_NO_SUCH_TASK;
}

/**
 * @brief	This function returns the active task id
 * @return	The active task id
 */
uint32_t ioHandlerSystickGetActiveTaskId(void) {
	return sgActiveTaskId_u32;
}

uint32_t ioHandlerSystickGetMask(void) {
	return sgSchedulerCounter_u32;
}

void ioHandlerSchedulerStop(void) {
	sgShutdownScheduler_b = true;
	circularBufferFlush(&sgTaskQueue_st);
}

void ioHandlerSchedulerResume(void) {
	sgShutdownScheduler_b = false;
}

