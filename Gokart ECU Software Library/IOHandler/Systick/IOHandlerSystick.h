/**
 * @file	IOHandlerSystick.h
 * @author	Gabor Kormendy
 * @version	1.0
 * @brief	This file contains function and variable declaration for
 * 			Systick module.
 */

#ifndef IOHANDLER_SYSTICK_IOHANDLERSYSTICK_H_
#define IOHANDLER_SYSTICK_IOHANDLERSYSTICK_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <driverlib/sysctl.h>
#include <driverlib/systick.h>

#include "../IOScheduler/IOScheduler.h"

#include "../../ErrorHandler/ErrorHandler.h"

#include "../../Utils/Buffers/CircularBuffer/CircularBuffer.h"

#include "../../ECUConfiguration.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

/// The number of ticks in one second.
#define IOHANDLER_SYSTICK_TICKS_PER_SECOND		1000
/// The maximum of the task scheduler counter.
#define IOHANDLER_SYSTICK_MAX_COUNTER			0x200
/// Helper macro to define task list
#define IOHANDLER_SYSTICK_TASK_LIST_START()		\
	IOHandlerTask gTaskList_pst[] = {
/// Helper macro to define task list entry
#define IOHANDLER_SYSTICK_TASK_LIST_ENTRY(taskId, taskFunction, taskInitFunction, schedulerMask, active)	\
	{ taskId, taskFunction, taskInitFunction, schedulerMask, active }
/// Helper macro to define task list
#define IOHANDLER_SYSTICK_TASK_LIST_END()	\
	};	\
	const uint32_t gTaskListSize_u32 = sizeof(gTaskList_pst) / sizeof(gTaskList_pst[0]);
/// Redefinition @see IOHANDLER_SYSTICK_TASK_LIST_START
#define SCHEDULER_TASK_LIST_START()		IOHANDLER_SYSTICK_TASK_LIST_START()
/// Redefinition @see IOHANDLER_SYSTICK_TASK_LIST_ENTRY
#define SCHEDULER_TASK(taskId, taskFunction, taskInitFunction, schedulerMask, active)		IOHANDLER_SYSTICK_TASK_LIST_ENTRY(taskId, taskFunction, taskInitFunction, schedulerMask, active)
/// Redefinition @see IOHANDLER_SYSTICK_TASK_LIST_END
#define SCHEDULER_TASK_LIST_END()		IOHANDLER_SYSTICK_TASK_LIST_END()

/// Helper macro to initialize software delay (DO NOT ENTER 0)
#define SCHEDULER_RUN_EVERY_N_TH_CYCLE(N)	\
	static uint32_t __slCounter_u32 = N - 1;	\
	if(0 != __slCounter_u32) { __slCounter_u32--; return; } else { __slCounter_u32 = N - 1; }


// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

/**
 * @brief	The task function type
 */
typedef void (*TaskFunction)(void);

/// The task init function type
typedef void (*TaskInitFunction)(void);

/**
 * @brief	The task description structure
 */
typedef struct IOHandlerTask {
	/// The task ID
	uint32_t iTaskId_u32;
	/// The task function
	TaskFunction iTask_pf;
	/// Optional task init function
	TaskInitFunction iTaskInit_pf;
	/// Scheduler mask for timing
	/// (if the schedule mask & task scheduler counter = 1 the function
	/// shall be called).
	uint32_t iSchedulerMask_u32;
	/// Signs that the task is active
	bool iActive_b;
	/// Signs that the task needs to be called
	//bool iCall_b;
} IOHandlerTask;

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------

GOKART_API	void 		scheduler(void);
GOKART_API	void 		ioHandlerInitSystick(void);
GOKART_API	void 		ioHandlerInitScheduler(IOHandlerTask*  pTaskList_pst, uint32_t pTaskListSize_u32);
GOKART_API	void 		ioHandlerOnSystickInterrupt(void);
GOKART_API	uint64_t 	ioHandlerSystickGetLifetime(void);
GOKART_API	int32_t 	ioHandlerDisableTask(uint32_t pTaskId_u32);
GOKART_API	int32_t 	ioHandlerEnableTask(uint32_t pTaskId_u32);
GOKART_API	uint32_t 	ioHandlerSystickGetActiveTaskId(void);
GOKART_API	uint32_t 	ioHandlerSystickGetMask(void);
GOKART_API	void		ioHandlerSchedulerStop(void);
GOKART_API	void		ioHandlerSchedulerResume(void);


#endif /* IOHANDLER_SYSTICK_IOHANDLERSYSTICK_H_ */
