/**
 * @file	IOHandlerPWM.c
 * @authors	Grimgrin
 * @date	2016 jan. 24
 * @brief	
 */

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include "IOHandlerPWM.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

#define IOHANDLER_PWM_GET_PWM_MODULE_BASE_ADDRESS(module)	\
	sgPWMModuleInfo_st.iPWMBaseAddresses_pu32[module]

#define IOHANDLER_PWM_GET_PWM_GENERATOR_MODULE_ADDRESS(generator)	\
	sgPWMModuleInfo_st.iPWMGenerators_pu32[generator]

#define IOHANDLER_PWM_GET_PWM_OUTPUT(output)		\
	sgPWMModuleInfo_st.iPWMOutputs_pu32[output]

#define IOHANDLER_PWM_GET_PWM_OUTPUT_BIT(outputBit)		\
	sgPWMModuleInfo_st.iPWMOutputBits_pu32[outputBit]

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

typedef struct IOHandlerPWMModule {
	/// Array containing all PWM modules base addresses
	uint32_t iPWMBaseAddresses_pu32[NUM_PWM_MODULE];
	/// Array containing all PWM generators addresses
	uint32_t iPWMGenerators_pu32[NUM_PWM_GENERATOR];
	/// Array containing all PWM outputs
	uint32_t iPWMOutputs_pu32[NUM_PWM_OUTPUT * NUM_PWM_GENERATOR];
	/// Array containing all PWM output bits
	uint32_t iPWMOutputBits_pu32[NUM_PWM_OUTPUT * NUM_PWM_GENERATOR];
} IOHandlerPWMModule;

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

static const IOHandlerPWMModule sgPWMModuleInfo_st = {
		{ PWM0_BASE, PWM1_BASE },
		{ PWM_GEN_0, PWM_GEN_1, PWM_GEN_2, PWM_GEN_3 },
		{ PWM_OUT_0, PWM_OUT_1, PWM_OUT_2, PWM_OUT_3, PWM_OUT_4, PWM_OUT_5, PWM_OUT_6, PWM_OUT_7 },
		{ PWM_OUT_0_BIT, PWM_OUT_1_BIT, PWM_OUT_2_BIT, PWM_OUT_3_BIT, PWM_OUT_4_BIT, PWM_OUT_5_BIT, PWM_OUT_6_BIT, PWM_OUT_7_BIT },
};

static uint32_t sgPWMClock_u32 = 0;

#if (ECU_CONFIGURATION_SYSTEM_PWM_CLOCK_CONFIGURATION == SYSCTL_PWMDIV_1)
#define IOHANDLER_PWM_MODULE_CLOCK_DIVIDER		1
#elif (ECU_CONFIGURATION_SYSTEM_PWM_CLOCK_CONFIGURATION == SYSCTL_PWMDIV_2)
#define IOHANDLER_PWM_MODULE_CLOCK_DIVIDER		2
#elif (ECU_CONFIGURATION_SYSTEM_PWM_CLOCK_CONFIGURATION == SYSCTL_PWMDIV_4)
#define IOHANDLER_PWM_MODULE_CLOCK_DIVIDER		4
#elif (ECU_CONFIGURATION_SYSTEM_PWM_CLOCK_CONFIGURATION == SYSCTL_PWMDIV_8)
#define IOHANDLER_PWM_MODULE_CLOCK_DIVIDER		8
#elif (ECU_CONFIGURATION_SYSTEM_PWM_CLOCK_CONFIGURATION == SYSCTL_PWMDIV_16)
#define IOHANDLER_PWM_MODULE_CLOCK_DIVIDER		16
#elif (ECU_CONFIGURATION_SYSTEM_PWM_CLOCK_CONFIGURATION == SYSCTL_PWMDIV_32)
#define IOHANDLER_PWM_MODULE_CLOCK_DIVIDER		32
#elif (ECU_CONFIGURATION_SYSTEM_PWM_CLOCK_CONFIGURATION == SYSCTL_PWMDIV_64)
#define IOHANDLER_PWM_MODULE_CLOCK_DIVIDER		64
#endif

// ----------------------------------------------------------------------------
// Function definitions.
// ----------------------------------------------------------------------------

/**
 * @brief	This function initializes all the enabled PWM modules and outputs.
 *
 * @return	none
 */
void ioHandlerInitPWMModules(void) {

	sgPWMClock_u32 = SysCtlClockGet() / IOHANDLER_PWM_MODULE_CLOCK_DIVIDER;

#if (IOHANDLER_PWM_MODULE_0_PWM_GENERATOR_0_ENABLE == TRUE || IOHANDLER_PWM_MODULE_0_PWM_GENERATOR_1_ENABLE == TRUE || IOHANDLER_PWM_MODULE_0_PWM_GENERATOR_2_ENABLE == TRUE || IOHANDLER_PWM_MODULE_0_PWM_GENERATOR_3_ENABLE == TRUE)
	SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
#endif

#if (IOHANDLER_PWM_MODULE_1_PWM_GENERATOR_0_ENABLE == TRUE || IOHANDLER_PWM_MODULE_1_PWM_GENERATOR_1_ENABLE == TRUE || IOHANDLER_PWM_MODULE_1_PWM_GENERATOR_2_ENABLE == TRUE || IOHANDLER_PWM_MODULE_1_PWM_GENERATOR_3_ENABLE == TRUE)
	SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM1);
#endif


#if (IOHANDLER_PWM_MODULE_0_PWM_GENERATOR_0_ENABLE == TRUE)
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	GPIOPinTypePWM(GPIO_PORTB_BASE, GPIO_PIN_6 | GPIO_PIN_7);
	GPIOPinConfigure(GPIO_PB6_M0PWM0);
	GPIOPinConfigure(GPIO_PB7_M0PWM1);
#endif

#if (IOHANDLER_PWM_MODULE_0_PWM_GENERATOR_1_ENABLE == TRUE)
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	GPIOPinTypePWM(GPIO_PORTB_BASE, GPIO_PIN_4 | GPIO_PIN_5);
	GPIOPinConfigure(GPIO_PB4_M0PWM2);
	GPIOPinConfigure(GPIO_PB5_M0PWM3);
#endif

#if (IOHANDLER_PWM_MODULE_0_PWM_GENERATOR_2_ENABLE == TRUE)
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	GPIOPinTypePWM(GPIO_PORTE_BASE, GPIO_PIN_6 | GPIO_PIN_7);
	GPIOPinConfigure(GPIO_PE4_M0PWM4);
	GPIOPinConfigure(GPIO_PE5_M0PWM5);
#endif

#if (IOHANDLER_PWM_MODULE_0_PWM_GENERATOR_3_ENABLE == TRUE)

#if (IOHANDLER_PWM_MODULE_0_OUTPUT_6_ALTERNATE == FALSE)
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
	GPIOPinTypePWM(GPIO_PORTC_BASE, GPIO_PIN_4);
	GPIOPinConfigure(GPIO_PC4_M0PWM6);
#else
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
	GPIOPinTypePWM(GPIO_PORTD_BASE, GPIO_PIN_0);
	GPIOPinConfigure(GPIO_PD0_M0PWM6);
#endif

#if (IOHANDLER_PWM_MODULE_0_OUTPUT_7_ALTERNATE == FALSE)
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
	GPIOPinTypePWM(GPIO_PORTC_BASE, GPIO_PIN_5);
	GPIOPinConfigure(GPIO_PC5_M0PWM7);
#else
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
	GPIOPinTypePWM(GPIO_PORTD_BASE, GPIO_PIN_1);
	GPIOPinConfigure(GPIO_PD1_M0PWM7);
#endif

#endif


#if (IOHANDLER_PWM_MODULE_1_PWM_GENERATOR_0_ENABLE == TRUE)
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
	GPIOPinTypePWM(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1);
	GPIOPinConfigure(GPIO_PD0_M1PWM0);
	GPIOPinConfigure(GPIO_PD1_M1PWM1);
#endif

#if (IOHANDLER_PWM_MODULE_1_PWM_GENERATOR_1_ENABLE == TRUE)

#if (IOHANDLER_PWM_MODULE_1_OUTPUT_2_ALTERNATE == FALSE)
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	GPIOPinTypePWM(GPIO_PORTA_BASE, GPIO_PIN_6);
	GPIOPinConfigure(GPIO_PA6_M1PWM2);
#else
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	GPIOPinTypePWM(GPIO_PORTE_BASE, GPIO_PIN_4);
	GPIOPinConfigure(GPIO_PE4_M1PWM2);
#endif

#if (IOHANDLER_PWM_MODULE_1_OUTPUT_3_ALTERNATE == FALSE)
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	GPIOPinTypePWM(GPIO_PORTA_BASE, GPIO_PIN_7);
	GPIOPinConfigure(GPIO_PA7_M1PWM3);
#else
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	GPIOPinTypePWM(GPIO_PORTE_BASE, GPIO_PIN_5);
	GPIOPinConfigure(GPIO_PE5_M1PWM3);
#endif


#endif

#if (IOHANDLER_PWM_MODULE_1_PWM_GENERATOR_2_ENABLE == TRUE)
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_0 | GPIO_PIN_1);
	GPIOPinConfigure(GPIO_PF0_M1PWM4);
	GPIOPinConfigure(GPIO_PF1_M1PWM5);
#endif

#if (IOHANDLER_PWM_MODULE_1_PWM_GENERATOR_3_ENABLE == TRUE)
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_2 | GPIO_PIN_3);
	GPIOPinConfigure(GPIO_PF2_M1PWM6);
	GPIOPinConfigure(GPIO_PF3_M1PWM7);
#endif

}

/**
 * @brief	This function configures the PWM module and sets the PWM frequency
 *
 * @param	pPWMOutput_pst	The PWM output structure
 * @param	pPeriod_u32		The PWM frequency
 *
 * @return	It returns 0 on success
 */
int32_t	ioHandlerPWMInitModule(IOHandlerPWMOutput* pPWMOutput_pst, uint32_t pPeriod_u32) {
	uint32_t lPWMBase_u32 = 0;
	uint32_t lPWMGenerator_u32 = 0;
	uint32_t lPWMOutputBit1_u32 = 0;
	uint32_t lPWMOutputBit2_u32 = 0;
	uint32_t lPWMPeriod_u32 = 0;

	__ASSERT(pPWMOutput_pst->iPWMModule_e > NUM_PWM_MODULE);
	__ASSERT(pPWMOutput_pst->iPWMGenerator_e > NUM_PWM_GENERATOR);
	__ASSERT(pPWMOutput_pst->iPWMOutput_e > NUM_PWM_OUTPUT);

	lPWMBase_u32 = IOHANDLER_PWM_GET_PWM_MODULE_BASE_ADDRESS(pPWMOutput_pst->iPWMModule_e);
	lPWMGenerator_u32 = IOHANDLER_PWM_GET_PWM_GENERATOR_MODULE_ADDRESS(pPWMOutput_pst->iPWMGenerator_e);
	lPWMOutputBit1_u32 = IOHANDLER_PWM_GET_PWM_OUTPUT_BIT(pPWMOutput_pst->iPWMGenerator_e * 2);
	lPWMOutputBit2_u32 = IOHANDLER_PWM_GET_PWM_OUTPUT_BIT(pPWMOutput_pst->iPWMGenerator_e * 2 + 1);

	PWMGenConfigure(lPWMBase_u32, lPWMGenerator_u32, PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC | PWM_GEN_MODE_DBG_RUN);
	PWMOutputInvert(lPWMBase_u32, lPWMOutputBit1_u32, false);
	PWMOutputInvert(lPWMBase_u32, lPWMOutputBit2_u32, false);
	PWMOutputFaultLevel(lPWMBase_u32, lPWMOutputBit1_u32, false);
	PWMOutputFaultLevel(lPWMBase_u32, lPWMOutputBit2_u32, false);

	lPWMPeriod_u32 = sgPWMClock_u32 / pPeriod_u32;

	__ASSERT(lPWMPeriod_u32 > 65535);
	PWMGenPeriodSet(lPWMBase_u32, lPWMGenerator_u32, lPWMPeriod_u32);

	PWMGenEnable(lPWMBase_u32, lPWMGenerator_u32);

	return 0;
}

/**
 * @brief	This function sets the duty cycle on the PWM output
 *
 * @param	pPWMOutput_pst	The PWM output structure
 * @param	pDutyCycle_u8	The duty cycle (0 - 100%)
 *
 * @return	It returns 0 on success
 */
int32_t	ioHandlerPWMSetDutyCycle(IOHandlerPWMOutput* pPWMOutput_pst, uint8_t pDutyCycle_u8) {
	uint32_t lPWMBase_u32 = 0;
	uint32_t lPWMGenerator_u32 = 0;
	uint32_t lPWMOutput_u32 = 0;
	uint32_t lPWMPeriod_u32 = 0;
	float32_t lPWMDutyCycle_f32 = 0;

	__ASSERT(pPWMOutput_pst->iPWMModule_e > NUM_PWM_MODULE);
	__ASSERT(pPWMOutput_pst->iPWMGenerator_e > NUM_PWM_GENERATOR);
	__ASSERT(pPWMOutput_pst->iPWMOutput_e > NUM_PWM_OUTPUT);
	__ASSERT(pDutyCycle_u8 > 100);

	lPWMBase_u32 = IOHANDLER_PWM_GET_PWM_MODULE_BASE_ADDRESS(pPWMOutput_pst->iPWMModule_e);
	lPWMGenerator_u32 = IOHANDLER_PWM_GET_PWM_GENERATOR_MODULE_ADDRESS(pPWMOutput_pst->iPWMGenerator_e);
	lPWMOutput_u32 = IOHANDLER_PWM_GET_PWM_OUTPUT(pPWMOutput_pst->iPWMOutput_e + pPWMOutput_pst->iPWMGenerator_e * 2);

	lPWMPeriod_u32 = PWMGenPeriodGet(lPWMBase_u32, lPWMGenerator_u32);
	lPWMDutyCycle_f32 = ((float) lPWMPeriod_u32) * ((float) pDutyCycle_u8) / 100;
	if(lPWMDutyCycle_f32 < lPWMPeriod_u32) {
		PWMPulseWidthSet(lPWMBase_u32, lPWMOutput_u32, (uint32_t) lPWMDutyCycle_f32);
	} else {
		PWMPulseWidthSet(lPWMBase_u32, lPWMOutput_u32, lPWMPeriod_u32);
	}

	return 0;
}

/**
 * @brief	This function sets the duty cycle on the PWM output
 *
 * @param	pPWMOutput_pst	The PWM output structure
 * @param	pDutyCycle_u16	The duty cycle raw value
 *
 * @return	It returns 0 on success
 */
int32_t	ioHandlerPWMSetRawDutyCycle(IOHandlerPWMOutput* pPWMOutput_pst, uint16_t pDutyCycle_u16) {
	uint32_t lPWMBase_u32 = 0;
	uint32_t lPWMOutput_u32 = 0;

	__ASSERT(pPWMOutput_pst->iPWMModule_e > NUM_PWM_MODULE);
	__ASSERT(pPWMOutput_pst->iPWMGenerator_e > NUM_PWM_GENERATOR);
	__ASSERT(pPWMOutput_pst->iPWMOutput_e > NUM_PWM_OUTPUT);

	lPWMBase_u32 = IOHANDLER_PWM_GET_PWM_MODULE_BASE_ADDRESS(pPWMOutput_pst->iPWMModule_e);
	lPWMOutput_u32 = IOHANDLER_PWM_GET_PWM_OUTPUT(pPWMOutput_pst->iPWMOutput_e + pPWMOutput_pst->iPWMGenerator_e * 2);

	PWMPulseWidthSet(lPWMBase_u32, lPWMOutput_u32, pDutyCycle_u16);

	return 0;
}

/**
 * @brief	This function enables or disables the PWM output
 *
 * @param	pPWMOutput_pst	The PWM output structure
 * @param	pState_b		The enable state
 *
 * @return	It returns 0 on success
 */
int32_t ioHandlerPWMSetOutputState(IOHandlerPWMOutput* pPWMOutput_pst, bool pState_b) {
	uint32_t lPWMBase_u32 = 0;
	uint32_t lPWMOutputBit_u32 = 0;

	__ASSERT(pPWMOutput_pst->iPWMModule_e > NUM_PWM_MODULE);
	__ASSERT(pPWMOutput_pst->iPWMGenerator_e > NUM_PWM_GENERATOR);
	__ASSERT(pPWMOutput_pst->iPWMOutput_e > NUM_PWM_OUTPUT);

	lPWMBase_u32 = IOHANDLER_PWM_GET_PWM_MODULE_BASE_ADDRESS(pPWMOutput_pst->iPWMModule_e);
	lPWMOutputBit_u32 = IOHANDLER_PWM_GET_PWM_OUTPUT_BIT(pPWMOutput_pst->iPWMOutput_e + pPWMOutput_pst->iPWMGenerator_e  * 2);

	PWMOutputState(lPWMBase_u32, lPWMOutputBit_u32, pState_b);

	return 0;
}

/**
 * @brief	This function returns the PWM clock frequency
 *
 * @return	The PWM clock frequency
 */
uint32_t ioHandlerPWMGetPWMClockFrequency(void) {
	return sgPWMClock_u32;
}

/**
 * @brief	This function returns the PWM generator period
 *
 * @param	pPWMOutput_pst	The PWM output structure
 *
 * @return	The PWM generator period
 */
uint32_t ioHandlerPWMGetPWMPeriod(IOHandlerPWMOutput* pPWMOutput_pst) {
	uint32_t lPWMBase_u32 = 0;
	uint32_t lPWMGenerator_u32 = 0;

	__ASSERT(pPWMOutput_pst->iPWMModule_e > NUM_PWM_MODULE);
	__ASSERT(pPWMOutput_pst->iPWMGenerator_e > NUM_PWM_GENERATOR);

	lPWMBase_u32 = IOHANDLER_PWM_GET_PWM_MODULE_BASE_ADDRESS(pPWMOutput_pst->iPWMModule_e);
	lPWMGenerator_u32 = IOHANDLER_PWM_GET_PWM_GENERATOR_MODULE_ADDRESS(pPWMOutput_pst->iPWMGenerator_e);

	return PWMGenPeriodGet(lPWMBase_u32, lPWMGenerator_u32);
}
