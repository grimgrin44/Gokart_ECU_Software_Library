/**
 * @file	IOHandlerCAN.h
 * @authors	Krisztian Enisz, Gabor Kormendy
 * @date	2015. 05. 12.
 * @brief	Contains the function and variable declaration for the low-level CAN I/O functions.
 */

#ifndef IOHANDLER_PWM_IOHANDLERPWM_H_
#define IOHANDLER_PWM_IOHANDLERPWM_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <driverlib/sysctl.h>
#include <driverlib/pwm.h>
#include <driverlib/gpio.h>

#include "../IOHandlerDefs.h"

#include "../../ECUConfiguration.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

/// Enable PWM module 0 generator 0
#define IOHANDLER_PWM_MODULE_0_PWM_GENERATOR_0_ENABLE		ECU_CONFIGURATION_PWM_MODULE_0_PWM_GENERATOR_0_ENABLE
/// Enable PWM module 0 generator 1
#define IOHANDLER_PWM_MODULE_0_PWM_GENERATOR_1_ENABLE		ECU_CONFIGURATION_PWM_MODULE_0_PWM_GENERATOR_1_ENABLE
/// Enable PWM module 0 generator 2
#define IOHANDLER_PWM_MODULE_0_PWM_GENERATOR_2_ENABLE		ECU_CONFIGURATION_PWM_MODULE_0_PWM_GENERATOR_2_ENABLE
/// Enable PWM module 0 generator 3
#define IOHANDLER_PWM_MODULE_0_PWM_GENERATOR_3_ENABLE		ECU_CONFIGURATION_PWM_MODULE_0_PWM_GENERATOR_3_ENABLE
/// Enable PWM module 1 generator 0
#define IOHANDLER_PWM_MODULE_1_PWM_GENERATOR_0_ENABLE		ECU_CONFIGURATION_PWM_MODULE_1_PWM_GENERATOR_0_ENABLE
/// Enable PWM module 1 generator 1
#define IOHANDLER_PWM_MODULE_1_PWM_GENERATOR_1_ENABLE		ECU_CONFIGURATION_PWM_MODULE_1_PWM_GENERATOR_1_ENABLE
/// Enable PWM module 1 generator 2
#define IOHANDLER_PWM_MODULE_1_PWM_GENERATOR_2_ENABLE		ECU_CONFIGURATION_PWM_MODULE_1_PWM_GENERATOR_2_ENABLE
/// Enable PWM module 1 generator 3
#define IOHANDLER_PWM_MODULE_1_PWM_GENERATOR_3_ENABLE		ECU_CONFIGURATION_PWM_MODULE_1_PWM_GENERATOR_3_ENABLE
/// Use PD0 as M0PWM6 output
#define IOHANDLER_PWM_MODULE_0_OUTPUT_6_ALTERNATE			ECU_CONFIGURATION_PWM_MODULE_0_OUTPUT_6_ALTERNATE
/// Use PD1 as M0PWM7 output
#define IOHANDLER_PWM_MODULE_0_OUTPUT_7_ALTERNATE			ECU_CONFIGURATION_PWM_MODULE_0_OUTPUT_7_ALTERNATE
/// Use PE4 as M1PWM2 output
#define IOHANDLER_PWM_MODULE_1_OUTPUT_2_ALTERNATE			ECU_CONFIGURATION_PWM_MODULE_1_OUTPUT_2_ALTERNATE
/// Use PE5 as M1PWM2 output
#define IOHANDLER_PWM_MODULE_1_OUTPUT_3_ALTERNATE			ECU_CONFIGURATION_PWM_MODULE_1_OUTPUT_3_ALTERNATE

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

enum PWMModule {
	PWM_MODULE_0,
	PWM_MODULE_1,
	NUM_PWM_MODULE
};

enum PWMGenerator {
	PWM_GENERATOR_0 = 0,
	PWM_GENERATOR_1,
	PWM_GENERATOR_2,
	PWM_GENERATOR_3,
	NUM_PWM_GENERATOR
};

enum PWMOutput {
	PWM_OUTPUT_1 = 0,
	PWM_OUTPUT_2,
	NUM_PWM_OUTPUT
};

typedef struct IOHandlerPWMOutput {
	/// The PWM module
	enum PWMModule iPWMModule_e;
	/// The PWM generator
	enum PWMGenerator iPWMGenerator_e;
	/// The PWM output
	enum PWMOutput iPWMOutput_e;
} IOHandlerPWMOutput;


// ----------------------------------------------------------------------------
// Variable declaration.
// ----------------------------------------------------------------------------



// ----------------------------------------------------------------------------
// Function declaration.
// ----------------------------------------------------------------------------

GOKART_API	void		ioHandlerInitPWMModules(void);
GOKART_API	int32_t		ioHandlerPWMInitModule(IOHandlerPWMOutput* pPWMOutput_pst, uint32_t pPeriod_u32);
GOKART_API	int32_t 	ioHandlerPWMSetPWMPeriod(IOHandlerPWMOutput* pPWMOutput_pst, uint32_t pPeriod_u32);
GOKART_API	int32_t		ioHandlerPWMSetDutyCycle(IOHandlerPWMOutput* pPWMOutput_pst, uint8_t pDutyCycle_u8);
GOKART_API	int32_t		ioHandlerPWMSetRawDutyCycle(IOHandlerPWMOutput* pPWMOutput_pst, uint16_t pDutyCycle_u16);
GOKART_API	int32_t 	ioHandlerPWMSetOutputState(IOHandlerPWMOutput* pPWMOutput_pst, bool pState_b);
GOKART_API	uint32_t	ioHandlerPWMGetPWMClockFrequency(void);
GOKART_API	uint32_t	ioHandlerPWMGetPWMPeriod(IOHandlerPWMOutput* pPWMOutput_pst);

#endif /* IOHANDLER_PWM_IOHANDLERPWM_H_ */
