/**
 * @file	IOHandlerCAN.c
 * @authors	Krisztian Enisz, Gabor Kormendy
 * @date	2015. 05. 12.
 * @brief	Contains the function and variable definitions for the low-level CAN I/O functions.
 */

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------
#include "IOHandlerCAN.h"

#if (ECU_CONFIGURATION_ENABLE_CAN == TRUE)

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------
/// Max number of CAN message handlers
#define IOHANDLER_CAN_RX_MESSAGE_HANDLER_NUM		16

// ----------------------------------------------------------------------------
// Variable definitions.
// ----------------------------------------------------------------------------

/// Array for the CAN RX message IDs
static uint32_t sgCANRxMessageIDs_pu32[] = IOHANDLER_CAN_RX_MSG_IDS;
/// Array for the CAN RX message DLCs
static uint8_t sgCANRxMessageDLCs_pu8[] = IOHANDLER_CAN_RX_MSG_DLC;
/// Number of the CAN RX messages
static uint8_t sgCANRxMessageCount_u8 = (sizeof(sgCANRxMessageIDs_pu32) / sizeof(uint32_t));
/// Message buffer for the RX messages on the CAN interface
static IOHandlerCANMessage sgCANRxMessageBuffer_pst[IOHANDLER_CAN_RX_BUFFER_SIZE];
/// Message buffer for the TX messages on the CAN interface
static IOHandlerCANMessage sgCANTxMessageBuffer_pst[IOHANDLER_CAN_TX_BUFFER_SIZE];
/// Message buffer object for the RX messages on the CAN interface

static IOHandlerCANMessageBuffer sgCANRxMessageBuffer_st;
/// Message buffer object for the TX messages on the CAN interface
static IOHandlerCANMessageBuffer sgCANTxMessageBuffer_st;
/// Received message handler functions
static IOHandlerCANMessageHandlerFunction sgCANRxMessageHandler_pf[IOHANDLER_CAN_RX_MESSAGE_HANDLER_NUM];
/// Active received message handler function counter
static uint8_t sgCANRxMessageHandlerNum_u8 = 0;
/// Mask for the free transmit CAN message objects
static volatile uint32_t sgCANFreeCANMessageObjects_u32 = 0x000000FF;
/// Signs that we received a frame
static volatile bool sgCANMessageReceived_b = false;
/// Signs that we transmitted a frame
static volatile bool sgCANMessageTransmitted_b = false;



// ----------------------------------------------------------------------------
// function definitions.
// ----------------------------------------------------------------------------

/**
 * @brief		Initializes the CAN messages.
 * @details 	This function initialize the CAN messages in the CAN controller and
 * 				in the RX and TX buffers. This function have to be called in the
 * 				ioHandlerInitCAN function after the CAN controller initialization.
 * 				@see IOHANDLER_CAN_RX_MSG_IDS
 * 				@see IOHANDLER_CAN_RX_MSG_DLC
 * @return		none
 */
static void ioHandlerCANMessageInit(void) {
	// CAN message object for initialization
	tCANMsgObject lCANMessageRxObject_st = { 0 };
	// The index variable (1)
	uint32_t lIndex_u32;

	// Add CAN RX messages to the controller
	lCANMessageRxObject_st.ui32MsgIDMask = IOHANDLER_CAN_RX_MSG_FILTER;
	lCANMessageRxObject_st.ui32Flags = MSG_OBJ_RX_INT_ENABLE | MSG_OBJ_USE_ID_FILTER;

	for (lIndex_u32 = 0; lIndex_u32 < sgCANRxMessageCount_u8; lIndex_u32++) {
		lCANMessageRxObject_st.ui32MsgID = sgCANRxMessageIDs_pu32[lIndex_u32];
		lCANMessageRxObject_st.ui32MsgLen = sgCANRxMessageDLCs_pu8[lIndex_u32];
		CANMessageSet(IOHANDLER_CAN_BASE, lIndex_u32 + 1, &lCANMessageRxObject_st, MSG_OBJ_TYPE_RX);
	}

	for (lIndex_u32 = 0; lIndex_u32 < IOHANDLER_CAN_RX_BUFFER_SIZE; lIndex_u32++) {
		memset(&sgCANRxMessageBuffer_pst[lIndex_u32], 0, sizeof(IOHandlerCANMessage));
	}

	for (lIndex_u32 = 0; lIndex_u32 < IOHANDLER_CAN_TX_BUFFER_SIZE; lIndex_u32++) {
		memset(&sgCANRxMessageBuffer_pst[lIndex_u32], 0, sizeof(IOHandlerCANMessage));
	}
}



/**
 * @brief	This function handles the CAN controller status interrupts.
 * @details	This function is for future usage, and extended error
 * 			handling can be enabled by defining
 * 			\bIOHANDLER_CAN_EXTENDED_ERROR_HANDLING.
 * @param	pStatus_u32		Status code of the CAN controller
 * @return 	none
 */
static void ioHandlerCANStatusInterruptHandler(uint32_t pStatus_u32) {

	if(pStatus_u32 & CAN_STATUS_RXOK) {
		// We received a CAN frame
		sgCANMessageReceived_b = true;
	}

	if(pStatus_u32 & CAN_STATUS_TXOK) {
		// We succesfully transmitted a frame
		sgCANMessageTransmitted_b = true;
	}

	// Error status flags
	if(pStatus_u32 & CAN_STATUS_BUS_OFF) {
		// Controller is in bus-off condition
		while(1) {

		}

	}

	if(pStatus_u32 & CAN_STATUS_EWARN) {
		// An error counter has reached a limit of at least 96

	}

	if(pStatus_u32 & CAN_STATUS_EPASS) {
		// CAN controller is in the error passive state
		// Reinitialize CAN controller
		SysCtlPeripheralReset(IOHANDLER_PERIPH_CAN);
		ioHandlerCANMessageBufferFlush(&sgCANTxMessageBuffer_st);
		sgCANFreeCANMessageObjects_u32 = 0x0000FFFF;
		ioHandlerInitCAN();
	}

#ifdef IOHANDLER_CAN_EXTENDED_ERROR_HANDLING

	switch(pStatus_u32 & CAN_STATUS_LEC_MASK) {

	case CAN_STATUS_LEC_NONE:
		// There was no error
		break;

	case CAN_STATUS_LEC_STUFF:
		// A bit stuffing error has occured
		break;

	case CAN_STATUS_LEC_FORM:
		// A formatting error has occured
		break;

	case CAN_STATUS_LEC_ACK:
		// An acknowledge error has occured
		break;

	case CAN_STATUS_LEC_BIT1:
		// The bus remained a bit level of 1 for longer than is allowed
		break;

	case CAN_STATUS_LEC_BIT0:
		// The bus remained a bit level of 0 for longer than is allowed
		break;

	case CAN_STATUS_LEC_CRC:
		// A CRC error has occured
		break;

	}

#endif // IOHANDLER_CAN_EXTENDED_ERROR_HANDLING

}

/**
 * @brief	Function which reads the received CAN message and puts it in the message buffer
 * @param 	pIntStatus_u64 			The interrupt status of the CAN controller
 * @return 	The interrupt or the error status
 */
static int32_t ioHandlerReadMessage(uint64_t pIntStatus_u64) {
	// Return value
	int32_t lRetVal_i32 = 0;
	// Array with zeros for the initialization of the CAN message object
	uint8_t lCANMessageDataBuffer_pu8[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
	// The CAN message object
	tCANMsgObject lCANMessageObject_st = { 0 };
	// Initializing the data field in the CAN message object
	lCANMessageObject_st.pui8MsgData = lCANMessageDataBuffer_pu8;
	// Get the received message (pIntStatus_u64 is the message object index)
	CANMessageGet(IOHANDLER_CAN_BASE, pIntStatus_u64, &lCANMessageObject_st, true);

	lRetVal_i32 = ioHandlerCANMessageBufferPut(&sgCANRxMessageBuffer_st, lCANMessageObject_st.ui32MsgID, lCANMessageDataBuffer_pu8, lCANMessageObject_st.ui32MsgLen);

	if(0 != lRetVal_i32) {
		// If we didn't find any empty space in the buffer, return with the error code
		return ERROR_HANDLER_CAN_RX_BUFFER_IS_FULL_ERROR_CODE;
	}
	return 0;
}


/**
 * @brief	Function which shall be called on CAN interrupt.
 * @details	The application must take care of calling this function
 * 			in the CAN interrupt handler. It can't be placed directly
 * 			into the interrupt vector table.
 * @return 	The interrupt status.
 */
static int64_t ioHandlerOnCANInterrupt(void) {

	// The CAN controller interrupt status
	uint64_t lIntStatus_u64 = 0;

	tCANMsgObject lMessageObject_st = { 0 };
	uint8_t lData_pu8[8] = { 0, 0, 0, 0, 0, 0, 0, 0};
	lMessageObject_st.pui8MsgData = lData_pu8;
	// Read the CAN interrupt status to find the cause of the interrupt
	lIntStatus_u64 = CANIntStatus(IOHANDLER_CAN_BASE, CAN_INT_STS_CAUSE);

	if (CAN_INT_INTID_STATUS == lIntStatus_u64) {
		// If the cause is a controller status interrupt, then get the status.
		// Read the controller status.  This will return a field of status
		// error bits that can indicate various errors.  Error processing
		// The act of reading this status will clear the interrupt.
		lIntStatus_u64 = CANStatusGet(IOHANDLER_CAN_BASE, CAN_STS_CONTROL);
		// Set a flag to indicate some errors may have occurred.
		ioHandlerCANStatusInterruptHandler(lIntStatus_u64);
		// Return with the interrupt status for custom error handling
		return lIntStatus_u64;
	} else if ((lIntStatus_u64 > 0) && (lIntStatus_u64 <= 32)) {
		// Get the received CAN message.
		if(lIntStatus_u64 <= 16) {
			return ioHandlerReadMessage(lIntStatus_u64);
		} else if(lIntStatus_u64 > 16) {
			CANMessageGet(IOHANDLER_CAN_BASE, lIntStatus_u64, &lMessageObject_st, true);
			sgCANFreeCANMessageObjects_u32 |= (1 << (lIntStatus_u64 - 17));
			return 0;
		}
	}
	SysCtlDelay(100);
	return lIntStatus_u64;
}



/**
 * @brief	Interrupt handler for the CAN module
 * @return	none
 */
void __INTERRUPT_HANDLER__ioHandlerOnCANInterruptHandler(void) {
	ioHandlerOnCANInterrupt();
}


/**
 * @brief		Initialize the CAN interface.
 * @details		This function initialize the CAN controller and the peripherials needed.
 * 				IOHandlerCAN.h contains macro definitions for configuring the CAN controller.
 * 				It must be configured properly according to the application.
 * @return 		none
 */
void ioHandlerInitCAN(void) {
	// The GPIO port and pins have been set up for CAN.
	// The CAN peripheral must be enabled.
	MAP_SysCtlPeripheralEnable(IOHANDLER_PERIPH_CAN);

	// Enable the GPIO port for the CAN interface.
	MAP_SysCtlPeripheralEnable(IOHANDLER_PERIPH_GPIO_CAN_RX);
	MAP_SysCtlPeripheralEnable(IOHANDLER_PERIPH_GPIO_CAN_TX);

	// Configure the GPIO pin muxing to select CAN functions for these pins.
	// This step selects which alternate function is available for these pins.
	// This is necessary if your part supports GPIO pin function muxing.
	MAP_GPIOPinConfigure(IOHANDLER_GPIO_CANRX);
	MAP_GPIOPinConfigure(IOHANDLER_GPIO_CANTX);

	// Enable the alternate function on the GPIO pins.  The above step selects
	// which alternate function is available.  This step actually enables the
	// alternate function instead of GPIO for these pins.
	MAP_GPIOPinTypeCAN(IOHANDLER_GPIO_PORT_BASE_CAN_RX, IOHANDLER_GPIO_PIN_RX_CAN);
	MAP_GPIOPinTypeCAN(IOHANDLER_GPIO_PORT_BASE_CAN_TX, IOHANDLER_GPIO_PIN_TX_CAN);

	// Initialize the CAN controller
	CANInit(IOHANDLER_CAN_BASE);

	// Set up the bit rate for the CAN bus.  This function sets up the CAN
	// bus timing for a nominal configuration.  You can achieve more control
	// over the CAN bus timing by using the function CANBitTimingSet() instead
	// of this one, if needed.
	CANBitRateSet(IOHANDLER_CAN_BASE, SysCtlClockGet(),
	IOHANDLER_CAN_BITRATE);

#if (ECU_CONFIGURATION_ENABLE_INT_HANDLER_REGISTRATION == TRUE)
	CANIntRegister(IOHANDLER_CAN_BASE, __INTERRUPT_HANDLER__ioHandlerOnCANInterruptHandler);
#endif

	// Enable interrupts on the CAN peripheral.  In this case the software uses static
	// allocation of interrupt handlers which means the name of the handler
	// is in the vector table of startup code.  If you want to use dynamic
	// allocation of the vector table, then you must also call CANIntRegister()
	// here.
	CANIntEnable(IOHANDLER_CAN_BASE,
	CAN_INT_MASTER | CAN_INT_ERROR | CAN_INT_STATUS);

	// Enable the CAN interrupt on the processor (NVIC).
	IntEnable(IOHANDLER_INT_CAN);

	ioHandlerCANMessageInit();

	ioHandlerCANMessageBufferInit(&sgCANRxMessageBuffer_st, sgCANRxMessageBuffer_pst, IOHANDLER_CAN_RX_BUFFER_SIZE);
	ioHandlerCANMessageBufferInit(&sgCANTxMessageBuffer_st, sgCANTxMessageBuffer_pst, IOHANDLER_CAN_TX_BUFFER_SIZE);

	// Enable the CAN for operation.
	CANEnable(IOHANDLER_CAN_BASE);
}

/**
 * @brief	This function checks the CAN controller full status, to see if the controller is in the bus off state.
 *
 * @return	It returns true if the CAN controller is in bus off state
 */
bool ioHandlerCANInBusOffState(void) {
	if(CANStatusGet(IOHANDLER_CAN_BASE, CAN_STS_CONTROL) & CAN_STATUS_BUS_OFF) {
		return true;
	}
	return false;
}

/**
 * @brief	This function clears the bus off state in the controller. This is a blocking function.
 *
 * @return	none
 */
void ioHandlerCANRecoverFromBusOff(void) {
	CANEnable(IOHANDLER_CAN_BASE);
	while(true == ioHandlerCANInBusOffState()) {
		// Wait for clear
	}
}


/**
 * @brief	This function puts a new message to the TX message buffer
 * @details	Searches for the first invalid message in the TX buffer and fill it up. If
 * 			no empty space found, it will return with ERROR_HANDLER_CAN_TX_BUFFER_IS_FULL_ERROR_CODE
 * 			error code.
 * 			@see IOHANDLER_CAN_TX_BUFFER_SIZE
 * 			@see gCANTxMessageBuffer_pst
 * @param 	pMessageId_pu32 		The message ID
 * @param 	pMessageData_pu8 		The message data
 * @param 	pMessageDataLength_u8 	The lenght of the message
 * @return 	It returns 0 (no error) or the error code
 */
int32_t ioHandlerCANSendMessageRaw(uint32_t pMessageId_u32, uint8_t* pMessageData_pu8, uint8_t pMessageDataLength_u8) {
	// Return value
	int32_t lRetVal_i32 = 0;
	// Place message into the buffer
	lRetVal_i32 = ioHandlerCANMessageBufferPut(&sgCANTxMessageBuffer_st, pMessageId_u32, pMessageData_pu8, pMessageDataLength_u8);
	return lRetVal_i32;
}


/**
 * @brief	This function puts a new message to the TX message buffer
 * @param	pBuffer_pst		The CAN message buffer
 * @return	It returns 0 on success or with an error code
 */
int32_t ioHandlerCANSendMessage(IOHandlerCANMessage* pBuffer_pst) {
	return ioHandlerCANMessageBufferPutMessage(&sgCANTxMessageBuffer_st, pBuffer_pst);
}


/**
 * @brief	Sends every CAN frame from the buffer to the bus.
 * @details	Loops through the gCANTxMessageBuffer_pst and sends every
 * 			valid frame on the CAN bus. Transmit interrupt is enabled, but
 * 			there is no usage of it.
 * 			@see IOHANDLER_CAN_TX_BUFFER_SIZE
 * 			@see sgCANTxMessageBuffer_pst
 * @return 	none
 */
void ioHandlerCANHandleTxMessages(void) {
	// The index variable;
	uint32_t lIndex_u32 = 0;
	// The mask variable
	uint32_t lMask_u32 = 1;
	// The transmitted CAN message.
	tCANMsgObject lMsgObjectTx_st = { 0 };
	// Message object
	IOHandlerCANMessage lCANMessage_st = { 0 };
	// Messages to send
	uint32_t lMessageCount_u32 = 0;

	lMessageCount_u32 = ioHandlerCANMessageBufferGetCount(&sgCANTxMessageBuffer_st);

	// Disable interrupt
	//IntDisable(IOHANDLER_INT_CAN);

	for(lIndex_u32 = 17; (lIndex_u32 <= 32) && (lMessageCount_u32 > 0); lIndex_u32++) {
		if(sgCANFreeCANMessageObjects_u32 & lMask_u32) {
			ioHandlerCANMessageBufferGetMessage(&sgCANTxMessageBuffer_st, &lCANMessage_st);
			lMsgObjectTx_st.ui32MsgID = lCANMessage_st.iID_u32;
			lMsgObjectTx_st.ui32MsgLen = lCANMessage_st.iDLC_u8;
			lMsgObjectTx_st.pui8MsgData = lCANMessage_st.iData;
			lMsgObjectTx_st.ui32Flags = MSG_OBJ_TX_INT_ENABLE;
			CANMessageSet(IOHANDLER_CAN_BASE, lIndex_u32, &lMsgObjectTx_st, MSG_OBJ_TYPE_TX);
			lMessageCount_u32--;
			sgCANFreeCANMessageObjects_u32 &= ~lMask_u32;
		}
		lMask_u32 = lMask_u32 << 1;
	}

	// Enable interrupt
	//IntEnable(IOHANDLER_INT_CAN);
}

/**
 * @brief	This function adds the passed filter and parser function to the parser list
 *
 * @param	pParserFunction_pf		Function pointer to the parser function
 *
 * @return	It returns 0 on success, otherwise -1.
 */
int32_t	ioHandlerCANAddMessageHandler(IOHandlerCANMessageHandlerFunction pParserFunction_pf) {
	if(NULL != pParserFunction_pf && sgCANRxMessageHandlerNum_u8 < IOHANDLER_CAN_RX_MESSAGE_HANDLER_NUM) {
		sgCANRxMessageHandler_pf[sgCANRxMessageHandlerNum_u8] = pParserFunction_pf;
		sgCANRxMessageHandlerNum_u8++;
		return 0;
	} else {
		return -1;
	}
}

/**
 * @brief	This function parses the received CAN message buffer based on the given handlers
 * @param	none
 * @return 	none
 */
void ioHandlerCANHandleRxMessages(void) {
	uint32_t lIndex_u32 = 0;
	IOHandlerCANMessage lCANMessage_st = { 0 };
	while(ioHandlerCANMessageBufferGetMessage(&sgCANRxMessageBuffer_st, &lCANMessage_st) != -1) {
		for(lIndex_u32 = 0; lIndex_u32 < sgCANRxMessageHandlerNum_u8; lIndex_u32++) {
			if(0 == sgCANRxMessageHandler_pf[lIndex_u32](&lCANMessage_st)) {
				// Message parsed
				break;
			}
		}
	}
}

/**
 * @brief	This function reads a message from the rx CAN message buffer
 * @param	pBuffer_pst		CAN message buffer
 * @return	It returns 0 on success, or -1 if the buffer is empty
 */
int32_t ioHandlerCANReadMessage(IOHandlerCANMessage* pBuffer_pst) {
	return ioHandlerCANMessageBufferGetMessage(&sgCANRxMessageBuffer_st, pBuffer_pst);
}


/*********************************************************
 *
 * Message buffer functions
 *
 *********************************************************/

/*
 * @brief	Initializes the CAN message buffer object
 * @param	pMessageBuffer_pst		The message buffer object
 * @param	pBuffer_pst				The CAN message buffer
 * @param	pBufferSize_u32			The CAN message buffer size
 * @return	It returns 0 on success else with an error code
 */
int32_t	ioHandlerCANMessageBufferInit(IOHandlerCANMessageBuffer* pMessageBuffer_pst, IOHandlerCANMessage* pBuffer_pst, uint32_t pBufferSize_u32) {
	pMessageBuffer_pst->iCANMessageBuffer_pst = pBuffer_pst;
	pMessageBuffer_pst->iBufferSize_u32 = pBufferSize_u32;
	ioHandlerCANMessageBufferFlush(pMessageBuffer_pst);
	return 0;
}

/*
 * @brief	Flushes the message buffer object by setting the head and tail pointers to their default
 * 			value. Also sets the counter to zero.
 * @param	pMessageBuffer_pst		The message buffer object
 * @return	It returns 0 on success else with an error code
 */
int32_t	ioHandlerCANMessageBufferFlush(IOHandlerCANMessageBuffer* pMessageBuffer_pst) {
	pMessageBuffer_pst->iHead_u32 = 0;
	pMessageBuffer_pst->iTail_u32 = 0;
	pMessageBuffer_pst->iCounter_u32 = 0;
	return 0;
}

/*
 * @brief	Returns the number of messages in the message buffer object
 * @param	pMessageBuffer_pst		The message buffer object
 * @return	It returns the number of messages in the buffer object
 */
int32_t ioHandlerCANMessageBufferGetCount(IOHandlerCANMessageBuffer* pMessageBuffer_pst) {
	return pMessageBuffer_pst->iCounter_u32;
}

/*
 * @brief	Places the given message into the message buffer object if there is available space
 * @param	pMessageBuffer_pst		The message buffer object
 * @param	pMessage_pst			The CAN message
 * @return	It returns 0 on success else with an error code
 */
int32_t	ioHandlerCANMessageBufferPutMessage(IOHandlerCANMessageBuffer* pMessageBuffer_pst, IOHandlerCANMessage* pMessage_pst) {
	if(pMessageBuffer_pst->iBufferSize_u32 != pMessageBuffer_pst->iCounter_u32) {
		memcpy(&pMessageBuffer_pst->iCANMessageBuffer_pst[pMessageBuffer_pst->iHead_u32], pMessage_pst, sizeof(IOHandlerCANMessage));
		pMessageBuffer_pst->iCounter_u32++;
		pMessageBuffer_pst->iHead_u32 = (pMessageBuffer_pst->iHead_u32 + 1) % pMessageBuffer_pst->iBufferSize_u32;
		return 0;
	} else {
		// Buffer is full
		return -1;
	}
}

/*
 * @brief	Gets a CAN message from the given buffer object if there is any
 * @param	pMessageBuffer_pst		The message buffer object
 * @param	pBuffer_pst				The CAN message buffer
 * @return	It returns 0 on success else with an error code
 */
int32_t ioHandlerCANMessageBufferGetMessage(IOHandlerCANMessageBuffer* pMessageBuffer_pst, IOHandlerCANMessage* pBuffer_pst) {
	if(0 != pMessageBuffer_pst->iCounter_u32) {
		memcpy(pBuffer_pst, &pMessageBuffer_pst->iCANMessageBuffer_pst[pMessageBuffer_pst->iTail_u32], sizeof(IOHandlerCANMessage));
		pMessageBuffer_pst->iCounter_u32--;
		pMessageBuffer_pst->iTail_u32 = (pMessageBuffer_pst->iTail_u32 + 1) % pMessageBuffer_pst->iBufferSize_u32;
		return 0;
	} else {
		// Buffer is empty
		return -1;
	}
}

/*
 * @brief	Places a CAN message into the given message buffer object
 * @param	pMessageBuffer_pst		The message buffer object
 * @param	pID_u32					ID of the CAN message
 * @param	pBuffer_pu8				The CAN message payload
 * @param	pDLC_u8					The length of the CAN message payload
 * @return	It returns 0 on success else with an error code
 */
int32_t ioHandlerCANMessageBufferPut(IOHandlerCANMessageBuffer* pMessageBuffer_pst, uint32_t pID_u32, uint8_t* pBuffer_pu8, uint8_t pDLC_u8) {
	if(pMessageBuffer_pst->iBufferSize_u32 != pMessageBuffer_pst->iCounter_u32) {
		IOHandlerCANMessage* lCANMessage_pst = &pMessageBuffer_pst->iCANMessageBuffer_pst[pMessageBuffer_pst->iHead_u32];
		lCANMessage_pst->iID_u32 = pID_u32;
		lCANMessage_pst->iDLC_u8 = pDLC_u8;
		memcpy(lCANMessage_pst->iData, pBuffer_pu8, pDLC_u8);
		pMessageBuffer_pst->iCounter_u32++;
		pMessageBuffer_pst->iHead_u32 = (pMessageBuffer_pst->iHead_u32 + 1) % pMessageBuffer_pst->iBufferSize_u32;
		return 0;
	} else {
		// Buffer is full
		return -1;
	}
}

#endif //(ECU_CONFIGURATION_ENABLE_CAN == TRUE)
