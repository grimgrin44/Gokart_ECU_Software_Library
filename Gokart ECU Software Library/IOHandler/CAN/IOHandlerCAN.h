/**
 * @file	IOHandlerCAN.h
 * @authors	Krisztian Enisz, Gabor Kormendy
 * @date	2015. 05. 12.
 * @brief	Contains the function and variable declaration for the low-level CAN I/O functions.
 */

#ifndef IOHANDLER_CAN_IOHANDLERCAN_H__
#define IOHANDLER_CAN_IOHANDLERCAN_H__

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#if (ECU_CONFIGURATION_ENABLE_CAN == TRUE)

#include "../../ErrorHandler/ErrorHandler.h"

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "inc/hw_can.h"
#include "inc/hw_ints.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom_map.h"
#include "driverlib/gpio.h"
#include "driverlib/can.h"
#include "driverlib/interrupt.h"

#include "../../ECUConfiguration.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------


/// The hardware peripherial for the IOHandler CAN port.
#define IOHANDLER_PERIPH_CAN				ECU_CONFIGURATION_PERIPH_CAN
/// The GPIO port for the IOHandler CAN TX port.
#define IOHANDLER_PERIPH_GPIO_CAN_RX		ECU_CONFIGURATION_PERIPH_GPIO_CAN_RX
/// The GPIO port for the IOHandler CAN RX port
#define IOHANDLER_PERIPH_GPIO_CAN_TX		ECU_CONFIGURATION_PERIPH_GPIO_CAN_TX
/// The GPIO peripherial settings for the IOHandler CAN port function multiplexing (RX).
#define IOHANDLER_GPIO_CANRX				ECU_CONFIGURATION_GPIO_CANRX
/// The GPIO peripherial settings for the IOHandler CAN port function multiplexing (TX).
#define IOHANDLER_GPIO_CANTX				ECU_CONFIGURATION_GPIO_CANTX
/// The GPIO base port for the IOHandler CAN RX.
#define IOHANDLER_GPIO_PORT_BASE_CAN_RX		ECU_CONFIGURATION_GPIO_PORT_BASE_CAN_RX
/// The GPIO base port for the IOHandler CAN TX.
#define IOHANDLER_GPIO_PORT_BASE_CAN_TX 	ECU_CONFIGURATION_GPIO_PORT_BASE_CAN_TX
/// The GPIO port pins for the IOHandler CAN (RX).
#define IOHANDLER_GPIO_PIN_RX_CAN			ECU_CONFIGURATION_GPIO_PIN_RX_CAN
/// The GPIO port pins for the IOHandler CAN (TX).
#define IOHANDLER_GPIO_PIN_TX_CAN			ECU_CONFIGURATION_GPIO_PIN_TX_CAN
/// The base for the IOHandler CAN.
#define IOHANDLER_CAN_BASE					ECU_CONFIGURATION_CAN_BASE
/// The NVIC interrupt for the CAN interface.
#define IOHANDLER_INT_CAN					ECU_CONFIGURATION_INT_CAN
/// The baudrate for the CAN interface (RX and TX).
#define IOHANDLER_CAN_BITRATE				ECU_CONFIGURATION_CAN_BITRATE
/// The CAN message filter mask for the received frames.
#define IOHANDLER_CAN_RX_MSG_FILTER			ECU_CONFIGURATION_CAN_RX_MSG_FILTER
/// Tha maximum number of messages that the CAN controller can handle
#define IOHANDLER_CAN_MAX_MESSAGE_OBJ		ECU_CONFIGURATION_CAN_MAX_MESSAGE_OBJ

/// The IDs of the received CAN messages (the count of the messages can not exceed 16).
#define IOHANDLER_CAN_RX_MSG_IDS			ECU_CONFIGURATION_CAN_RX_MSG_IDS
/// The DLC of the received CAN messages (the count of the messages can not exceed 16). It must be in sync with the IDs above.
#define IOHANDLER_CAN_RX_MSG_DLC			ECU_CONFIGURATION_CAN_RX_MSG_DLC

/// The size of the CAN RX buffer
#define IOHANDLER_CAN_RX_BUFFER_SIZE		ECU_CONFIGURATION_CAN_RX_BUFFER_SIZE
/// The size of the CAN TX buffer
#define IOHANDLER_CAN_TX_BUFFER_SIZE		ECU_CONFIGURATION_CAN_TX_BUFFER_SIZE

// This enables the extended error detection in the controller status interrupt
//#define IOHANDLER_CAN_EXTENDED_ERROR_HANDLING

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

/**
 * @brief	Structure for the received and transmitted CAN frames
 */
typedef struct IOHandlerCANMessage {
	/// The data length counter.
	uint8_t iDLC_u8;
	/// The message ID.
	uint32_t iID_u32;
	/// The data.
	uint8_t iData[8];
} IOHandlerCANMessage;

typedef struct IOHandlerCANMessageBuffer {
	/// Pointer to the buffer
	IOHandlerCANMessage* iCANMessageBuffer_pst;
	/// Size of the buffer
	uint32_t iBufferSize_u32;
	/// Head pointer
	uint32_t iHead_u32;
	/// Tail pointer
	uint32_t iTail_u32;
	/// Message counter
	uint32_t iCounter_u32;
} IOHandlerCANMessageBuffer;

/// CAN message parser function type
typedef int32_t (*IOHandlerCANMessageHandlerFunction)(IOHandlerCANMessage*);

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------



// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------

// CAN module functions
GOKART_API void 	ioHandlerInitCAN(void);
GOKART_API int32_t 	ioHandlerCANSendMessageRaw(uint32_t pMessageId_u32, uint8_t* pMessageData_pu8, uint8_t pMessageDataLength_u8);
GOKART_API int32_t 	ioHandlerCANSendMessage(IOHandlerCANMessage* pBuffer_pst);
GOKART_API int32_t 	ioHandlerCANReadMessage(IOHandlerCANMessage* pBuffer_pst);
GOKART_API void 	ioHandlerCANHandleTxMessages(void);
GOKART_API void		ioHandlerCANHandleRxMessages(void);
GOKART_API int32_t	ioHandlerCANAddMessageHandler(IOHandlerCANMessageHandlerFunction pHandlerFunction_pf);
GOKART_API void		ioHandlerCANRecoverFromBusOff(void);
GOKART_API bool		ioHandlerCANInBusOffState(void);

// Buffer functions
GOKART_API int32_t 	ioHandlerCANMessageBufferInit(IOHandlerCANMessageBuffer* pMessageBuffer_pst, IOHandlerCANMessage* pBuffer_pst, uint32_t pBufferSize_u32);
GOKART_API int32_t 	ioHandlerCANMessageBufferFlush(IOHandlerCANMessageBuffer* pMessageBuffer_pst);
GOKART_API int32_t 	ioHandlerCANMessageBufferGetCount(IOHandlerCANMessageBuffer* pMessageBuffer_pst);
GOKART_API int32_t	ioHandlerCANMessageBufferPutMessage(IOHandlerCANMessageBuffer* pMessageBuffer_pst, IOHandlerCANMessage* pMessage_pst);
GOKART_API int32_t 	ioHandlerCANMessageBufferGetMessage(IOHandlerCANMessageBuffer* pMessageBuffer_pst, IOHandlerCANMessage* pBuffer_pst);
GOKART_API int32_t 	ioHandlerCANMessageBufferPut(IOHandlerCANMessageBuffer* pMessageBuffer_pst, uint32_t pID_u32, uint8_t* pBuffer_pu8, uint8_t pDLC_u8);

GOKART_API void		__INTERRUPT_HANDLER__ioHandlerOnCANInterruptHandler(void);


#endif //(ECU_CONFIGURATION_ENABLE_CAN == TRUE)

#endif //  IOHANDLER_CAN_IOHANDLERCAN_H__
