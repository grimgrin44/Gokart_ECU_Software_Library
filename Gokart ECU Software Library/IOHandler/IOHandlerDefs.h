/*
 * IOHandlerDefs.h
 *
 *  Created on: 2015 m�j. 18
 *      Author: Grimgrin
 */

#ifndef IOHANDLER_IOHANDLERDEFS_H_
#define IOHANDLER_IOHANDLERDEFS_H_

#include <stdint.h>
#include <stdbool.h>
#include <driverlib/debug.h>

typedef float	float32_t;

#define FALSE	0

#define TRUE	1

#define MASTER	0

#define SLAVE	1

#endif /* IOHANDLER_IOHANDLERDEFS_H_ */
