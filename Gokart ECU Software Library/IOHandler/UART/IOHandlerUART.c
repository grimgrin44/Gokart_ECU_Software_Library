/**
 * @file	IOHandlerUART.c
 * @author	Gabor Kormendy
 * @version	1.0
 * @date	2015.05.24.
 * @brief	Contains variable and function definition for the UART module.
 */

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------
#include "IOHandlerUART.h"


// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

/// The UART configuration
#define IOHANDLER_UART_CONFIGURATION	IOHANDLER_UART_DATA_LENGTH 	| \
										IOHANDLER_UART_STOP_BITS 	| \
										IOHANDLER_UART_PARITY

/// Clear screen command (7 bytes)
#define IOHANDLER_UART_TERMINAL_CMD_CLEAR_SCREEN		{ 0x1b, 0x5b, 0x32, 0x4a, 0x1b, 0x5b, 0x48 }
/// Save cursor position command (3 bytes)
#define IOHANDLER_UART_TERMINAL_CMD_SAVE_POS			{ 0x1b, 0x5b, 0x73 }
/// Restore cursor position command (3 bytes)
#define IOHANDLER_UART_TERMINAL_CMD_RESTORE_POS			{ 0x1b, 0x5b, 0x73 }
///

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

/// Receive buffer for UART
uint8_t gUARTRxBuffer_pu8[IOHANDLER_UART_RX_BUFFER_SIZE];
/// Transmission buffer for UART
uint8_t gUARTTxBuffer_pu8[IOHANDLER_UART_TX_BUFFER_SIZE];
/// Buffer for UART receive
IOHandlerUARTBuffer gUARTRxBuffer_st;
/// Buffer for UART transmission
IOHandlerUARTBuffer gUARTTxBuffer_st;
/// Signs that sending in progress
bool gSendingInProgress_b = false;

// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------


/**
 * @brief	This function checks the buffer and return true if it is empty.
 * @param	pBuffer_pst	Structure pointer to the buffer.
 * @return 	It returns true if the buffer is empty.
 */
static bool ioHandlerUARTBufferIsEmpty(IOHandlerUARTBuffer* pBuffer_pst) {
	return (0 == circularBufferDataAvailable(pBuffer_pst));
}

/**
 * @brief	This function calculates the number of data in the buffer.
 * @param	pBuffer_pst Structure pointer to the buffer.
 * @return	It returns with the number of data in the buffer.
 */
uint32_t ioHandlerUARTGetDataCount(IOHandlerUARTBuffer* pBuffer_pst) {
	return circularBufferDataAvailable(pBuffer_pst);
}

/**
 * @brief	This function reads the receive buffer and puts the data into the
 * 			gUARTRxBuffer_st if it is not full.
 * @return	none
 */
static void ioHandlerUARTReadData(void) {
	uint8_t lData_u8 = 0;

	lData_u8 = UARTCharGet(IOHANDLER_UART_BASE);
	circularBufferPut(&gUARTRxBuffer_st, lData_u8);
}

/**
 * @brief	This function sends data over UART if the gUARTTxBuffer_st is not empty.
 * @return	none
 */
static void ioHandlerUARTWriteData(void) {

	uint8_t lData_u8 = 0;

	if(0 == circularBufferGet(&gUARTTxBuffer_st, &lData_u8)) {
		UARTCharPutNonBlocking(IOHANDLER_UART_BASE, lData_u8);
	}

}


/**
 * @brief	This function will initialize the UART module.
 * @return	none
 */
void ioHandlerInitUART(void) {
	// Enable the UART peripheral
	SysCtlPeripheralEnable(IOHANDLER_PERIPH_UART);
	// Enable the GPIO ports for RX and TX pins
	SysCtlPeripheralEnable(IOHANDLER_PERIPH_GPIO_UART_RX);
	SysCtlPeripheralEnable(IOHANDLER_PERIPH_GPIO_UART_TX);
	// Configure GPIO-s
	GPIOPinTypeUART(IOHANDLER_GPIO_PORT_BASE_UART_RX,
	IOHANDLER_GPIO_PIN_UART_RX);
	GPIOPinTypeUART(IOHANDLER_GPIO_PORT_BASE_UART_TX,
	IOHANDLER_GPIO_PIN_UART_TX);
	GPIOPinConfigure(IOHANDLER_GPIO_UARTRX);
	GPIOPinConfigure(IOHANDLER_GPIO_UARTTX);

	// Initialize buffers

	gUARTTxBuffer_st.iBufferSize_u32 = IOHANDLER_UART_TX_BUFFER_SIZE;
	gUARTTxBuffer_st.iBuffer_pu8 = gUARTTxBuffer_pu8;
	gUARTTxBuffer_st.iOverwriteProtection_b = true;

	gUARTRxBuffer_st.iBufferSize_u32 = IOHANDLER_UART_RX_BUFFER_SIZE;
	gUARTRxBuffer_st.iBuffer_pu8 = gUARTRxBuffer_pu8;
	gUARTRxBuffer_st.iOverwriteProtection_b = true;

	circularBufferInit(&gUARTRxBuffer_st);
	circularBufferInit(&gUARTTxBuffer_st);

	// Configure UART module
	UARTConfigSetExpClk(IOHANDLER_UART_BASE, SysCtlClockGet(),
	IOHANDLER_UART_BITRATE, IOHANDLER_UART_CONFIGURATION);

	// Enable interrupts
	UARTIntEnable(IOHANDLER_UART_BASE, UART_INT_TX | UART_INT_RX);
	IntEnable(IOHANDLER_INT_UART);

	// Enable UART module
	UARTEnable(IOHANDLER_UART_BASE);

	UARTTxIntModeSet(IOHANDLER_UART_BASE, UART_TXINT_MODE_EOT);
	UARTFIFODisable(IOHANDLER_UART_BASE);
}

/**
 * @brief	This function shall be called in the UART interrupt routine.
 * @return 	none
 */
void ioHandlerOnUARTInterrupt(void) {
	uint32_t lIntStatus_u32 = 0;
	//Read interrupt status
	lIntStatus_u32 = UARTIntStatus(IOHANDLER_UART_BASE, true);
	UARTIntClear(IOHANDLER_UART_BASE, lIntStatus_u32);

	// Receive interrupt
	if (lIntStatus_u32 & UART_INT_RX) {
		UARTIntClear(IOHANDLER_UART_BASE, UART_INT_RX);
		ioHandlerUARTReadData();
	}

	// Transmit interrupt
	if (lIntStatus_u32 & UART_INT_TX) {
		UARTIntClear(IOHANDLER_UART_BASE, UART_INT_TX);
		ioHandlerUARTWriteData();
	}

}

/**
 * @brief	Gets the number of received data
 * @return	none
 */
uint32_t ioHandlerUARTDataAvailable(void) {
	return ioHandlerUARTGetDataCount(&gUARTRxBuffer_st);
}

/**
 * @brief	This function places a message into the TX buffer
 * @return	Returns 0 on succes or the error code.
 */
int32_t ioHandlerUARTSendData(uint8_t* pData_pu8, uint32_t pDataLength_u32) {

	bool lStartTransmission_b = false;

	if (true == ioHandlerUARTBufferIsEmpty(&gUARTTxBuffer_st)) {
		lStartTransmission_b = true;
	}

	circularBufferWriteData(&gUARTTxBuffer_st, pData_pu8, pDataLength_u32);


	if (true == lStartTransmission_b) {
		ioHandlerUARTWriteData();
	}
	return 0;
}


/*
 * @brief	This function reads bytes from the receiver buffer
 * @return	Returns the actual number of bytes read or with an error code
 */
int32_t ioHandlerUARTGetData(uint32_t pLength_u32, uint8_t* pBuffer_pu8) {
	return circularBufferReadData(&gUARTRxBuffer_st, pBuffer_pu8, pLength_u32);
}

/**
 * @brief	This function sends a command to the PuTTY terminal to clear the screen
 *
 * @return	none
 */
void ioHandlerUARTTerminalClearScreen(void) {
	static uint8_t lClearCommand_pu8[7] = { 0x1B, 0x5B, 0x32, 0x4A, 0x1B, 0x5B, 0x48 };
	ioHandlerUARTSendData(lClearCommand_pu8, 7);
}

/**
 * @brief	This function sends a command to the PuTTY terminal to go back to the line
 *
 * @return	none
 */
void ioHandlerUARTTerminalCarriageReturn(void) {
	static uint8_t lCarriageReturnCommand_pu8[1] = { '\r' };
	ioHandlerUARTSendData(lCarriageReturnCommand_pu8, 1);
}
