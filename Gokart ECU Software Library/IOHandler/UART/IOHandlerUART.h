/**
 * @file	IOHandlerUART.h
 * @author	Gabor Kormendy
 * @version	1.0
 * @date	2015.05.24.
 * @brief	Contains variable and function declaration for the UART module.
 */

#ifndef IOHANDLER_UART_IOHANDLERUART_H_
#define IOHANDLER_UART_IOHANDLERUART_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------


#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_uart.h"
#include "inc/hw_ints.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include <driverlib/sysctl.h>
#include <driverlib/gpio.h>
#include <driverlib/uart.h>
#include <driverlib/pin_map.h>
#include <driverlib/interrupt.h>

#include "../../Utils/Buffers/CircularBuffer/CircularBuffer.h"

#include "../../ECUConfiguration.h"


// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

/// The UART peripheral.
#define IOHANDLER_PERIPH_UART				ECU_CONFIGURATION_PERIPH_UART
/// The GPIO peripheral for UART RX.
#define IOHANDLER_PERIPH_GPIO_UART_RX		ECU_CONFIGURATION_PERIPH_GPIO_UART_RX
/// The GPIO peripheral for UART TX.
#define IOHANDLER_PERIPH_GPIO_UART_TX		ECU_CONFIGURATION_PERIPH_GPIO_UART_TX
/// The GPIO peripherial settings for the UART port function multiplexing (RX).
#define IOHANDLER_GPIO_UARTRX				ECU_CONFIGURATION_GPIO_UARTRX
/// The GPIO peripherial settings for the UART port function multiplexing (TX).
#define IOHANDLER_GPIO_UARTTX				ECU_CONFIGURATION_GPIO_UARTTX
/// The base address of the UART RX GPIO.
#define IOHANDLER_GPIO_PORT_BASE_UART_RX	ECU_CONFIGURATION_GPIO_PORT_BASE_UART_RX
/// The base address of the UART RX GPIO.
#define IOHANDLER_GPIO_PORT_BASE_UART_TX	ECU_CONFIGURATION_GPIO_PORT_BASE_UART_TX
/// The UART RX pin number
#define IOHANDLER_GPIO_PIN_UART_RX			ECU_CONFIGURATION_GPIO_PIN_UART_RX
/// The UART RX pin number
#define IOHANDLER_GPIO_PIN_UART_TX			ECU_CONFIGURATION_GPIO_PIN_UART_TX
/// The UART module base address
#define IOHANDLER_UART_BASE					ECU_CONFIGURATION_UART_BASE
/// The UART module interrupt
#define IOHANDLER_INT_UART					ECU_CONFIGURATION_INT_UART
/// The baudrate of the UART peripheral
#define IOHANDLER_UART_BITRATE				ECU_CONFIGURATION_UART_BITRATE
/// The data length on the bus
#define IOHANDLER_UART_DATA_LENGTH			ECU_CONFIGURATION_UART_DATA_LENGTH
/// The parity configuration
#define IOHANDLER_UART_PARITY				ECU_CONFIGURATION_UART_PARITY
/// The number of stop bits
#define IOHANDLER_UART_STOP_BITS			ECU_CONFIGURATION_UART_STOP_BITS
/// The receive buffer size in bytes
#define IOHANDLER_UART_RX_BUFFER_SIZE		ECU_CONFIGURATION_UART_RX_BUFFER_SIZE
/// The transmit buffer size in bytes
#define IOHANDLER_UART_TX_BUFFER_SIZE		ECU_CONFIGURATION_UART_TX_BUFFER_SIZE

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

typedef CircularBuffer IOHandlerUARTBuffer;

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------

GOKART_API	void 		ioHandlerInitUART(void);
GOKART_API	void 		ioHandlerOnUARTInterrupt(void);
GOKART_API	int32_t 	ioHandlerUARTSendData(uint8_t* pData_pu8, uint32_t pDataLength_u32);
GOKART_API	int32_t 	ioHandlerUARTGetData(uint32_t pLength_u32, uint8_t* pBuffer_pu8);
GOKART_API	uint32_t 	ioHandlerUARTDataAvailable(void);

#if(ECU_CONFIGURATION_UART_TERMINAL_MODE == TRUE)
GOKART_API	void		ioHandlerUARTTerminalClearScreen(void);
GOKART_API	void		ioHandlerUARTTerminalCarriageReturn(void);
#endif


#endif /* IOHANDLER_UART_IOHANDLERUART_H_ */
