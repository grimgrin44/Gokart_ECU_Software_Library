/**
 * @file	IOHandlerSystem.h
 * @authors	Gabor Kormendy
 * @date	2015.06.08.
 * @brief	This file contains function and variable declarations for the System module.
 */

#ifndef IOHANDLER_SYSTEM_IOHANDLERSYSTEM_H_
#define IOHANDLER_SYSTEM_IOHANDLERSYSTEM_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_ints.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/interrupt.h"
#include "driverlib/fpu.h"

#include "../../ECUConfiguration.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

/// The system clock parameters.
#define IOHANDLER_SYSTEM_CLOCK_PARAMETERS 			ECU_CONFIGURATION_SYSTEM_CLOCK_PARAMETERS
/// The PWM clock configuration
#define IOHANDLER_SYSTEM_PWM_CLOCK_CONFIGURATION	ECU_CONFIGURATION_SYSTEM_PWM_CLOCK_CONFIGURATION

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// function declartions.
// ----------------------------------------------------------------------------

GOKART_API	void ioHandlerInitSystem(void);
GOKART_API 	void ioHandlerSystemDelay(uint32_t pCount_u32);
GOKART_API	void ioHandlerIdentDevice(uint32_t pCommand_u32);
GOKART_API	void ioHandlerEnableSystemInterrupts(void);
GOKART_API	void ioHandlerDisableSystemInterrupts(void);

#endif /* IOHANDLER_SYSTEM_IOHANDLERSYSTEM_H_ */
