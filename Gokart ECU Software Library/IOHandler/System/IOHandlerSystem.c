/**
 * @file	IOHandlerSystem.c
 * @authors	Gabor Kormendy
 * @date	2015.06.08.
 * @brief	This file contains function and variable definitions for the System module.
 */

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include "IOHandlerSystem.h"

// ----------------------------------------------------------------------------
// function definitions.
// ----------------------------------------------------------------------------

/**
 * @brief	This function initializes the clock system and enables FPU and all GPIO peripherals
 * @return	none
 */
void ioHandlerInitSystem(void) {
    // Set the clocking.
	SysCtlClockSet(IOHANDLER_SYSTEM_CLOCK_PARAMETERS);
    // Enable lazy stacking for interrupt handlers.  This allows floating-point
    // instructions to be used within interrupt handlers, but at the expense of
    // extra stack usage.
    FPUEnable();
    FPULazyStackingEnable();
    // Set the PWM clock frequency
	SysCtlPWMClockSet(IOHANDLER_SYSTEM_PWM_CLOCK_CONFIGURATION);
	// Enable all GPIO ports
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
}

/**
 * @brief	This function provides a simple delay
 * @param	pCount_u32	The number of cycles to wait
 * @return	none
 */
void ioHandlerSystemDelay(uint32_t pCount_u32) {
	SysCtlDelay(pCount_u32);
}

/**
 * @brief	This function enables the system interrupts
 * @return	none
 */
void ioHandlerEnableSystemInterrupts(void) {
	IntMasterEnable();
}

/**
 * @brief	This function disables the system interrupts
 * @return	none
 */
void ioHandlerDisableSystemInterrupts(void) {
	IntMasterDisable();
}

/**
 * @brief	This function provides a simple way to identify the device visually
 * @param	pCommand_u32	The ident command
 * @return	none
 */
void ioHandlerIdentDevice(uint32_t pCommand_u32) {
	// TODO: implement the device identification
}
