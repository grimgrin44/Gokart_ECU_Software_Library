/**
 * File: 			Application.h
 * File Version:	1.0
 * Project: 		GoKart
 * Project Version: 1.0
 * Folder: 			Application
 * Creators:		-
 * Last Mod. Date:	2015.05.14.
 * Reviewers:		-
 * Last Rev. Date:	-
 *
 * Contains the function and variable definitions for the
 * application.
 */

#ifndef APPLICATION_APPLICATION_H_
#define APPLICATION_APPLICATION_H_


// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_ints.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"

#include "../ECUConfiguration.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

// The structure for the application data.
typedef struct ApplicationData {

} ApplicationData;

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// function declartions.
// ----------------------------------------------------------------------------

GOKART_API	ApplicationData* applicationGetApplicationData(void);


#endif /* APPLICATION_APPLICATION_H_ */
