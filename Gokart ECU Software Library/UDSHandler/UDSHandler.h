/**
 * File: 			UDSHandler.h
 * File Version:	1.0
 * Project: 		GoKart
 * Project Version: 1.0
 * Folder: 			UDSHandler
 * Creators:		Gabor Kormendy
 * Last Mod. Date:	2015.05.18.
 * Reviewers:		-
 * Last Rev. Date:	-
 *
 * Contains the function and variable declaration for the UDS service
 *
 */

#ifndef UDSHANDLER_UDSHANDLER_H_
#define UDSHANDLER_UDSHANDLER_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

// Single frame type
#define ISO_TP_FRAME_TYPE_SINGLE_FRAME			0x00
// First frame type
#define ISO_TP_FRAME_TYPE_FIRST_FRAME			0x10
// Consecutive frame
#define ISO_TP_FRAME_TYPE_CONSECUTIVE_FRAME		0x20
// Flow control frame
#define ISO_TP_FRAME_TYPE_FLOW_CONTROL_FRAME	0x30

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

typedef struct ISOTpFrame {
	uint8_t iFrameType_u8;
	uint16_t iDataLength_u16;
	uint8_t iFramePayload[];
} IDOTpFrame;

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------




#endif /* UDSHANDLER_UDSHANDLER_H_ */
