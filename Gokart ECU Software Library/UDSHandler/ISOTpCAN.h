/**
 * @file	ISOTpCAN.h
 * @authors	Gabor Kormendy
 * @date	2015. 12. 30.
 * @brief	Contains the function and variable declaration for the ISO Tp CAN protocol.
 */

#ifndef UDSHANDLER_ISOTPCAN_H_
#define UDSHANDLER_ISOTPCAN_H_


// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include "../IOHandler/CAN/IOHandlerCAN.h"


// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

#define ISO_TP_CAN_BUFFER_SIZE				256

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------


typedef struct ISOTpCANBuffer {
	/// Buffer memory block
	uint8_t iBuffer_pu8[ISO_TP_CAN_BUFFER_SIZE];
	/// Number of bytes in the buffer
	uint32_t iDataCount_u32;
	/// Index variable
	uint32_t iIndex_u32;
} ISOTpCANBuffer;


// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------

void isoTpCANInit(void);
bool isoTpCANDataReady(void);
bool isoTpCANTransmitCompleted(void);
uint32_t isoTpCANDataCount(void);
int32_t isoTpSendData(uint8_t* pData_pu8, uint32_t pLength_u32);
uint8_t* isoTpGetReceiveBufferAddress(void);
void isoTpCANStartTransmission(void);
void isoTpCANStopTransmission(void);
void isoTpCANEnableReception(void);
void isoTpCANDisableReception(void);
void isoTpCANReceivedCANMessageCallback(IOHandlerCANMessage* pCANMessage_pst);
void isoTpCANTransmitCANMessageCallback(IOHandlerCANMessage* pCANMessage_pst);

#endif /* UDSHANDLER_ISOTPCAN_H_ */
