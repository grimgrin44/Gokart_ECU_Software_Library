/**
 * @file	ISOTpCAN.c
 * @authors	Gabor Kormendy
 * @date	2015. 12. 30.
 * @brief	Contains the function and variable definitions for the ISO Tp CAN protocol.
 */


// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include "ISOTpCAN.h"


// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

#define ISO_TP_CAN_MESSAGE_TYPE_SINGLE				0
#define ISO_TP_CAN_MESSAGE_TYPE_FIRST				1
#define ISO_TP_CAN_MESSAGE_TYPE_CONSECUTIVE			2
#define ISO_TP_CAN_MESSAGE_TPYE_FLOW_CONTROL		3

#define ISO_TP_CAN_GET_MESSAGE_TYPE(x)				((x >> 4) & 0x0F)
#define ISO_TP_CAN_GET_SINGLE_MESSAGE_SIZE(x)		(x & 0x07)
#define ISO_TP_CAN_GET_FIRST_MESSAGE_SIZE(x, y) 	(((x & 0x0F) << 8) | y)

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

typedef enum {
	IDLE = 0,
	MULTIPLE_FRAME_RECEPTION
} ISOTpCANStatus;


// ----------------------------------------------------------------------------
// Variable definitions.
// ----------------------------------------------------------------------------

static ISOTpCANBuffer sgISOTpCANTransmitBuffer_st;
static ISOTpCANBuffer sgISOTpCANReceiveBuffer_st;

static bool sgTransmissionCompleted_b = true;
static bool sgReceptionEnabled_b = true;

static ISOTpCANStatus sgReceptionState_e = IDLE;

// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------


void isoTpCANInit(void) {
	sgISOTpCANTransmitBuffer_st.iDataCount_u32 = 0;
	sgISOTpCANTransmitBuffer_st.iIndex_u32 = 0;

	sgISOTpCANReceiveBuffer_st.iDataCount_u32 = 0;
	sgISOTpCANReceiveBuffer_st.iIndex_u32 = 0;
}

void isoTpCANReceivedCANMessageCallback(IOHandlerCANMessage* pCANMessage_pst) {

	uint8_t lMessageType_u8 = 0xFF;

	if(false == sgReceptionEnabled_b) {
		return;
	}

	// Get message type

	lMessageType_u8 = ISO_TP_CAN_GET_MESSAGE_TYPE(pCANMessage_pst->iData[0]);

	switch(sgReceptionState_e) {

	case IDLE:

		if(ISO_TP_CAN_MESSAGE_TYPE_SINGLE == lMessageType_u8) {
			uint8_t lMessageSize_u8 = ISO_TP_CAN_GET_SINGLE_MESSAGE_SIZE(pCANMessage_pst->iData[0]);
			memcpy(sgISOTpCANReceiveBuffer_st.iBuffer_pu8, &(pCANMessage_pst->iData[1]), lMessageSize_u8);
			sgISOTpCANReceiveBuffer_st.iDataCount_u32 = lMessageSize_u8;
			sgISOTpCANReceiveBuffer_st.iIndex_u32 = 0;
			isoTpCANDisableReception();
			sgReceptionEnabled_b = IDLE;
		} else if(ISO_TP_CAN_MESSAGE_TYPE_FIRST == lMessageType_u8) {
			uint16_t lMessageSize_u16 = ISO_TP_CAN_GET_FIRST_MESSAGE_SIZE(pCANMessage_pst->iData[0], pCANMessage_pst->iData[1]);
			if(lMessageSize_u16 < ISO_TP_CAN_BUFFER_SIZE) {
				memcpy(sgISOTpCANReceiveBuffer_st.iBuffer_pu8, &(pCANMessage_pst->iData[2]), 6);
				sgISOTpCANReceiveBuffer_st.iDataCount_u32 = 6;
				sgISOTpCANReceiveBuffer_st.iIndex_u32 = 0;
				// Send flow control frame
				sgReceptionState_e = MULTIPLE_FRAME_RECEPTION;
			} else {

			}
		} else if(ISO_TP_CAN_MESSAGE_TPYE_FLOW_CONTROL) {

		}

		break;


	case MULTIPLE_FRAME_RECEPTION:

		if(ISO_TP_CAN_MESSAGE_TYPE_CONSECUTIVE == lMessageType_u8) {

		}

		break;

	}



}

void isoTpCANTransmitCANMessageCallback(IOHandlerCANMessage* pCANMessage_pst) {

	if(false == sgTransmissionCompleted_b) {
		if(sgISOTpCANTransmitBuffer_st.iDataCount_u32 == 0) {

		}
	}

}


