/*
 * Logging.c
 *
 *  Created on: 2015 j�l. 1
 *      Author: Grimgrin
 */

#include "Logging.h"

static LoggingControllerData* sgControllerData_pst = NULL;
static volatile bool sgLoggingEnabled_b = false;
static volatile uint32_t sgSchedulerCounter = 0;

void loggingInit(LoggingControllerData* pControllerData_pst) {
	sgControllerData_pst = pControllerData_pst;
	sgSchedulerCounter = 0;
	if (NULL == sgControllerData_pst) {
		loggingDisable();
	}
}

void loggingScheduler(void) {
	sgSchedulerCounter++;
	if ((LOGGING_MAIN_SCHEDULER_RATE / LOGGING_RATE) == sgSchedulerCounter) {
		if (true == sgLoggingEnabled_b) {
			sgControllerData_pst->iLogWriter_pf(sgControllerData_pst->iLogData_pu8, sgControllerData_pst->iLogDataLength_u32);
		}
		sgSchedulerCounter = 0;
	}
}

void loggingEnable(void) {
	if (NULL != sgControllerData_pst && NULL != sgControllerData_pst->iLogWriter_pf) {
		sgLoggingEnabled_b = true;
	}
}

void loggingDisable(void) {
	sgLoggingEnabled_b = false;
}

