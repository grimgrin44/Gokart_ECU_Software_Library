/*
 * Logging.h
 *
 *  Created on: 2015 j�l. 1
 *      Author: Grimgrin
 */

#ifndef UTILS_LOGGING_LOGGING_H_
#define UTILS_LOGGING_LOGGING_H_


#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

/// Scheduler ticks per second
#define LOGGING_MAIN_SCHEDULER_RATE			1000
/// Logging scheduler ticks per second
#define LOGGING_RATE						100

typedef int32_t (*LogWriter)(uint8_t* pData_pu8, uint32_t pDataLength_u32);

typedef struct LoggingControllerData {
	uint8_t* iLogData_pu8;
	uint32_t iLogDataLength_u32;
	LogWriter iLogWriter_pf;
}LoggingControllerData;

void loggingInit(LoggingControllerData* pControllerData_pst);
void loggingScheduler(void);
void loggingEnable(void);
void loggingDisable(void);

#endif /* UTILS_LOGGING_LOGGING_H_ */
