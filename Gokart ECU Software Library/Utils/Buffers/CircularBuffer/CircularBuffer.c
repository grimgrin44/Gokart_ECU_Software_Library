/*
 * CircularBuffer.c
 *
 *  Created on: 2015 j�l. 1
 *      Author: Grimgrin
 */

#include "CircularBuffer.h"

void circularBufferInit(CircularBuffer* pBuffer_pst) {
	uint32_t lIndex_u32 = 0;

	pBuffer_pst->iHead_u32 = 0;
	pBuffer_pst->iTail_u32 = 0;
	pBuffer_pst->iDataCount_u32 = 0;

	for (lIndex_u32 = 0; lIndex_u32 < pBuffer_pst->iBufferSize_u32; lIndex_u32++) {
		pBuffer_pst->iBuffer_pu8[lIndex_u32] = 0;
	}
}

__inline int32_t circularBufferPut(CircularBuffer* pBuffer_pst, uint8_t pData_u8) {
	if ((pBuffer_pst->iDataCount_u32 == pBuffer_pst->iBufferSize_u32) && (true == pBuffer_pst->iOverwriteProtection_b)) {
		return -1;
	} else {
		pBuffer_pst->iBuffer_pu8[pBuffer_pst->iHead_u32] = pData_u8;
		pBuffer_pst->iHead_u32 = (pBuffer_pst->iHead_u32 + 1) % pBuffer_pst->iBufferSize_u32;
		if (pBuffer_pst->iTail_u32 == pBuffer_pst->iHead_u32) {
			pBuffer_pst->iTail_u32 = (pBuffer_pst->iTail_u32 + 1) % pBuffer_pst->iBufferSize_u32;
		}

		if (pBuffer_pst->iDataCount_u32 != pBuffer_pst->iBufferSize_u32) {
			pBuffer_pst->iDataCount_u32++;
		}
	}
	return 0;
}

__inline int32_t circularBufferGet(CircularBuffer* pBuffer_pst, uint8_t* pData_u8) {
	if (pBuffer_pst->iDataCount_u32 > 0) {
		*pData_u8 = pBuffer_pst->iBuffer_pu8[pBuffer_pst->iTail_u32];
		pBuffer_pst->iTail_u32 = (pBuffer_pst->iTail_u32 + 1) % pBuffer_pst->iBufferSize_u32;
		pBuffer_pst->iDataCount_u32--;
		return 0;
	} else {
		return -1;
	}
}

__inline int32_t circularBufferWriteData(CircularBuffer* pBuffer_pst, uint8_t* pData_pu8, uint32_t pDataLength_u32) {

	uint32_t lIndex_u32 = 0;

	bool lWillOverwrite_b = false;

	if (((pBuffer_pst->iBufferSize_u32 - pBuffer_pst->iDataCount_u32) < pDataLength_u32)) {
		lWillOverwrite_b = true;
	}

	if ((true == lWillOverwrite_b) && (true == pBuffer_pst->iOverwriteProtection_b)) {
		return -1;
	} else {

		for (lIndex_u32 = 0; lIndex_u32 < pDataLength_u32; lIndex_u32++) {
			pBuffer_pst->iBuffer_pu8[pBuffer_pst->iHead_u32] = pData_pu8[lIndex_u32];
			pBuffer_pst->iHead_u32 = (pBuffer_pst->iHead_u32 + 1) % pBuffer_pst->iBufferSize_u32;
			if (pBuffer_pst->iDataCount_u32 < pBuffer_pst->iBufferSize_u32) {
				pBuffer_pst->iDataCount_u32++;
			}
		}

		if (true == lWillOverwrite_b) {
			pBuffer_pst->iTail_u32 = pBuffer_pst->iHead_u32;
		}

	}
	return lIndex_u32;
}

__inline int32_t circularBufferReadData(CircularBuffer* pBuffer_pst, uint8_t* pBuffer_pu8, uint32_t pReadSize_u32) {

	uint32_t lIndex_u32 = 0;

	for (lIndex_u32 = 0; (lIndex_u32 < pBuffer_pst->iDataCount_u32) && (lIndex_u32 < pReadSize_u32); lIndex_u32++) {
		pBuffer_pu8[lIndex_u32] = pBuffer_pst->iBuffer_pu8[pBuffer_pst->iTail_u32];
		pBuffer_pst->iTail_u32 = (pBuffer_pst->iTail_u32 + 1) % pBuffer_pst->iBufferSize_u32;
	}
	pBuffer_pst->iDataCount_u32 -= lIndex_u32;
	return lIndex_u32;
}

__inline int32_t circularBufferReadDataUntil(CircularBuffer* pBuffer_pst, uint8_t* pBuffer_pu8, uint32_t pBufferSize_u32, const char pLimit_pc) {
	uint32_t lIndex_u32 = 0;

	for (lIndex_u32 = 0; (lIndex_u32 < pBufferSize_u32) && (lIndex_u32 < pBuffer_pst->iDataCount_u32); lIndex_u32++) {
		if (pLimit_pc == pBuffer_pst->iBuffer_pu8[pBuffer_pst->iTail_u32]) {
			break;
		} else {
			pBuffer_pu8[lIndex_u32] = pBuffer_pst->iBuffer_pu8[pBuffer_pst->iTail_u32];
			pBuffer_pst->iTail_u32 = (pBuffer_pst->iTail_u32 + 1) % pBuffer_pst->iBufferSize_u32;
			pBuffer_pst->iDataCount_u32--;
		}
	}
	return lIndex_u32;
}

__inline int32_t circularBufferDataAvailable(CircularBuffer* pBuffer_pst) {
	if (pBuffer_pst->iHead_u32 >= pBuffer_pst->iTail_u32) {
		return pBuffer_pst->iHead_u32 - pBuffer_pst->iTail_u32;
	} else {
		return pBuffer_pst->iBufferSize_u32 - pBuffer_pst->iTail_u32 + pBuffer_pst->iHead_u32;
	}
}

__inline int32_t circularBufferSpaceAvailable(CircularBuffer* pBuffer_pst) {
	return pBuffer_pst->iBufferSize_u32 - pBuffer_pst->iDataCount_u32;
}

__inline int32_t circularBufferFlush(CircularBuffer* pBuffer_pst) {
	pBuffer_pst->iHead_u32 = 0;
	pBuffer_pst->iTail_u32 = 0;
	pBuffer_pst->iDataCount_u32 = 0;
	return 0;
}
