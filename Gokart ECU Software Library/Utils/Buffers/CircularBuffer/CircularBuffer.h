/*
 * CircularBuffer.h
 *
 *  Created on: 2015 j�l. 1
 *      Author: Grimgrin
 */

#ifndef UTILS_BUFFERS_CIRCULARBUFFER_CIRCULARBUFFER_H_
#define UTILS_BUFFERS_CIRCULARBUFFER_CIRCULARBUFFER_H_

#include <stdint.h>
#include <stdbool.h>

typedef struct CircularBuffer {
	volatile uint32_t iHead_u32;
	volatile uint32_t iTail_u32;
	volatile uint32_t iDataCount_u32;
	uint32_t iBufferSize_u32;
	uint8_t* iBuffer_pu8;
	bool iOverwriteProtection_b;
} CircularBuffer;

void circularBufferInit(CircularBuffer* pBuffer_pst);
__inline int32_t circularBufferPut(CircularBuffer* pBuffer_pst, uint8_t pData_u8);
__inline int32_t circularBufferGet(CircularBuffer* pBuffer_pst, uint8_t* pData_u8);
__inline int32_t circularBufferWriteData(CircularBuffer* pBuffer_pst, uint8_t* pData_pu8, uint32_t pDataLength_u32);
__inline int32_t circularBufferReadData(CircularBuffer* pBuffer_pst, uint8_t* pBuffer_pu8, uint32_t pReadSize_u32);
__inline int32_t circularBufferReadDataUntil(CircularBuffer* pBuffer_pst, uint8_t* pBuffer_pu8, uint32_t pBufferSize_u32, const char pLimit_pc);
__inline int32_t circularBufferDataAvailable(CircularBuffer* pBuffer_pst);
__inline int32_t circularBufferSpaceAvailable(CircularBuffer* pBuffer_pst);
__inline int32_t circularBufferFlush(CircularBuffer* pBuffer_pst);

#endif /* UTILS_BUFFERS_CIRCULARBUFFER_CIRCULARBUFFER_H_ */
