/**
 * @file	GokartECUSoftwareLibrary.h
 * @authors	Grimgrin
 * @date	2016 jan. 25
 * @brief	
 */
#ifndef GOKARTECUSOFTWARELIBRARY_H_
#define GOKARTECUSOFTWARELIBRARY_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

/*
 * Standard files
 */
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

/*
 * IOHandler software module
 */
#include "IOHandler/Analog/IOHandlerAnalog.h"
#include "IOHandler/CAN/IOHandlerCAN.h"
#include "IOHandler/DIO/IOHandlerDIO.h"
#include "IOHandler/EEPROM/IOHandlerEEPROM.h"
#include "IOHandler/GeneralTimer/IOHandlerTimer.h"
#include "IOHandler/PWM/IOHandlerPWM.h"
#include "IOHandler/SPI/IOHandlerSPI.h"
#include "IOHandler/System/IOHandlerSystem.h"
#include "IOHandler/Systick/IOHandlerSystick.h"
#include "IOHandler/UART/IOHandlerUART.h"
#include "IOHandler/Watchdog/IOHandlerWatchdog.h"

/*
 * Motor driver software module
 */
#include "MotorDriver/BTS7960B/BTS7960B.h"
#include "MotorDriver/DRV8711/DRV8711Driver.h"
#include "MotorDriver/Servo/ServoDriver.h"

/*
 * Remote diagnostic interface software module
 */
#include "RemoteDiagnosticInterface/RemoteDiagnosticInterface.h"

/*
 * Data handler software module
 */
#include "DataHandler/CAN/DataHandlerCAN.h"
#include "DataHandler/ParameterManagement/ParameterManagement.h"

/*
 * GPS software module
 */
#include "GPS/GPSData.h"
#include "GPS/NEO-6/NEO-6.h"

/*
 * Remote control
 */
#include "RemoteControl/RemoteControl.h"

/*
 * Utils software module
 */
#include "Utils/Buffers/CircularBuffer/CircularBuffer.h"


// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------


#endif /* GOKARTECUSOFTWARELIBRARY_H_ */
