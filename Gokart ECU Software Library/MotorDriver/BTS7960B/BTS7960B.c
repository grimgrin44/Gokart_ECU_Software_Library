/**
 * @file	BTS7960B.c
 * @authors	G�bor K�rmendy
 * @date	2016 jan. 23
 * @brief	
 */

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include "BTS7960B.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

enum Direction {
	FORWARD,
	REVERSE
};

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

enum Direction sgMotorDirection_e = FORWARD;

static IOHandlerPWMOutput sgPWMOutput_st;

// ----------------------------------------------------------------------------
// Function definitions.
// ----------------------------------------------------------------------------

void motorDriverInitBTS7960B(IOHandlerPWMOutput* pPWMOutput_pst) {
	__ASSERT(pPWMOutput_pst == NULL);

	sgPWMOutput_st.iPWMModule_e = pPWMOutput_pst->iPWMModule_e;
	sgPWMOutput_st.iPWMGenerator_e = pPWMOutput_pst->iPWMGenerator_e;

	SysCtlPeripheralEnable(MOTOR_DRIVER_BTS7960B_ENABLE_FORWARD_PIN_PERIPHERAL_BASE);
	SysCtlPeripheralEnable(MOTOR_DRIVER_BTS7960B_ENABLE_REVERSE_PIN_PERIPHERAL_BASE);

	GPIOPinTypeGPIOOutput(MOTOR_DRIVER_BTS7960B_ENABLE_FORWARD_PIN_PORT, MOTOR_DRIVER_BTS7960B_ENABLE_FORWARD_PIN);
	GPIOPinTypeGPIOOutput(MOTOR_DRIVER_BTS7960B_ENABLE_REVERSE_PIN_PORT, MOTOR_DRIVER_BTS7960B_ENABLE_REVERSE_PIN);

	ioHandlerPWMInitModule(&sgPWMOutput_st, MOTOR_DRIVER_BTS7960B_PWM_PERIOD_IN_HZ);

	sgPWMOutput_st.iPWMOutput_e = PWM_OUTPUT_1;
	ioHandlerPWMSetOutputState(&sgPWMOutput_st, true);

	sgPWMOutput_st.iPWMOutput_e = PWM_OUTPUT_2;
	ioHandlerPWMSetOutputState(&sgPWMOutput_st, true);
}

int32_t	motorDriverBTS7960BForward(void) {

	sgMotorDirection_e = FORWARD;
	sgPWMOutput_st.iPWMOutput_e = PWM_OUTPUT_1;
	ioHandlerPWMSetDutyCycle(&sgPWMOutput_st, 0);
	sgPWMOutput_st.iPWMOutput_e = PWM_OUTPUT_2;
	ioHandlerPWMSetDutyCycle(&sgPWMOutput_st, 0);

	return 0;
}

int32_t motorDriverBTS7960BReverse(void) {

	sgMotorDirection_e = REVERSE;
	sgPWMOutput_st.iPWMOutput_e = PWM_OUTPUT_1;
	ioHandlerPWMSetDutyCycle(&sgPWMOutput_st, 0);
	sgPWMOutput_st.iPWMOutput_e = PWM_OUTPUT_2;
	ioHandlerPWMSetDutyCycle(&sgPWMOutput_st, 0);

	return 0;
}

int32_t motorDriverBTS7960BSetEnableForward(bool pEnable_b) {
	if(true == pEnable_b) {
		GPIOPinWrite(MOTOR_DRIVER_BTS7960B_ENABLE_FORWARD_PIN_PORT, MOTOR_DRIVER_BTS7960B_ENABLE_FORWARD_PIN, MOTOR_DRIVER_BTS7960B_ENABLE_FORWARD_PIN);
	} else {
		GPIOPinWrite(MOTOR_DRIVER_BTS7960B_ENABLE_FORWARD_PIN_PORT, MOTOR_DRIVER_BTS7960B_ENABLE_FORWARD_PIN, 0);
	}
	return 0;
}

int32_t motorDriverBTS7960BSetEnableReverse(bool pEnable_b) {
	if(true == pEnable_b) {
		GPIOPinWrite(MOTOR_DRIVER_BTS7960B_ENABLE_REVERSE_PIN_PORT, MOTOR_DRIVER_BTS7960B_ENABLE_REVERSE_PIN, MOTOR_DRIVER_BTS7960B_ENABLE_REVERSE_PIN);
	} else {
		GPIOPinWrite(MOTOR_DRIVER_BTS7960B_ENABLE_REVERSE_PIN_PORT, MOTOR_DRIVER_BTS7960B_ENABLE_REVERSE_PIN, 0);
	}
	return 0;
}

int32_t motorDriverBTS7960BSetDutyCycle(uint8_t pDutyCycle_u8) {
	if(FORWARD == sgMotorDirection_e) {
		sgPWMOutput_st.iPWMOutput_e = PWM_OUTPUT_1;
		ioHandlerPWMSetDutyCycle(&sgPWMOutput_st, 100-pDutyCycle_u8);
	} else if(REVERSE == sgMotorDirection_e) {
		sgPWMOutput_st.iPWMOutput_e = PWM_OUTPUT_2;
		ioHandlerPWMSetDutyCycle(&sgPWMOutput_st, 100-pDutyCycle_u8);
	}
	return 0;
}

void motorDriverBTS7960BCurrentAlarmCallback(void) {

}
