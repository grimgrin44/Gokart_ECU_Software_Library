/**
 * @file	BTS7960B.h
 * @authors	G�bor K�rmendy
 * @date	2016 jan. 23
 * @brief	
 */
#ifndef MOTORDRIVER_BTS7960B_BTS7960B_H_
#define MOTORDRIVER_BTS7960B_BTS7960B_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "../../IOHandler/PWM/IOHandlerPWM.h"

#include "../../ECUConfiguration.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

#define MOTOR_DRIVER_BTS7960B_ENABLE_FORWARD_PIN_PERIPHERAL_BASE	ECU_CONFIGURATION_BTS7960B_ENABLE_FORWARD_PIN_PERIPHERAL_BASE

#define MOTOR_DRIVER_BTS7960B_ENABLE_REVERSE_PIN_PERIPHERAL_BASE	ECU_CONFIGURATION_BTS7960B_ENABLE_REVERSE_PIN_PERIPHERAL_BASE

#define	MOTOR_DRIVER_BTS7960B_PWM_PERIOD_IN_HZ						ECU_CONFIGURATION_BTS7960B_PWM_PERIOD_IN_HZ

#define MOTOR_DRIVER_BTS7960B_ENABLE_FORWARD_PIN_PORT				ECU_CONFIGURATION_BTS7960B_ENABLE_FORWARD_PIN_PORT

#define MOTOR_DRIVER_BTS7960B_ENABLE_FORWARD_PIN					ECU_CONFIGURATION_BTS7960B_ENABLE_FORWARD_PIN

#define MOTOR_DRIVER_BTS7960B_ENABLE_REVERSE_PIN_PORT				ECU_CONFIGURATION_BTS7960B_ENABLE_REVERSE_PIN_PORT

#define MOTOR_DRIVER_BTS7960B_ENABLE_REVERSE_PIN					ECU_CONFIGURATION_BTS7960B_ENABLE_REVERSE_PIN

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------

GOKART_API	void	motorDriverInitBTS7960B(IOHandlerPWMOutput* pPWMOutput_pst);
GOKART_API	int32_t	motorDriverBTS7960BForward(void);
GOKART_API	int32_t motorDriverBTS7960BReverse(void);
GOKART_API	int32_t motorDriverBTS7960BSetEnableForward(bool pEnable_b);
GOKART_API	int32_t motorDriverBTS7960BSetEnableReverse(bool pEnable_b);
GOKART_API	int32_t motorDriverBTS7960BSetDutyCycle(uint8_t pDutyCycle_u8);
GOKART_API	void	motorDriverBTS7960BCurrentAlarmCallback(void);



#endif /* MOTORDRIVER_BTS7960B_BTS7960B_H_ */
