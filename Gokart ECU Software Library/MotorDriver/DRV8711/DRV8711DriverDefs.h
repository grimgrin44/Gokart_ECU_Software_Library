/*
 * DRV8711DriverDefs.h
 *
 *  Created on: 2015 j�n. 23
 *      Author: Grimgrin
 */

#ifndef MOTORDRIVER_DRV8711_DRV8711DRIVERDEFS_H_
#define MOTORDRIVER_DRV8711_DRV8711DRIVERDEFS_H_

/// DRV8711 control register address
#define DRV8711_REG_CTRL		0x00
/// DRV8711 torque register address
#define DRV8711_REG_TORQUE		0x01
/// DRV8711 off register address
#define DRV8711_REG_OFF			0x02
/// DRV8711 blank register address
#define DRV8711_REG_BLANK		0x03
/// DRV8711 decay register address
#define DRV8711_REG_DECAY		0x04
/// DRV8711 stall register address
#define DRV8711_REG_STALL		0x05
/// DRV8711 drive register address
#define DRV8711_REG_DRIVE		0x06
/// DRV8711 status register address
#define DRV8711_REG_STATUS		0x07

/// DRV8711 write operation mask
#define DRV8711_WRITE_OPERATION				(0x00)
/// DRV8711 read operation mask
#define DRV8711_READ_OPERATION				(0x80)


/* CTRL register bitmasks */

/// Enable motor
#define DRV8711_ENABLE_MOTOR			(0x01 << 0)
/// Disable motor
#define DRV8711_DISABLE_MOTOR			(0x00 << 0)
/// Direction set by direction pin
#define DRV8711_DIR_SET_BY_DIR_PIN		(0x01 << 1)
/// Direction set by inverse of direction pin
#define	DRV8711_DIR_SET_BY_INV_DIR_PIN	(0x00 << 1)
/// Step one
#define DRV8711_STEP_ONE				(0x01 << 2)
/// Full-step mode
#define DRV8711_MODE_FULL_STEP			(0x00 << 3)
/// Half-step mode
#define DRV8711_MODE_HALF_STEP			(0x01 << 3)
/// 1/4 step mode
#define DRV8711_MODE_1_4_STEP			(0x02 << 3)
/// 1/8 step mode
#define DRV8711_MODE_1_8_STEP			(0x03 << 3)
/// 1/16 step mode
#define DRV8711_MODE_1_16_STEP			(0x04 << 3)
/// 1/32 step mode
#define DRV8711_MODE_1_32_STEP			(0x05 << 3)
/// 1/64 step mode
#define DRV8711_MODE_1_64_STEP			(0x06 << 3)
/// 1/128 step mode
#define DRV8711_MODE_1_128_STEP			(0x07 << 3)
/// 1/256 step mode
#define DRV8711_MODE_1_256_STEP			(0x08 << 3)
/// Internal stall detection
#define DRV8711_INTERNAL_STALL_DETECT	(0x01 << 7)
/// External stall detection
#define DRV8711_EXTERNAL_STALL_DETECT	(0x00 << 7)
/// Current sense gain 5
#define DRV8711_ISENSE_GAIN_5			(0x00 << 8)
/// Current sense gain 10
#define DRV8711_ISENSE_GAIN_10			(0x01 << 8)
/// Current sense gain 20
#define DRV8711_ISENSE_GAIN_20			(0x02 << 8)
/// Current sense gain 40
#define DRV8711_ISENSE_GAIN_40			(0x03 << 8)
/// Dead time 400 ns
#define DRV8711_DTIME_400ns				(0x00 << 10)
/// Dead time 450 ns
#define DRV8711_DTIME_450ns				(0x01 << 10)
/// Dead time 650 ns
#define DRV8711_DTIME_650ns				(0x02 << 10)
/// Dead time 850 ns
#define DRV8711_DTIME_850ns				(0x03 << 10)

/* Torque register */

#define DRV8711_SMPLTH_50us				(0x00 << 8)
#define DRV8711_SMPLTH_100us			(0x01 << 8)
#define DRV8711_SMPLTH_200us			(0x02 << 8)
#define DRV8711_SMPLTH_300us			(0x03 << 8)
#define DRV8711_SMPLTH_400us			(0x04 << 8)
#define DRV8711_SMPLTH_600us			(0x05 << 8)
#define DRV8711_SMPLTH_800us			(0x06 << 8)
#define DRV8711_SMPLTH_1000us			(0x07 << 8)

/* Off register */
#define DRV8711_PWM_MODE_INTERNAL_IDX	(0x00 << 8)
#define DRV8711_PWM_MODE_BYPASS_IDX		(0x01 << 8)

/* Blank register */
#define DRV8711_ABT_ENABLED				(0x01 << 8)
#define DRV8711_ABT_DISABLED			(0x00 << 8)

/* Decay register */
#define DRV8711_DECMOD_FORCE_SLOW		(0x00 << 8)
#define DRV8711_DECMOD_SLOW_INC_MIX_DEC	(0x01 << 8)
#define DRV8711_DECMOD_FORCE_FAST		(0x02 << 8)
#define DRV8711_DECMOD_MIXED			(0x03 << 8)
#define DRV8711_DECMOD_SLOW_INC_AUTO_MIX_DEC	(0x04 << 8)
#define DRV8711_DECMOD_AUTO_MIXED		(0x05 << 8)

/* Stall register */
#define DRV8711_SDCNT_STALLn_ASSERT_1_STEP	(0x00 << 8)
#define DRV8711_SDCNT_STALLn_ASSERT_2_STEP	(0x01 << 8)
#define DRV8711_SDCNT_STALLn_ASSERT_4_STEP	(0x02 << 8)
#define DRV8711_SDCNT_STALLn_ASSERT_8_STEP	(0x03 << 8)
#define DRV8711_VDIV_32						(0x00 << 10)
#define DRV8711_VIDV_16						(0x01 << 10)
#define DRV8711_VDIV_8						(0x02 << 10)
#define DRV8711_VDIV_4						(0x03 << 10)

/* Drive register */
#define DRV8711_OCPTH_250mV					(0x00 << 0)
#define DRV8711_OCPTH_500mV					(0x01 << 0)
#define DRV8711_OCPTH_750mV					(0x02 << 0)
#define DRV8711_OCPTH_1000mV				(0x03 << 0)
#define DRV8711_OCPDEG_1us					(0x00 << 2)
#define DRV8711_OCPDEG_2us					(0x01 << 2)
#define DRV8711_OCPDEG_4us					(0x02 << 2)
#define DRV8711_OCPDEG_8us					(0x03 << 2)
#define DRV8711_TDRIVEN_250ns				(0x00 << 4)
#define DRV8711_TDRIVEN_500ns				(0x01 << 4)
#define DRV8711_TDRIVEN_1us					(0x02 << 4)
#define DRV8711_TDRIVEN_2us					(0x03 << 4)
#define DRV8711_TDRIVEP_250ns				(0x00 << 6)
#define DRV8711_TDRIVEP_500ns				(0x01 << 6)
#define DRV8711_TDRIVEP_1us					(0x02 << 6)
#define DRV8711_TDRIVEP_2us					(0x03 << 6)
#define DRV8711_IDRIVEN_100mA				(0x00 << 8)
#define DRV8711_IDRIVEN_200mA				(0x01 << 8)
#define DRV8711_IDRIVEN_300mA				(0x02 << 8)
#define DRV8711_IDRIVEN_400mA				(0x03 << 8)
#define DRV8711_IDRIVEP_50mA				(0x00 << 10)
#define DRV8711_IDRIVEP_100mA				(0x01 << 10)
#define DRV8711_IDRIVEP_150mA				(0x02 << 10)
#define DRV8711_IDRIVEP_200mA				(0x03 << 10)

/* Status register */
#define DRV8711_NORMAL_OPERATION				(0x00)
#define DRV8711_OVERTEMPERATURE_SHUTDOWN		(0x01 << 0)
#define DRV8711_CHANNEL_A_OVERCURRENT_SHUTDOWN	(0x01 << 1)
#define DRV8711_CHANNEL_B_OVERCURRENT_SHUTDOWN	(0x01 << 2)
#define DRV8711_CHANNEL_A_PREDRIVER_FAULT		(0x01 << 3)
#define DRV8711_CHANNEL_B_PREDRIVER_FAULT		(0x01 << 4)
#define DRV8711_UNDERVOLTAGE_LOCKOUT			(0x01 << 5)
#define DRV8711_STALL_DETECTED					(0x01 << 6)
#define DRV8711_LATCHED_STALL_DETECTED			(0x01 << 7)



#endif /* MOTORDRIVER_DRV8711_DRV8711DRIVERDEFS_H_ */
