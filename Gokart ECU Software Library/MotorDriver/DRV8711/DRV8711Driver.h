/*
 * DRV8711Driver.h
 *
 *  Created on: 2015 j�n. 21
 *      Author: G�bor
 */

#ifndef MOTORDRIVER_DRV8711_DRV8711DRIVER_H_
#define MOTORDRIVER_DRV8711_DRV8711DRIVER_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include "../../ECUConfiguration.h"

#include "../../IOHandler/IOHandlerDefs.h"
#include "../../IOHandler/SPI/IOHandlerSPI.h"
#include "../../IOHandler/DIO/IOHandlerDIO.h"
#include "../../IOHandler/System/IOHandlerSystem.h"

#include <driverlib/timer.h>

#include "DRV8711DriverDefs.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

/// DRV8711 control register default config
#define DRV8711_CONTROL_REGISTER_DEFAULT_CONFIG		(\
	DRV8711_DISABLE_MOTOR | \
	DRV8711_DIR_SET_BY_DIR_PIN | \
	DRV8711_MODE_FULL_STEP | \
	DRV8711_INTERNAL_STALL_DETECT | \
	DRV8711_ISENSE_GAIN_10 | \
	DRV8711_DTIME_850ns \
	)

/// DRV8711 torque register default config
#define DRV8711_TORQUE_REGISTER_DEFAULT_CONFIG		(\
	0x0010 | \
	DRV8711_SMPLTH_100us \
	)

/// DRV8711 off register default config
#define DRV8711_OFF_REGISTER_DEFAULT_CONFIG			(\
	0x0030 | \
	DRV8711_PWM_MODE_INTERNAL_IDX \
	)

/// DRV8711 blank register default config
#define DRV8711_BLANK_REGISTER_DEFAULT_CONFIG		(\
	0x0080 | \
	DRV8711_ABT_DISABLED \
	)

/// DRV8711 decay register default config
#define DRV8711_DECAY_REGISTER_DEFAULT_CONFIG		(\
	0x0010 | \
	DRV8711_DECMOD_SLOW_INC_MIX_DEC \
	)

/// DRV8711 stall register default config
#define DRV8711_STALL_REGISTER_DEFAULT_CONFIG		(\
	0x0010 | \
	DRV8711_SDCNT_STALLn_ASSERT_8_STEP | \
	DRV8711_VDIV_32 \
	)
/// DRV8711 drive register default config
#define DRV8711_DRIVE_REGISTER_DEFAULT_CONFIG		(\
	DRV8711_OCPTH_250mV | \
	DRV8711_OCPDEG_1us | \
	DRV8711_TDRIVEN_500ns | \
	DRV8711_TDRIVEP_500ns | \
	DRV8711_IDRIVEN_200mA | \
	DRV8711_IDRIVEP_200mA \
	)

#define DRV8711_DEFAULT_CONFIGURATION	\
	DRV8711_CONTROL_REGISTER_DEFAULT_CONFIG, \
	DRV8711_TORQUE_REGISTER_DEFAULT_CONFIG, \
	DRV8711_OFF_REGISTER_DEFAULT_CONFIG, \
	DRV8711_BLANK_REGISTER_DEFAULT_CONFIG, \
	DRV8711_DECAY_REGISTER_DEFAULT_CONFIG, \
	DRV8711_STALL_REGISTER_DEFAULT_CONFIG, \
	DRV8711_DRIVE_REGISTER_DEFAULT_CONFIG, \
	0

/// Base address of the reset pin port
#define DRV8711_RESET_PIN_PORT						IOHANDLER_DIO_PORTA_BASE
/// Reset pin number
#define DRV8711_RESET_PIN							IOHANDLER_DIO_PIN_5

#define DRV8711_STEP_PIN_PORT						IOHANDLER_DIO_PORTA_BASE
/// Step pin number
#define DRV8711_STEP_PIN							IOHANDLER_DIO_PIN_6

#define DRV8711_DIRECTION_PIN_PORT					IOHANDLER_DIO_PORTA_BASE
/// Direction pin number
#define DRV8711_DIRECTION_PIN						IOHANDLER_DIO_PIN_7

#define DRV8711_SLEEP_PIN_PORT						IOHANDLER_DIO_PORTE_BASE
/// Sleep pin number
#define DRV8711_SLEEP_PIN							IOHANDLER_DIO_PIN_5

#define DRV8711_CHIP_SELECT_PIN_PORT				IOHANDLER_DIO_PORTA_BASE
/// Chip select pin number
#define DRV8711_CHIP_SELECT_PIN						IOHANDLER_DIO_PIN_2


#define DRV8711_SPEED_CONTROLLER_ENABLED			FALSE


#define DRV8711_STEPPER_TIMER_PERIPHERAL			SYSCTL_PERIPH_TIMER1

#define DRV8711_STEPPER_TIMER_BASE					TIMER1_BASE

#define DRV8711_STEPPER_TIMER_SUBMODULE				TIMER_A

#define DRV8711_STEPPER_TIMER_INTERRUPT				TIMER_TIMA_TIMEOUT

#define DRV8711_STEPPER_TIMER_NVIC_INTERRUPT		INT_TIMER1A

#define DRV8711_STEPPER_TIMER_CONFIGURATION			TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PERIODIC

#define DRV8711_STEPPER_TIMER_CLOCK_SOURCE			TIMER_CLOCK_SYSTEM

#define DRV8711_STEPPER_TIMER_DIVIDER				80

#define DRV8711_STEPPER_TIMER_LOAD					1000

#define DRV8711_DEFAULT_HOLDING_TORQUE				0x10

#define DRV8711_DEFAULT_ENABLE_HOLDING_TORQUE		TRUE

#if (DRV8711_SPEED_CONTROLLER_ENABLED == TRUE)

#define DRV8711_STEPPING_ACCELERATION

#define DRV8711_STEPPING_DECELERATION

#endif

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

typedef void (*DRV8711InterruptCallback)(uint32_t pStatusRegister_u32);

typedef struct DRV8711Configuration {
	/// Control register value
	uint32_t iCTRL_u32;
	/// Torque register value
	uint32_t iTORQUE_u32;
	/// Off register value
	uint32_t iOFF_u32;
	/// Blank register value
	uint32_t iBLANK_u32;
	/// Decay register value
	uint32_t iDECAY_u32;
	/// Stall register value
	uint32_t iSTALL_u32;
	/// Drive register value
	uint32_t iDRIVE_u32;
	/// Status register value
	uint32_t iSTATUS_u32;
} DRV8711Configuration;


#if (DRV8711_SPEED_CONTROLLER_ENABLED == TRUE)

typedef struct DRV8711SpeedControllerData {

} DRV8711SpeedControllerData;

#endif

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------

void motorDriverDRV8711Init(DRV8711Configuration* pConfiguration_pst, DRV8711InterruptCallback pInterruptCallback_pf);
void motorDriverDRV8711Configure(DRV8711Configuration* pConfiguration_pst);
void motorDriverDRV8711GetConfig(DRV8711Configuration* pConfiguration_pst);
void motorDriverDRV8711Control(uint32_t pStepsToGo_u32);
void motorDriverDRV8711SetHoldingTorque(uint8_t pTorque_u8);
void motorDriverDRV8711SetTorque(uint8_t pTorque_u8);
void motorDriverDRV8711StepOne(void);
void motorDriverDRV8711ChangeDirection(void);
void motorDriverDRV8711ChangeStepMode(uint32_t pStepMode_u32);
uint32_t motorDriverDRV8711ReadStatus(void);
void motorDriverDRV8711ClearStatus(uint32_t pMask_u32);
void motorDriverDRV8711EnableMotor(void);
void motorDriverDRV8711DisableMotor(void);
void motorDriverDRV8711Sleep(void);
void motorDriverDRV8711WakeUp(void);
void motorDriverDRV8711Reset(void);

void motorDriverDRV8711SetCurrent(float32_t pCurrent_f32);
float32_t motorDriverDRV8711GetCurrent(void);

int32_t motorDriverOnDRV8711Interrupt(void);
int32_t motorDriverOnDRV8711StepTimerInterrupt(void);



#endif /* MOTORDRIVER_DRV8711_DRV8711DRIVER_H_ */
