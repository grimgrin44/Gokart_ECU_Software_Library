/*
 * DRV8711Driver.c
 *
 *  Created on: 2015 j�n. 21
 *      Author: G�bor
 */

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------
#include "DRV8711Driver.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

typedef enum SpeedControllerState {
	ACCELERATION, RUNNING, DECELARATION, STOP
} SpeedControllerState;

typedef enum MotorState {
	SLEEPING, MOVING, HOLDING, IDLE
} MotorState;

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

static DRV8711InterruptCallback sgInterruptCallback_pf = NULL;
static DRV8711Configuration* sgConfiguration_pst = NULL;

static volatile uint32_t sgStepsToGo_u32 = 0;
static volatile bool sgEnableHoldingTorque_b = DRV8711_DEFAULT_ENABLE_HOLDING_TORQUE;
static volatile uint8_t sgHoldingTorque_u8 = DRV8711_DEFAULT_HOLDING_TORQUE;
static volatile bool sgHoldingTorqueActive_b = false;
static volatile bool sgMotorEnabled_b = false;
static volatile bool sgSleepMode_b = false;
static volatile bool sgRunning_b = false;

// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------

static void motorDriverDRV8711Select(bool pSelect_b) {
	if (true == pSelect_b) {
		GPIOPinWrite(DRV8711_CHIP_SELECT_PIN_PORT, DRV8711_CHIP_SELECT_PIN, DRV8711_CHIP_SELECT_PIN);
	} else {
		GPIOPinWrite(DRV8711_CHIP_SELECT_PIN_PORT, DRV8711_CHIP_SELECT_PIN, 0);
	}
}

static void motorDriverDRV8711ReadRegister(uint32_t pRegister_u32, uint32_t* pValue_pu32) {
	// SPI transfer object
	IOHandlerSPITransfer lTransfer_st = { 0 };
	// Receive buffer
	uint8_t lRxBuffer_pu8[2] = { 0, 0 };
	// Transmit buffer
	uint8_t lTxBuffer_pu8[2] = { 0, 0 };

	lTransfer_st.iRxData_pu8 = lRxBuffer_pu8;
	lTransfer_st.iTxData_pu8 = lTxBuffer_pu8;
	lTransfer_st.iDiscardRxData_b = false;
	lTransfer_st.iSelector_pf = motorDriverDRV8711Select;
	lTransfer_st.iDLC_u32 = 2;

	lTxBuffer_pu8[0] = DRV8711_READ_OPERATION | (pRegister_u32 << 4);
	lTxBuffer_pu8[1] = 0xFF;

	ioHandlerSPIExecuteTransfer(&lTransfer_st);

	*pValue_pu32 = (((uint32_t) lRxBuffer_pu8[0] << 8) & 0x0F) | lRxBuffer_pu8[1];
}

static void motorDriverDRV8711WriteRegister(uint32_t pRegister_u32, uint32_t pValue_pu32) {
	// SPI transfer object
	IOHandlerSPITransfer lTransfer_st = { 0 };
	// Transmit buffer
	uint8_t lTxBuffer_pu8[2] = { 0, 0 };

	lTransfer_st.iTxData_pu8 = lTxBuffer_pu8;
	lTransfer_st.iDiscardRxData_b = false;
	lTransfer_st.iSelector_pf = motorDriverDRV8711Select;
	lTransfer_st.iDLC_u32 = 2;

	lTxBuffer_pu8[0] = DRV8711_WRITE_OPERATION | (pRegister_u32 << 4) | (pValue_pu32 >> 8);
	lTxBuffer_pu8[1] = pValue_pu32 & 0xFF;

	ioHandlerSPIExecuteTransfer(&lTransfer_st);
}

void motorDriverDRV8711Init(DRV8711Configuration* pConfiguration_pst, DRV8711InterruptCallback pInterruptCallback_pf) {

	sgInterruptCallback_pf = pInterruptCallback_pf;
	sgConfiguration_pst = pConfiguration_pst;

	GPIOPinTypeGPIOOutput(DRV8711_STEP_PIN_PORT, DRV8711_STEP_PIN);
	GPIOPinTypeGPIOOutput(DRV8711_DIRECTION_PIN_PORT, DRV8711_DIRECTION_PIN);
	GPIOPinTypeGPIOOutput(DRV8711_RESET_PIN_PORT, DRV8711_RESET_PIN);
	GPIOPinTypeGPIOOutput(DRV8711_SLEEP_PIN_PORT, DRV8711_SLEEP_PIN);
	GPIOPinTypeGPIOOutput(DRV8711_CHIP_SELECT_PIN_PORT, DRV8711_CHIP_SELECT_PIN);

#if (ECU_CONFIGURATION_MOTOR_DRIVER_DRV8711_ENABLE_STATUS_INTERRUPT == TRUE)

#endif

	motorDriverDRV8711Select(false);
	motorDriverDRV8711WakeUp();
	motorDriverDRV8711Reset();
	motorDriverDRV8711Configure(sgConfiguration_pst);

	SysCtlPeripheralEnable(DRV8711_STEPPER_TIMER_PERIPHERAL);
	TimerClockSourceSet(DRV8711_STEPPER_TIMER_BASE, DRV8711_STEPPER_TIMER_CLOCK_SOURCE);
	TimerConfigure(DRV8711_STEPPER_TIMER_BASE, DRV8711_STEPPER_TIMER_CONFIGURATION);
	TimerPrescaleSet(DRV8711_STEPPER_TIMER_BASE, DRV8711_STEPPER_TIMER_SUBMODULE, DRV8711_STEPPER_TIMER_DIVIDER);
	TimerLoadSet(DRV8711_STEPPER_TIMER_BASE, DRV8711_STEPPER_TIMER_SUBMODULE, DRV8711_STEPPER_TIMER_LOAD);
	TimerEnable(DRV8711_STEPPER_TIMER_BASE, DRV8711_STEPPER_TIMER_SUBMODULE);
	TimerIntEnable(DRV8711_STEPPER_TIMER_BASE, DRV8711_STEPPER_TIMER_INTERRUPT);

	IntEnable(DRV8711_STEPPER_TIMER_NVIC_INTERRUPT);
}

void motorDriverDRV8711Configure(DRV8711Configuration* pConfiguration_pst) {
	// Index variable
	uint32_t lIndex_u32 = 0;
	// Temporary pointer to the configuration structure element
	uint32_t lTemp_u32 = NULL;

	for (lIndex_u32 = 0; lIndex_u32 < (sizeof(DRV8711Configuration) / sizeof(uint32_t)); lIndex_u32++) {
		lTemp_u32 = ((uint32_t*) pConfiguration_pst)[lIndex_u32];
		motorDriverDRV8711WriteRegister(lIndex_u32, lTemp_u32);
	}

}

void motorDriverDRV8711GetConfig(DRV8711Configuration* pConfiguration_pst) {
	// Index variable
	uint32_t lIndex_u32 = 0;

	uint32_t lRegisterValue_u32 = 0;

	for (lIndex_u32 = 0; lIndex_u32 < (sizeof(DRV8711Configuration) / sizeof(uint32_t)); lIndex_u32++) {
		motorDriverDRV8711ReadRegister(lIndex_u32, &lRegisterValue_u32);
		((uint32_t*) pConfiguration_pst)[lIndex_u32] = lRegisterValue_u32;
	}
}

void motorDriverDRV8711EnableMotor(void) {
	// Register value
	uint32_t lRegisterValue_u32 = 0;
	motorDriverDRV8711ReadRegister(DRV8711_REG_CTRL, &lRegisterValue_u32);
	lRegisterValue_u32 |= DRV8711_ENABLE_MOTOR;
	motorDriverDRV8711WriteRegister(DRV8711_REG_CTRL, lRegisterValue_u32);
	sgMotorEnabled_b = true;
}

void motorDriverDRV8711DisableMotor(void) {
	// Register value
	uint32_t lRegisterValue_u32 = 0;

	motorDriverDRV8711ReadRegister(DRV8711_REG_CTRL, &lRegisterValue_u32);
	lRegisterValue_u32 &= ~DRV8711_ENABLE_MOTOR;
	motorDriverDRV8711WriteRegister(DRV8711_REG_CTRL, lRegisterValue_u32);
	sgMotorEnabled_b = false;
}

void motorDriverDRV8711Reset(void) {
	GPIOPinWrite(DRV8711_RESET_PIN_PORT, DRV8711_RESET_PIN, DRV8711_RESET_PIN);
	ioHandlerSystemDelay(1000);
	GPIOPinWrite(DRV8711_RESET_PIN_PORT, DRV8711_RESET_PIN, 0);
	ioHandlerSystemDelay(1000);
}

void motorDriverDRV8711WakeUp(void) {
	GPIOPinWrite(DRV8711_SLEEP_PIN_PORT, DRV8711_SLEEP_PIN, DRV8711_SLEEP_PIN);
}

void motorDriverDRV8711Sleep(void) {
	GPIOPinWrite(DRV8711_SLEEP_PIN_PORT, DRV8711_SLEEP_PIN, 0);
}

uint32_t motorDriverDRV8711ReadStatus(void) {
	uint32_t lStatus_u32 = 0;
	motorDriverDRV8711ReadRegister(DRV8711_REG_STATUS, &lStatus_u32);
	return lStatus_u32;
}

void motorDriverDRV8711ClearStatus(uint32_t pMask_u32) {
	motorDriverDRV8711WriteRegister(DRV8711_REG_STATUS, 0);
}

void motorDriverDRV8711ChangeDirection(void) {
	static uint8_t slDirection_u8 = 0;
	GPIOPinWrite(DRV8711_DIRECTION_PIN_PORT, DRV8711_DIRECTION_PIN, slDirection_u8);
	slDirection_u8 = ~slDirection_u8;
}

void motorDriverDRV8711StepOne(void) {
	GPIOPinWrite(DRV8711_STEP_PIN_PORT, DRV8711_STEP_PIN, DRV8711_STEP_PIN);
	ioHandlerSystemDelay(1000);
	GPIOPinWrite(DRV8711_STEP_PIN_PORT, DRV8711_STEP_PIN, 0);
	ioHandlerSystemDelay(100);
}

void motorDriverDRV8711ChangeStepMode(uint32_t pStepMode_u32) {
	// Mask for mode select
	static const uint8_t slModeMask_cu8 = 0x78;
	// Control register value
	uint32_t lRegisterValue_u32 = 0;

	if (((pStepMode_u32 & slModeMask_cu8) >> 3) < 9) {
		motorDriverDRV8711ReadRegister(DRV8711_REG_CTRL, &lRegisterValue_u32);
		lRegisterValue_u32 &= ~slModeMask_cu8;
		lRegisterValue_u32 |= pStepMode_u32;
		motorDriverDRV8711WriteRegister(DRV8711_REG_CTRL, lRegisterValue_u32);
	}
}

void motorDriverDRV8711Control(uint32_t pStepsToGo_u32) {
	// Add the steps to the counter
	sgStepsToGo_u32 += pStepsToGo_u32;
	if (sgStepsToGo_u32 > 0) {
		sgRunning_b = true;
	}
	// TODO
}

void motorDriverDRV8711SetTorque(uint8_t pTorque_u8) {
	uint32_t lRegisterValue_u32 = 0;
	motorDriverDRV8711ReadRegister(DRV8711_REG_TORQUE, &lRegisterValue_u32);
	lRegisterValue_u32 = (lRegisterValue_u32 & 0xFF00) | pTorque_u8;
	motorDriverDRV8711WriteRegister(DRV8711_REG_TORQUE, lRegisterValue_u32);
}

int32_t motorDriverOnDRV8711Interrupt(void) {
	uint32_t lStatus_u32 = 0;
	lStatus_u32 = (uint32_t) motorDriverDRV8711ReadStatus();
	if (0 != lStatus_u32 && NULL != sgInterruptCallback_pf) {
		sgInterruptCallback_pf(lStatus_u32);
		return 0;
	}
	// Unexpected interrupt
	return -1;
}

#if (DRV8711_SPEED_CONTROLLER_ENABLED == TRUE)

int32_t motorDriverOnDRV8711StepTimerInterrupt(void) {
	static uint8_t lStepState_u8 = 0;
	static bool lSleeping_b = false;
	static bool lRunnging_b = false;
	static uint32_t lRegister_u32 = 0;

	uint32_t lIntStatus_u32 = 0;

	lIntStatus_u32 = TimerIntStatus(DRV8711_STEPPER_TIMER_BASE, true);
	TimerIntClear(DRV8711_STEPPER_TIMER_BASE, DRV8711_STEPPER_TIMER_INTERRUPT);

	if (true == sgMotorEnabled_b && sgStepsToGo_u32 > 0) {
		lRunnging_b = true;

		if (true == lSleeping_b) {
			lSleeping_b = false;
			motorDriverDRV8711WakeUp();
		}

		sgStepsToGo_u32--;
		GPIOPinWrite(DRV8711_STEP_PIN_PORT, DRV8711_STEP_PIN, lStepState_u8);
		lStepState_u8 = ~lStepState_u8;
	} else if (true == lRunnging_b) {
		lRunnging_b = false;
		if (false == sgEnableHoldingTorque_b) {
			motorDriverDRV8711Sleep();
			lSleeping_b = true;
		} else {
			motorDriverDRV8711WriteRegister(DRV8711_REG_TORQUE, lRegister_u32);
		}

	}
	return lIntStatus_u32;
}


/*
int32_t motorDriverOnDRV8711StepTimerInterrupt(void) {

#if (DRV8711_DEFAULT_ENABLE_HOLDING_TORQUE == TRUE)
	static MotorState lMotorState_e = HOLDING;
#else
	static MotorState lMotorState_e = SLEEPING;
#endif

	static uint8_t lStepState_u8 = 0;

	if(false == sgMotorEnabled_b) {
		return 0;
	}

	// Motor driver state machine
	switch (lMotorState_e) {

	case RUNNING:
		// Running state, while sgStepsToGo_u32 is not equal to zero and motor is enabled toggle the
		// step pin, otherwise go to sleep or holding state.

		if(0 == sgStepsToGo_u32) {

			if(true == sgEnableHoldingTorque_b) {
				lMotorState_e = HOLDING;
			} else {
				lMotorState_e = SLEEPING;
			}

		} else {
			GPIOPinWrite(DRV8711_STEP_PIN_PORT, DRV8711_STEP_PIN, lStepState_u8);
			lStepState_u8 = ~lStepState_u8;
			sgStepsToGo_u32--;
		}

		break;

	case IDLE:
		// Idle state, if sgStepsToGo_u32 greater than zero and motor is enabled, then wake up
		// DRV8711 if it is sleeping, then change the torque to the default value, then go to
		// running state
		if(sgStepsToGo_u32 > 0) {
			lMotorState_e = RUNNING;
			if(true == slSleeping_b) {
				motorDriverDRV8711WakeUp();
				sgSleepMode_b = false;
			}

			motorDriverDRV8711SetTorque(sgConfiguration_pst->iTORQUE_u32 & 0xFF);
		}
		break;

	case HOLDING:
		// Set holding torque and go to idle state
		motorDriverDRV8711SetTorque(sgHoldingTorque_u8);
		lMotorState_e = IDLE;
		break;

	case SLEEPING:
		// Put DRV8711 into sleep mode and go to idle state
		motorDriverDRV8711Sleep();
		sgSleepMode_b = true;
		lMotorState_e = IDLE;
		break;
	}

}

*/

#else

int32_t motorDriverOnDRV8711StepTimerInterrupt(void) {
	static SpeedControllerState lState_e = STOP;

	switch(lState_e) {

		case STOP:
		break;

		case ACCELERATION:
		break;

		case DECELARATION:
		break;

		case RUNNING:
		break;

		default:
		lState_e = STOP;
		break;

	}
	return 0;
}

#endif
