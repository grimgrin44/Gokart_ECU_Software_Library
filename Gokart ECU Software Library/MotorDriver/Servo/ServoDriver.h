/**
 * @file	Servo.h
 * @authors	Gabor Kormendy
 * @date	2016. 01. 17.
 * @brief	Contains the function and variable declaration for servo motor driving.
 */

#ifndef MOTORDRIVER_SERVO_SERVODRIVER_H_
#define MOTORDRIVER_SERVO_SERVODRIVER_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <driverlib/sysctl.h>
#include <driverlib/pwm.h>
#include <driverlib/gpio.h>

#include "../../IOHandler/IOHandlerDefs.h"
#include "../../IOHandler/PWM/IOHandlerPWM.h"

#include "../../ECUConfiguration.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

/// Servo PWM period in Hz
#define SERVO_PWM_PERIOD_IN_HZ					ECU_CONFIGURATION_SERVO_PWM_PERIOD_IN_HZ

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

/// Servo number enumaration
enum ServoNumber {
	SERVO_1 = 0,
	SERVO_2 = 1,
	NUM_SERVOS
};

// ----------------------------------------------------------------------------
// Variable declaration.
// ----------------------------------------------------------------------------



// ----------------------------------------------------------------------------
// Function declaration.
// ----------------------------------------------------------------------------

GOKART_API	void 	motorDriverInitServo(IOHandlerPWMOutput* pPWMOutput_pst);
GOKART_API	int32_t	motorDriverServoInitServo(uint32_t pServo_u32, uint32_t pHomePosition_u32, bool pEnable_b);
GOKART_API	int32_t motorDriverServoSetPosition(uint32_t pServo_u32, uint32_t pPosition_u32);
GOKART_API	int32_t motorDriverServoRawWrite(uint32_t pServo_u32, uint32_t pRawValue_u32);
GOKART_API	int32_t motorDriverServoToHomePosition(uint32_t pServo_u32);
GOKART_API	int32_t motorDriverServoDisable(uint32_t pServo_u32);
GOKART_API	int32_t motorDriverServoEnable(uint32_t pServo_u32);

#endif /* MOTORDRIVER_SERVO_SERVODRIVER_H_ */
