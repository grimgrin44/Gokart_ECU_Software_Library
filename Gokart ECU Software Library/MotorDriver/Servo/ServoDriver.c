/**
 * @file	Servo.c
 * @authors	Gabor Kormendy
 * @date	2016. 01. 17.
 * @brief	Contains the function and variable declaration for servo motor driving.
 */


#include "ServoDriver.h"


/// Servo PWM range in us
#define SERVO_PWM_RANGE		(ECU_CONFIGURATION_SERVO_PWM_MAX_PULSE_WIDTH_IN_US - ECU_CONFIGURATION_SERVO_PWM_MIN_PULSE_WIDTH_IN_US)
/// Servo PWM atomic value for calculation
#define SERVO_PWM_ATOMIC	(SERVO_PWM_RANGE / 180)

/// PWM signal period divider
static uint32_t sgPWMPeriod_u32 = 0;
/// PWM duty cycle resolution
static float sgPWMAtomic_f32 = 0;
/// Servo home positions
static uint32_t sgServoHomePositions_pu32[NUM_SERVOS] = { 0, 0 };
/// Array for PWM outputs
static const enum PWMOutput sgServoOutputs_pe[NUM_SERVOS] = { PWM_OUTPUT_1, PWM_OUTPUT_2 };

static IOHandlerPWMOutput sgPWMOutput_st;

/**
 * @brief	This function initializes the PWM module used to drive the motors.
 * @param	none
 * @return	none
 */
void motorDriverInitServo(IOHandlerPWMOutput* pPWMOutput_pts) {

	__ASSERT(pPWMOutput_pts == NULL);

	sgPWMOutput_st.iPWMModule_e = pPWMOutput_pts->iPWMModule_e;
	sgPWMOutput_st.iPWMGenerator_e = pPWMOutput_pts->iPWMGenerator_e;

	ioHandlerPWMInitModule(&sgPWMOutput_st, SERVO_PWM_PERIOD_IN_HZ);

	sgPWMAtomic_f32 = ((float) ioHandlerPWMGetPWMClockFrequency()) / 1000 / 1000;

	sgPWMPeriod_u32 = ioHandlerPWMGetPWMPeriod(&sgPWMOutput_st);

	sgPWMOutput_st.iPWMOutput_e = PWM_OUTPUT_1;
	ioHandlerPWMSetRawDutyCycle(&sgPWMOutput_st, sgPWMPeriod_u32 / 50);
	sgPWMOutput_st.iPWMOutput_e = PWM_OUTPUT_2;
	ioHandlerPWMSetRawDutyCycle(&sgPWMOutput_st, sgPWMPeriod_u32 / 50);
}

/**
 * @brief	This function initializes the servo motor
 * @param	pServo_u32			The servo motor
 * @param	pHomePosition_u32	The home position of the servo (0 - 180 in degrees)
 * @param	pEnable_b			The desired enable state after the initialization
 * @return	It returns 0 on success or with an error code
 */
int32_t	motorDriverServoInitServo(uint32_t pServo_u32, uint32_t pHomePosition_u32, bool pEnable_b) {

	__ASSERT(!(pServo_u32 < NUM_SERVOS));
	__ASSERT(pHomePosition_u32 > 180);

	sgServoHomePositions_pu32[pServo_u32] = pHomePosition_u32;
	motorDriverServoSetPosition(pServo_u32, pHomePosition_u32);

	sgPWMOutput_st.iPWMOutput_e = sgServoOutputs_pe[pServo_u32];
	ioHandlerPWMSetOutputState(&sgPWMOutput_st, pEnable_b);
	return 0;
}

/**
 * @brief	This function sets the servo motor position
 * @param	pServo_u32		The servo motor
 * @param	pPosition_u32	The desired position of the rotor (0 - 180 in degrees)
 * @return	It returns 0 on success or with an error code
 */
int32_t motorDriverServoSetPosition(uint32_t pServo_u32, uint32_t pPosition_u32) {
	float32_t lDutyCycle_f32 = 0;

	__ASSERT(!(pServo_u32 < NUM_SERVOS));
	__ASSERT(pPosition_u32 > 180);

	lDutyCycle_f32 = (( (float) pPosition_u32 / 180.0) * SERVO_PWM_RANGE + ECU_CONFIGURATION_SERVO_PWM_MIN_PULSE_WIDTH_IN_US) * sgPWMAtomic_f32;

	sgPWMOutput_st.iPWMOutput_e = sgServoOutputs_pe[pServo_u32];
	ioHandlerPWMSetRawDutyCycle(&sgPWMOutput_st, (uint16_t) lDutyCycle_f32);
	return 0;
}

/**
 * @brief	This function moves the servo motor to its home position
 * @param	pServo_u32	The servo motor
 * @return	It returns 0 on success or with an error code
 */
int32_t motorDriverServoToHomePosition(uint32_t pServo_u32) {
	__ASSERT(!(pServo_u32 < NUM_SERVOS));

	return motorDriverServoSetPosition(pServo_u32, sgServoHomePositions_pu32[pServo_u32]);
}

/**
 * @brief	This function disables the servo motor PWM output
 * @param	pServo_u32	The servo motor
 * @return	It returns 0 on success or with an error code
 */
int32_t motorDriverServoDisable(uint32_t pServo_u32) {
	__ASSERT(!(pServo_u32 < NUM_SERVOS));

	sgPWMOutput_st.iPWMOutput_e = sgServoOutputs_pe[pServo_u32];
	ioHandlerPWMSetOutputState(&sgPWMOutput_st, false);
	return 0;
}

/**
 * @brief	This function enables the servo motor PWM output
 * @param	pServo_u32	The servo motor
 * @return	It returns 0 on success or with an error code
 */
int32_t motorDriverServoEnable(uint32_t pServo_u32) {
	__ASSERT(!(pServo_u32 < NUM_SERVOS));

	sgPWMOutput_st.iPWMOutput_e = sgServoOutputs_pe[pServo_u32];
	ioHandlerPWMSetOutputState(&sgPWMOutput_st, false);
	return 0;
}

/**
 * @brief	Raw write to the servo duty cycle
 * @param	pServo_u32		The servo motor
 * @param	pRawValue_u32	The raw value
 * @return	It returns 0 on success or with an error code
 */
int32_t motorDriverServoRawWrite(uint32_t pServo_u32, uint32_t pRawValue_u32) {
	__ASSERT(!(pServo_u32 < NUM_SERVOS));

	sgPWMOutput_st.iPWMOutput_e = sgServoOutputs_pe[pServo_u32];
	ioHandlerPWMSetRawDutyCycle(&sgPWMOutput_st, (uint16_t) pRawValue_u32);
	return 0;
}
