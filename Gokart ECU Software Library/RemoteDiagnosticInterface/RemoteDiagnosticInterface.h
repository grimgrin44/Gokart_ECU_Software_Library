/*
 * RemoteDiagnosticInterface.h
 *
 *  Created on: 2016 jan. 13
 *      Author: Grimgrin
 */

#ifndef REMOTEDIAGNOSTICINTERFACE_REMOTEDIAGNOSTICINTERFACE_H_
#define REMOTEDIAGNOSTICINTERFACE_REMOTEDIAGNOSTICINTERFACE_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "../IOHandler/CAN/IOHandlerCAN.h"
#include "../IOHandler/System/IOHandlerSystem.h"
#include "../IOHandler/EEPROM/IOHandlerEEPROM.h"
#include "../IOHandler/UART/IOHandlerUART.h"
#include "../IOHandler/Systick/IOHandlerSystick.h"

#include "../ECUConfiguration.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

#define REMOTE_DIAGNOSTIC_INTERFACE_RESPONSE_MESSAGE_ID			0x703

#define REMOTE_DIAGNOSTIC_INTERFACE_REQUEST_MESSAGE_ID			0x702

#define REMOTE_DIAGNOSTIC_INTERFACE_OVER_UART					FALSE

#define REMOTE_DIAGNOSTIC_INTERFACE_TEST_TABLE_SIZE				8

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

enum RDIRequestType {
	RDI_REQ_OPCTL = 0,
	RDI_REQ_TESTCALL,
	RDI_REQ_IDENTDEV,
	RDI_REQ_READMEM,
	RDI_REQ_SOFTRESET,
	NUM_RDI_REQESTS
};

enum RDIOperationMode {
	RDI_OP_DEFAULT = 0,
	RDI_OP_TESTMODE,
	NUM_RDI_OP_MODES
};

enum RDIReadMemoryType {
	RDI_READMEM_EEPROM = 0,
	RDI_READMEM_SRAM,
	NUM_RDI_READMEM_TYPES
};

enum RDIResponseType {
	RDI_RESP_READMEM = 3,
	RDI_RESP_ERROR = 15
};

enum RDIResponseError {
	RDI_ERR_INVALID_FUNCID = 1,
	RDI_ERR_INVALID_MODE,
	RDI_ERR_INVALID_ADDRESS,
	RDI_ERR_INVALID_TYPE,
	RDI_ERR_TEST_MODE_OFF,
	RDI_ERR_INVALID_CHECKSUM
};

typedef struct RDIRequest {
	/// The Device ID
	uint8_t iDeviceID_u8;
	/// The request type
	uint8_t iRDIRequestType_u8;
	/// The request command
	uint8_t iRequestCommand_u8;
	/// The request data
	uint8_t iRequestData_pu8[4];
	/// The request checksum
	uint16_t iRequestChecksum_u16;
} RDIRequest;

typedef struct RDIResponse {
	/// The response type
	uint8_t iRDIResponseType_u8;
	/// The request command
	uint8_t iResponseCommand_u8;
	/// The response data
	uint8_t iResponseData_pu8[4];
} RDIResponse;

typedef int32_t (*RDITestFunction)(uint32_t pParameter_u32);

typedef int32_t (*RDIOperationChangeFunction)(uint8_t pOperationMode_u8);

// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------

GOKART_API	void	remoteDiagnosticInterfaceInit(void);
GOKART_API	bool 	remoteDiagnosticInterfaceCANMessageFilter(uint32_t pMessageID_u32);
GOKART_API	int32_t	remoteDiagnosticInterfaceCANMessageHandler(IOHandlerCANMessage* pReceivedMessage_pst);
GOKART_API	int32_t remoteDiagnosticInterfaceSetTestFunctionTable(RDITestFunction* pTestFunctionTable_pf);
GOKART_API	int32_t remoteDiagnosticInterfaceSetOperationChangeFunction(RDIOperationChangeFunction pOperationChangeFunction_pf);

#endif /* REMOTEDIAGNOSTICINTERFACE_REMOTEDIAGNOSTICINTERFACE_H_ */
