/*
 * RemoteDiagnosticInterface.c
 *
 *  Created on: 2016 jan. 13
 *      Author: Grimgrin
 */

#include "RemoteDiagnosticInterface.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

#define REMOTE_DIAGNOSTIC_INTERFACE_SRAM_ADDRESS_VALID(address)			(address >= MEMORY_SRAM_ADDRESS_START && address < MEMORY_SRAM_ADDRESS_END)

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

static RDIOperationChangeFunction sgOperationChangeFunction_pf = NULL;

static RDITestFunction* sgTestFunctionTable_pf = NULL;

static enum RDIOperationMode sgCurrentMode_e = RDI_OP_DEFAULT;

// ----------------------------------------------------------------------------
// Function definitions.
// ----------------------------------------------------------------------------

/**
 * @brief	This function calculates the checksum for the given byte array
 *
 * @param	pData_pu8	The byte array
 * @param	pLength_u8	The length of the data
 *
 * @return	The checksum (16 bit)
 */
static __inline uint16_t remoteDiagnosticInterfaceCalculateChecksum(uint8_t* lData_pu8, uint8_t pLength_u8) {
	uint8_t lIndex_u8 = 0;
	uint16_t lChecksum_u16 = 0;
	for(lIndex_u8 = 0; lIndex_u8 < 6; lIndex_u8++) {
		lChecksum_u16 += lData_pu8[lIndex_u8];
	}
	return lChecksum_u16;
}


/**
 * @brief	This function sends negative response
 *
 * @param	pRequest_pst	The request structure
 * @param	pErrorCode_u32	The error code
 *
 * @return	none
 */
static void remoteDiagnosticInterfaceSendNegativeResponse(uint32_t pErrorCode_u32) {
	uint8_t lResponsePayload_pu8[8] = { 0 };
	uint16_t lChecksum_u16 = 0;

	lResponsePayload_pu8[0] = (ECU_CONFIGURATION_DEVICE_ID << 4) & 0x0F;
	lResponsePayload_pu8[1] = pErrorCode_u32;
	lChecksum_u16 = remoteDiagnosticInterfaceCalculateChecksum(lResponsePayload_pu8, 6);
	memcpy(&(lResponsePayload_pu8[6]), &lChecksum_u16, 2);

#if (REMOTE_DIAGNOSTIC_INTERFACE_OVER_UART == FALSE)
	ioHandlerCANSendMessageRaw(REMOTE_DIAGNOSTIC_INTERFACE_RESPONSE_MESSAGE_ID, lResponsePayload_pu8, 8);
#else
	ioHandlerUARTSendData(lResponsePayload_pu8, 8);
#endif

}

/**
 * @brief	This function builds up and sends the response
 *
 * @param	pResponse_pst	The response structure
 *
 * @return	none
 */
static void remoteDiagnosticInterfaceSendResponse(RDIResponse* pResponse_pst) {
	uint8_t lResponsePayload_pu8[8] = { 0 };
	uint16_t lChecksum_u16 = 0;

	lResponsePayload_pu8[0] = ECU_CONFIGURATION_DEVICE_ID << 4 | pResponse_pst->iRDIResponseType_u8;

	switch(pResponse_pst->iRDIResponseType_u8) {

	case RDI_RESP_READMEM:
		lResponsePayload_pu8[1] = pResponse_pst->iResponseCommand_u8;
		memcpy(&lResponsePayload_pu8[2], pResponse_pst->iResponseData_pu8, 4);
		break;

	default:
		// Invalid response type
		return;
	}

	lChecksum_u16 = remoteDiagnosticInterfaceCalculateChecksum(lResponsePayload_pu8, 6);
	lResponsePayload_pu8[6] = lChecksum_u16 >> 8;
	lResponsePayload_pu8[7] = (uint8_t) lChecksum_u16;

#if (REMOTE_DIAGNOSTIC_INTERFACE_OVER_UART == FALSE)
	ioHandlerCANSendMessageRaw(REMOTE_DIAGNOSTIC_INTERFACE_RESPONSE_MESSAGE_ID, lResponsePayload_pu8, 8);
#else
	ioHandlerUARTSendData(lResponsePayload_pu8, 8);
#endif
}

/**
 * @brief	This function extracts the request data from the given byte array
 *
 * @param	pPayload_pu8	The byte array
 * @param	pRequest_pst	The request structure to be filled up
 *
 * @return	none
 */
static void remoteDiagnosticInterfaceGetRequest(uint8_t* pPayload_pu8, RDIRequest* pRequest_pst) {
	pRequest_pst->iDeviceID_u8 = pPayload_pu8[0] >> 4;
	pRequest_pst->iRDIRequestType_u8 = pPayload_pu8[0] & 0x0F;
	pRequest_pst->iRequestCommand_u8 = pPayload_pu8[1];
	memcpy(pRequest_pst->iRequestData_pu8, &pPayload_pu8[2], 4);
	memcpy(&pRequest_pst->iRequestChecksum_u16, &pPayload_pu8[6], 2);
}

/**
 * @brief	This function initializes the remote diagnostic interface and registers the CAN message handler
 *
 * @return none
 */
void remoteDiagnosticInterfaceInit(void) {
	ioHandlerCANAddMessageHandler(remoteDiagnosticInterfaceCANMessageHandler);
}

/**
 * @brief	This function handles the received RDI CAN message
 *
 * @param	pReceivedMessage_pst	The received CAN message object
 *
 * @return	It returns 0 on success or with an error code
 */
int32_t	remoteDiagnosticInterfaceCANMessageHandler(IOHandlerCANMessage* pReceivedMessage_pst) {
	RDIRequest lRequest_st = { 0 };
	RDIResponse lResponse_st = { 0 };

	// Check the message ID
	if(pReceivedMessage_pst->iID_u32 != REMOTE_DIAGNOSTIC_INTERFACE_REQUEST_MESSAGE_ID) {
		return -1;
	}

	remoteDiagnosticInterfaceGetRequest(pReceivedMessage_pst->iData, &lRequest_st);

	if(lRequest_st.iDeviceID_u8 != ECU_CONFIGURATION_DEVICE_ID) {
		// Device id doesn't match
		return 0;
	}

	if(lRequest_st.iRequestChecksum_u16 != remoteDiagnosticInterfaceCalculateChecksum(pReceivedMessage_pst->iData, 6)) {
		// Wrong checksum
		remoteDiagnosticInterfaceSendNegativeResponse(RDI_ERR_INVALID_CHECKSUM);
		return 0;
	}

	switch(lRequest_st.iRDIRequestType_u8) {

	/*
	 * Soft reset call
	 */
	case RDI_REQ_SOFTRESET:
		SysCtlReset();
		while(1) {
			/// Infinite loop
		}

	/*
	 * Read memory call
	 */
	case RDI_REQ_READMEM: {
		uint32_t lAddress_u32 = 0;
		memcpy((uint8_t*) &lAddress_u32, lRequest_st.iRequestData_pu8, 4);
		if(RDI_READMEM_EEPROM == lRequest_st.iRequestCommand_u8) {
			IOHandlerEEPROMData lReadData_st = { 0 };
			uint32_t lValue_u32 = 0;
			lReadData_st.iAddress_u32 = lAddress_u32;
			lReadData_st.iLength_u32 = 4;
			lReadData_st.iData_pu32 = &lValue_u32;
			if(0 == ioHandlerEEPROMReadData(&lReadData_st)) {
				lResponse_st.iRDIResponseType_u8 = RDI_RESP_READMEM;
				memcpy(lResponse_st.iResponseData_pu8, &lValue_u32, 4);
			} else {
				remoteDiagnosticInterfaceSendNegativeResponse(RDI_ERR_INVALID_ADDRESS);
				return 0;
			}
		} else if(RDI_READMEM_SRAM == lRequest_st.iRequestCommand_u8) {
			if(REMOTE_DIAGNOSTIC_INTERFACE_SRAM_ADDRESS_VALID(lAddress_u32)) {
				lResponse_st.iRDIResponseType_u8 = RDI_RESP_READMEM;
				lResponse_st.iResponseCommand_u8 = lRequest_st.iRequestCommand_u8;
				uint32_t lValue_u32 = *((uint32_t*) lAddress_u32);
				memcpy(lResponse_st.iResponseData_pu8, &lValue_u32, 4);
			} else {
				remoteDiagnosticInterfaceSendNegativeResponse(RDI_ERR_INVALID_ADDRESS);
				return 0;
			}
		} else {
			remoteDiagnosticInterfaceSendNegativeResponse(RDI_ERR_INVALID_TYPE);
			return 0;
		}


		break;
	}

	/*
	 * Ident device call
	 */
	case RDI_REQ_IDENTDEV:
		ioHandlerIdentDevice(lRequest_st.iRequestCommand_u8);
		break;

	/*
	 * Change operation mode call
	 */
	case RDI_REQ_OPCTL:
		if(RDI_OP_DEFAULT == lRequest_st.iRequestCommand_u8) {

			if(sgCurrentMode_e != lRequest_st.iRequestCommand_u8) {
				sgOperationChangeFunction_pf(lRequest_st.iRequestCommand_u8);
				ioHandlerSchedulerResume();
				sgCurrentMode_e = RDI_OP_DEFAULT;
			}

		} else if(RDI_OP_TESTMODE == lRequest_st.iRequestCommand_u8) {

			if(sgCurrentMode_e != lRequest_st.iRequestCommand_u8) {
				ioHandlerSchedulerStop();
				sgOperationChangeFunction_pf(lRequest_st.iRequestCommand_u8);
				sgCurrentMode_e = RDI_OP_DEFAULT;
			}

		} else {
			remoteDiagnosticInterfaceSendNegativeResponse(RDI_ERR_INVALID_MODE);
			return 0;
		}
		break;

	/*
	 * Test function call
	 */
	case RDI_REQ_TESTCALL:
		if(RDI_OP_TESTMODE == sgCurrentMode_e) {
			uint32_t lParameter_u32 = 0;
			memcpy(&lParameter_u32, lRequest_st.iRequestData_pu8, 4);
			if(lRequest_st.iRequestCommand_u8 < REMOTE_DIAGNOSTIC_INTERFACE_TEST_TABLE_SIZE) {
				if(NULL != sgTestFunctionTable_pf[lRequest_st.iRequestCommand_u8]) {
					// Call test function
					sgTestFunctionTable_pf[lRequest_st.iRequestCommand_u8](lParameter_u32);
				}
			} else {
				remoteDiagnosticInterfaceSendNegativeResponse(RDI_ERR_INVALID_FUNCID);
			}
			return 0;
		} else {
			remoteDiagnosticInterfaceSendNegativeResponse(RDI_ERR_TEST_MODE_OFF);
			return 0;
		}

	}

	// Send response
	remoteDiagnosticInterfaceSendResponse(&lResponse_st);

	return 0;
}

/**
 * @brief	This function sets the test function table
 *
 * @param	pTestFunctionTable_pf		The test function table
 *
 * @return	This function is always returns with 0
 */
int32_t remoteDiagnosticInterfaceSetTestFunctionTable(RDITestFunction* pTestFunctionTable_pf) {
	__ASSERT(NULL == pTestFunctionTable_pf);
	sgTestFunctionTable_pf = pTestFunctionTable_pf;
	return 0;
}

/**
 * @brief	This function sets the operation change function
 *
 * @param	pOperationChangeFunction_pf		Function pointer to the operation change function
 *
 * @return 	This function is always returns with 0
 */
int32_t remoteDiagnosticInterfaceSetOperationChangeFunction(RDIOperationChangeFunction pOperationChangeFunction_pf) {
	__ASSERT(NULL == pOperationChangeFunction_pf);
	sgOperationChangeFunction_pf = pOperationChangeFunction_pf;
	return 0;
}





