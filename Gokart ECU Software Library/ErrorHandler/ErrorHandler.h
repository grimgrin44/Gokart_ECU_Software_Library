/**
 * File: 			ErrorHandler.h
 * File Version:	1.0
 * Project: 		GoKart
 * Project Version: 1.0
 * Folder: 			ErrorHandler
 * Creators:		-
 * Last Mod. Date:	2015.05.12.
 * Reviewers:		-
 * Last Rev. Date:	-
 *
 * Contains the function and variable definitions for the
 * error handler functions.
 */

#ifndef ERRORHANDLER_ERRORHANDLER_H_
#define ERRORHANDLER_ERRORHANDLER_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include "../ECUConfiguration.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

#ifndef NULL
#define NULL 											 	0
#endif
// The error code of the CAN receive message error.
#define ERROR_HANDLER_CAN_RX_ERROR_CODE						-100
// The error code of the CAN receive buffer is full error.
#define ERROR_HANDLER_CAN_RX_BUFFER_IS_FULL_ERROR_CODE		-101
// The error code of the CAN transmit buffer is full error.
#define ERROR_HANDLER_CAN_TX_BUFFER_IS_FULL_ERROR_CODE		-102


// The error code of the general timer: task pool is full
#define ERROR_HANDLER_TIMER_TASK_POOL_IS_FULL				-200
// The error code of the general timer: task function is null
#define ERROR_HANDLER_TIMER_TASK_FUNCTION_IS_NULL			-201


// The error code of the uninitialized EEPROM module
#define ERROR_HANDLER_EEPROM_UNINITIALIZED					-300
// The error code of the EEPROM module read
#define ERROR_HANDLER_EEPROM_READ_ERROR						-301
// The error code of the EEPROM module write
#define ERROR_HANDLER_EEPROM_WRITE_ERROR					-302
// The error code of the EEPROM module mass erase
#define ERROR_HANDLER_EEPROM_ERASE_ERROR					-303


// The error code of the scheduler no such task error.
#define ERROR_HANDLER_SCHEDULER_NO_SUCH_TASK				-150

// The error code of the parameter management: block is null
#define ERROR_HANDLER_PARAMETER_MANAGEMENT_BLOCK_IS_NULL		-500
// The error code of the parameter management read error
#define ERROR_HANDLER_PARAMETER_MANAGEMENT_EEPROM_READ_ERROR	-501
// The error code of the parameter management write error
#define ERROR_HANDLER_PARAMETER_MANAGEMENT_EEPROM_WRITE_ERROR	-502

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// function declartions.
// ----------------------------------------------------------------------------


#endif /* ERRORHANDLER_ERRORHANDLER_H_ */
