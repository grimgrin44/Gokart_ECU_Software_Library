/**
 * @file	AlgorithmPID.h
 * @authors	Grimgrin
 * @date	2016 jan. 27
 * @brief	
 */
#ifndef ALGORITHM_PID_ALGORITHMPID_H_
#define ALGORITHM_PID_ALGORITHMPID_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "../../ECUConfiguration.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

typedef struct PIDConfiguration {
	/// P value
	uint32_t iP_u32;
	/// I value
	uint32_t iI_u32;
	/// D value
	uint32_t iD_u32;
	/// Lower limit value
	int32_t iLowerLimit_u32;
	/// Upper limit value
	int32_t iUpperLimit_u32;
} PIDConfiguration;

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------

GOKART_API	int32_t	algorithmPIDRun(PIDConfiguration* pPIDConfiguration_pst, int32_t pReferenceValue_i32, int32_t pControlledValue_i32);



#endif /* ALGORITHM_PID_ALGORITHMPID_H_ */
