/**
 * @file	GlobalDefinitions.h
 * @author	Gabor Kormendy
 * @version	1.0
 * @brief	This file contains global definitions for the whole library.
 */

#ifndef GLOBALDEFINITIONS_H_
#define GLOBALDEFINITIONS_H_

/************************************************************************************************************************
 *
 * DO NOT EDIT THIS SECTION!
 *
 *************************************************************************************************************************/

#ifndef	TRUE
#define TRUE	1
#endif

#ifndef FALSE
#define FALSE	0
#endif

#define INPUT				(0 << 0)

#define OUTPUT				(1 << 0)

#define DISABLE_EVENT		(0 << 1)

#define ENABLE_EVENT		(1 << 1)

#define FALLING_EDGE_EVENT	(0 << 2)

#define RISING_EDGE_EVENT	(1 << 2)

#define RESISTOR_DISABLE	(0 << 3)

#define RESISTOR_ENABLE		(1 << 3)

#define PULLUP_RESISTOR		(0 << 4)

#define PULLDOWN_RESISTOR	(1 << 4)


#define	___ASSERT(x)	\
	do { } while(x)

#define GOKART_API		extern

#define MEMORY_SRAM_ADDRESS_START	0x20000000

#define MEMORY_SRAM_SIZE			0x00008000

#define MEMORY_SRAM_ADDRESS_END		(MEMORY_SRAM_ADDRESS_START + MEMORY_SRAM_SIZE)

#endif /* GLOBALDEFINITIONS_H_ */
