/**
 * @file	ParameterManagement.c
 * @authors	Gabor Kormendy
 * @date	2015.06.10.
 * @brief	This file contains function and variable definitions for
 * 			parameter management in the EEPROM.
 */

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------
#include "ParameterManagement.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

/// The CAN message ID range start of the parameter control CAN frames
#define PARAMETER_MANAGEMENT_CAN_MESSAGE_ID_RANGE_START					800
/// The CAN message ID range end of the parameter control CAN frames
#define PARAMETER_MANAGEMENT_CAN_MESSAGE_ID_RANGE_END					804

#if (PARAMETER_MANAGEMENT_CAN_MESSAGE_ID_RANGE_START > PARAMETER_MANAGEMENT_CAN_MESSAGE_ID_RANGE_END)
#error "CAN message id range is not correct!"
#endif

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

enum {
	PM_MSG_ID_SETPARAM = PARAMETER_MANAGEMENT_CAN_MESSAGE_ID_RANGE_START,
	PM_MSG_ID_GETPARAM,
	PM_MSG_ID_GETPARAMRESP,
	PM_MSG_ID_SAVEPARAM,
	PM_MSG_ID_LOADPARAM = PARAMETER_MANAGEMENT_CAN_MESSAGE_ID_RANGE_END
};

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------

/// Pointer to the parameter block
static uint32_t* sgParameterBlock_pu32 = NULL;
/// Pointer to the default parameter block
static uint32_t* sgDefaultParameterBlock_pu32 = NULL;
/// The size of the parameter block
static uint32_t sgParameterBlockSize_u32 = 0;

// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------

/**
 * @brief	This function checks the parameter block end sequence.
 *
 * @param	pEEPROMData_pst		Structure with the data buffer, the EEPROM address and byte count
 *
 * @return	The last two byte is equals with the end sequnce it returns true.
 */
static bool parameterManagementIsParameterBlockValid(IOHandlerEEPROMData* pEEPROMData_pst) {
	// Temporary variables for sequence check
	uint32_t lSequence0_u32 = 0;
	uint32_t lSequence1_u32 = 0;

	lSequence0_u32 = pEEPROMData_pst->iData_pu32[0];
	lSequence1_u32 = pEEPROMData_pst->iData_pu32[1];

	if ((PARAMETER_MANAGEMENT_PARAMETER_BLOCK_END_SEQUENCE_0 == lSequence0_u32) && (PARAMETER_MANAGEMENT_PARAMETER_BLOCK_END_SEQUENCE_1 == lSequence1_u32)) {
		return true;
	}

	return false;
}

/**
 * @brief	This function checks the previously read data from EEPROM and searches for the first
 * 			non 0xFFFFFFFF data.
 *
 * @param	pEEPROMData_pst		Structure with the data buffer, the EEPROM address and byte count
 *
 * @return	If all data in the buffer is 0xFFFFFFFF, then it returns true, else false.
 */
bool parameterManagementIsEEPROMEmpty(IOHandlerEEPROMData* pEEPROMData_pst) {
	// Index variable
	uint32_t lIndex_u32 = 0;

	for (lIndex_u32 = 0; lIndex_u32 < (pEEPROMData_pst->iLength_u32 / 4); lIndex_u32++) {
		if (PARAMETER_MANAGEMENT_PARAMETER_BLOCK_EEPROM_EMPTY_VALUE != pEEPROMData_pst->iData_pu32[lIndex_u32]) {
			return false;
		}
	}

	return true;
}

/**
 * @brief	This function initializes the parameter management module and reads the parameter block
 * 			from EEPROM. If it is different from the parameter block programmed into the FLASH, it
 * 			will reprogram the EEPROM with the default parameters.
 *
 * @param	pDefaultParameterBlock_pu32		The default parameter block programmed into the FLASH memory
 * @param	pRuntimeParameterBlock_pu32		The runtime parameter block in the SRAM
 * @param	pParameterBlockSize_u32			The parameter block size in bytes
 *
 * @return	It returns 0 on success or with an error code
 */
int32_t parameterManagementInit(uint32_t* pDefaultParameterBlock_pu32, uint32_t* pRuntimeParameterBlock_pu32, uint32_t pParameterBlockSize_u32) {
	// EEPROM error code
	int32_t lEEPROMErrorCode_i32 = 0;
	// EEPROM io structure
	IOHandlerEEPROMData lEEPROMio_st = { 0 };
	// EEPROM io structure for end sequence checking
	IOHandlerEEPROMData lSequenceData_st = { 0 };
	// Buffer for end sequence reading
	uint32_t lSequenceBuffer_pu32[2];

	// Assign static variables
	sgParameterBlock_pu32 = pRuntimeParameterBlock_pu32;
	sgDefaultParameterBlock_pu32 = pDefaultParameterBlock_pu32;

	// Check the parameter block address
	if (NULL == sgParameterBlock_pu32 || NULL == sgDefaultParameterBlock_pu32) {
		return ERROR_HANDLER_PARAMETER_MANAGEMENT_BLOCK_IS_NULL;
	}

	sgParameterBlockSize_u32 = pParameterBlockSize_u32;

	// Check parameter block end sequence
	lSequenceData_st.iAddress_u32 = PARAMETER_MANAGEMENT_PARAMETER_BLOCK_EEPROM_START_ADDRESS + pParameterBlockSize_u32;
	lSequenceData_st.iData_pu32 = lSequenceBuffer_pu32;
	lSequenceData_st.iLength_u32 = 8;	// In bytes

	lEEPROMErrorCode_i32 = ioHandlerEEPROMReadData(&lSequenceData_st);

	if (lEEPROMErrorCode_i32 < 0) {
		return ERROR_HANDLER_PARAMETER_MANAGEMENT_EEPROM_READ_ERROR;
	}

	if (true == parameterManagementIsParameterBlockValid(&lSequenceData_st)) {
		// Parameter block is valid

		lEEPROMio_st.iAddress_u32 = PARAMETER_MANAGEMENT_PARAMETER_BLOCK_EEPROM_START_ADDRESS;
		lEEPROMio_st.iData_pu32 = sgParameterBlock_pu32;
		lEEPROMio_st.iLength_u32 = sgParameterBlockSize_u32;

		// Reading parameter block from EEPROM
		lEEPROMErrorCode_i32 = ioHandlerEEPROMReadData(&lEEPROMio_st);

		if (lEEPROMErrorCode_i32 < 0) {
			return ERROR_HANDLER_PARAMETER_MANAGEMENT_EEPROM_READ_ERROR;
		}

	} else {
		// Parameter block isn't valid, overwrite it with the default configuration

		// Erase EEPROM
		ioHandlerEEPROMErase();

		// Programming EEPROM with default parameters
		lEEPROMio_st.iAddress_u32 = PARAMETER_MANAGEMENT_PARAMETER_BLOCK_EEPROM_START_ADDRESS;
		lEEPROMio_st.iData_pu32 = pDefaultParameterBlock_pu32;
		lEEPROMio_st.iLength_u32 = pParameterBlockSize_u32;

		// Writing
		lEEPROMErrorCode_i32 = ioHandlerEEPROMWriteData(&lEEPROMio_st);

		// Error checking
		if (lEEPROMErrorCode_i32 < 0) {
			return ERROR_HANDLER_PARAMETER_MANAGEMENT_EEPROM_WRITE_ERROR;
		}

		// Program end sequence
		lSequenceBuffer_pu32[0] = PARAMETER_MANAGEMENT_PARAMETER_BLOCK_END_SEQUENCE_0;
		lSequenceBuffer_pu32[1] = PARAMETER_MANAGEMENT_PARAMETER_BLOCK_END_SEQUENCE_1;

		lSequenceData_st.iAddress_u32 = PARAMETER_MANAGEMENT_PARAMETER_BLOCK_EEPROM_START_ADDRESS + sgParameterBlockSize_u32;
		lSequenceData_st.iData_pu32 = lSequenceBuffer_pu32;
		lSequenceData_st.iLength_u32 = 8;

		// Writing
		lEEPROMErrorCode_i32 = ioHandlerEEPROMWriteData(&lSequenceData_st);

		// Error checking
		if (lEEPROMErrorCode_i32 < 0) {
			return ERROR_HANDLER_PARAMETER_MANAGEMENT_EEPROM_WRITE_ERROR;
		}

		// Read back parameters
		lEEPROMio_st.iAddress_u32 = PARAMETER_MANAGEMENT_PARAMETER_BLOCK_EEPROM_START_ADDRESS;
		lEEPROMio_st.iData_pu32 = sgParameterBlock_pu32;
		lEEPROMio_st.iLength_u32 = sgParameterBlockSize_u32;

		// Reading
		lEEPROMErrorCode_i32 = ioHandlerEEPROMReadData(&lEEPROMio_st);

		if (lEEPROMErrorCode_i32 < 0) {
			return ERROR_HANDLER_PARAMETER_MANAGEMENT_EEPROM_READ_ERROR;
		}
	}

	return NULL;
}

/**
 * @brief	This function saves the runtime parameter block into EEPROM
 *
 * @return	It returns 0 on success or with an error code.
 */
int32_t parameterManagementSave(void) {
	// EEPROM error code
	int32_t lEEPROMErrorCode_i32 = 0;
	// EEPROM io structure
	IOHandlerEEPROMData lEEPROMio_st = { 0 };
	// Check the parameter block address
	if (NULL == sgParameterBlock_pu32) {
		return ERROR_HANDLER_PARAMETER_MANAGEMENT_BLOCK_IS_NULL;
	}

	lEEPROMio_st.iAddress_u32 = PARAMETER_MANAGEMENT_PARAMETER_BLOCK_EEPROM_START_ADDRESS;
	lEEPROMio_st.iLength_u32 = sgParameterBlockSize_u32;
	lEEPROMio_st.iData_pu32 = sgParameterBlock_pu32;

	lEEPROMErrorCode_i32 = ioHandlerEEPROMWriteData(&lEEPROMio_st);

	if (lEEPROMErrorCode_i32 < 0) {
		return ERROR_HANDLER_PARAMETER_MANAGEMENT_EEPROM_WRITE_ERROR;
	} else {
		return NULL;
	}

}

/**
 * @brief	This function reads the whole parameter block from EEPROM.
 *
 * @param	pParameterBlock_pu32	The parameter block container
 *
 * @return	It returns 0 on success or with an error code
 */
int32_t parameterManagementRead(uint32_t* pParameterBlock_pu32) {
	// EEPROM error code
	int32_t lEEPROMErrorCode_i32 = 0;
	// EEPROM io structure
	IOHandlerEEPROMData lEEPROMio_st = { 0 };
	// Check the parameter block address
	if (NULL == sgParameterBlock_pu32) {
		return ERROR_HANDLER_PARAMETER_MANAGEMENT_BLOCK_IS_NULL;
	}

	lEEPROMio_st.iAddress_u32 = PARAMETER_MANAGEMENT_PARAMETER_BLOCK_EEPROM_START_ADDRESS;
	lEEPROMio_st.iLength_u32 = sgParameterBlockSize_u32;
	lEEPROMio_st.iData_pu32 = sgParameterBlock_pu32;

	lEEPROMErrorCode_i32 = ioHandlerEEPROMReadData(&lEEPROMio_st);

	if (lEEPROMErrorCode_i32 < 0) {
		return ERROR_HANDLER_PARAMETER_MANAGEMENT_EEPROM_READ_ERROR;
	} else {
		return NULL;
	}
}

/**
 * @brief	This function overwrites the parameter block in EEPROM with the default parameters from FLASH.
 *
 * @return	It returns 0 on success or with an error code
 */
int32_t parameterManagementRestoreDefault(void) {
	// EEPROM error code
	int32_t lEEPROMErrorCode_i32 = 0;
	// EEPROM io structure
	IOHandlerEEPROMData lEEPROMio_st = { 0 };
	// Check the parameter block address
	if (NULL == sgDefaultParameterBlock_pu32) {
		return ERROR_HANDLER_PARAMETER_MANAGEMENT_BLOCK_IS_NULL;
	}

	lEEPROMio_st.iAddress_u32 = PARAMETER_MANAGEMENT_PARAMETER_BLOCK_EEPROM_START_ADDRESS;
	lEEPROMio_st.iLength_u32 = sgParameterBlockSize_u32;
	lEEPROMio_st.iData_pu32 = sgDefaultParameterBlock_pu32;

	lEEPROMErrorCode_i32 = ioHandlerEEPROMWriteData(&lEEPROMio_st);

	if (lEEPROMErrorCode_i32 < 0) {
		return ERROR_HANDLER_PARAMETER_MANAGEMENT_EEPROM_WRITE_ERROR;
	} else {
		return NULL;
	}
}

/**
 * @brief	This function sends the response message
 *
 * @param	pResponse_pst	The response structure
 *
 * @return 	none
 */
static void parameterManagementSendResponse(PMResponse* pResponse_pst) {
	uint8_t lResponsePayload_pu8[8] = { 0 };

	ioHandlerCANSendMessageRaw(PARAMETER_MANAGEMENT_RESPONSE_MESSAGE_ID, lResponsePayload_pu8, 8);
}

/**
 * @brief	This function sends a negative response
 *
 * @param	pErrorCode_u32	The error code
 *
 * @return	none
 */
static void parameterManagementSendNegativeResponse(uint32_t pErrorCode_u32) {
	uint8_t lResponsePayload_pu8[8] = { 0 };
	uint16_t lChecksum_u16 = 0;

	ioHandlerCANSendMessageRaw(PARAMETER_MANAGEMENT_RESPONSE_MESSAGE_ID, lResponsePayload_pu8, 8);
}

/**
 * @brief	This function parses the request message
 *
 * @param	pPayload_pu8	The CAN message payload
 * @param	pRequest_pst	The request structure pointer
 *
 * @return	none
 */
static void parameterManagementGetRequest(uint8_t* pPayload_pu8, PMRequest* pRequest_pst) {

}


/**
 * @brief	This function parses the CAN messages that affect the parameters in EEPROM or in SRAM. Message id must be checked!
 *
 * @param	pReceivedMessage_pst	The received CAN message to be parsed
 *
 * @return	It returns 0 if the message is parsed
 */
int32_t parameterManagementCANMessageHandler(IOHandlerCANMessage* pReceivedMessage_pst) {
	PMRequest lRequest_st = { 0 };
	PMResponse lResponse_st = { 0 };
	IOHandlerEEPROMData lEEPROMData_st = { 0 };

	if(PARAMETER_MANAGEMENT_REQUEST_MESSAGE_ID != pReceivedMessage_pst->iID_u32) {
		return -1;
	}

	parameterManagementGetRequest(pReceivedMessage_pst->iData, &lRequest_st);

	if(ECU_CONFIGURATION_DEVICE_ID != lRequest_st.iDeviceID_u8) {
		// Device ID doesn't match
		return 0;
	}

	// Safety checks
	if(sgParameterBlockSize_u32 < ((lRequest_st.iParamID_u8 + 1) * 4)) {
		// Parameter ID is invalid
		parameterManagementSendNegativeResponse(PM_ERR_INVALID_PARAMID);
		return 0;
	}

	switch(lRequest_st.iRequestType_u8) {

	case PM_REQ_GETPARAM:
		lResponse_st.iResponseType_u8 = PM_RESP_GETPARAMRESP;
		memcpy(lResponse_st.iResponseData_pu8, &sgParameterBlock_pu32[lRequest_st.iParamID_u8], 4);
		parameterManagementSendResponse(&lResponse_st);
		break;

	case PM_REQ_SETPARAM:
		memcpy(&sgParameterBlock_pu32[lRequest_st.iParamID_u8], lRequest_st.iRequestData_pu8, 4);
		break;

	case PM_REQ_LOADPARAM:
		lEEPROMData_st.iAddress_u32 = PARAMETER_MANAGEMENT_PARAMETER_BLOCK_EEPROM_START_ADDRESS + (lRequest_st.iParamID_u8 * 4);
		lEEPROMData_st.iLength_u32 = 4;
		lEEPROMData_st.iData_pu32 = &sgParameterBlock_pu32[lRequest_st.iParamID_u8];
		ioHandlerEEPROMReadData(&lEEPROMData_st);
		break;

	case PM_REQ_SAVEPARAM:
		lEEPROMData_st.iAddress_u32 = PARAMETER_MANAGEMENT_PARAMETER_BLOCK_EEPROM_START_ADDRESS + (lRequest_st.iParamID_u8 * 4);
		lEEPROMData_st.iLength_u32 = 4;
		lEEPROMData_st.iData_pu32 = &sgParameterBlock_pu32[lRequest_st.iParamID_u8];
		ioHandlerEEPROMWriteData(&lEEPROMData_st);
		break;



	default:
		break;

	}

	/*
	switch(pReceivedMessage_pst->iID_u32) {

	case PM_MSG_ID_SAVEPARAM: {
		IOHandlerEEPROMData lEEPROMData_st = { 0 };
		uint32_t lParameterValue_u32 = *(sgParameterBlock_pu32 + );
		lEEPROMData_st.iAddress_u32 = PARAMETER_MANAGEMENT_PARAMETER_BLOCK_EEPROM_START_ADDRESS + lParamID_u8;
		lEEPROMData_st.iLength_u32 = 4;
		lEEPROMData_st.iData_pu32 = &lParameterValue_u32;
		ioHandlerEEPROMWriteData(&lEEPROMData_st);
		break;
	}

	case PM_MSG_ID_LOADPARAM: {
		IOHandlerEEPROMData lEEPROMData_st = { 0 };
		uint32_t lParameterValue_u32 = *(sgParameterBlock_pu32 + lParamID_u8);
		lEEPROMData_st.iAddress_u32 = PARAMETER_MANAGEMENT_PARAMETER_BLOCK_EEPROM_START_ADDRESS + lParamID_u8;
		lEEPROMData_st.iLength_u32 = 4;
		lEEPROMData_st.iData_pu32 = &lParameterValue_u32;
		ioHandlerEEPROMReadData(&lEEPROMData_st);
		*(sgParameterBlock_pu32 + lParamID_u8) = lParameterValue_u32;
		break;
	}

	case PM_MSG_ID_SETPARAM: {
		const uint16_t lCheckSum_u16 = lDeviceID_u8 + lParamID_u8;
		if(lCheckSum_u16 == pReceivedMessage_pst->iData[6] + pReceivedMessage_pst->iData[7]) {
			memcpy((sgParameterBlock_pu32 + lParamID_u8), &(pReceivedMessage_pst->iData[2]), 4);
		} else {
			// drop message
		}
		break;
	}

	case PM_MSG_ID_GETPARAM: {
		uint8_t lResponseData_pu8[6] = { 0 };
		uint32_t lParameterValue_u32 = *(sgParameterBlock_pu32 + (uint32_t) lParamID_u8);
		lResponseData_pu8[0] = ECU_CONFIGURATION_DEVICE_ID;
		lResponseData_pu8[1] = lParamID_u8;
		memcpy(&lResponseData_pu8[2], (uint8_t*) &lParameterValue_u32, 4);
		ioHandlerCANSendMessageRaw(PM_MSG_ID_GETPARAMRESP, lResponseData_pu8, 6);
		break;
	}

	}
	*/

	return 0;
}
