/**
 * @file	ParameterManagement.h
 * @authors	Gabor Kormendy
 * @date	2015.06.10.
 * @brief	This file contains function and variable declaration for
 * 			parameter management in the EEPROM.
 */

#ifndef DATAHANDLER_PARAMETERMANAGEMENT_PARAMETERMANAGEMENT_H_
#define DATAHANDLER_PARAMETERMANAGEMENT_PARAMETERMANAGEMENT_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "../../IOHandler/CAN/IOHandlerCAN.h"
#include "../../IOHandler/EEPROM/IOHandlerEEPROM.h"
#include "../../ErrorHandler/ErrorHandler.h"

#include "../../ECUConfiguration.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

/// The start address of the parameter block in EEPROM
#define PARAMETER_MANAGEMENT_PARAMETER_BLOCK_EEPROM_START_ADDRESS		0x00000000
/// The empty value of the EEPROM block
#define PARAMETER_MANAGEMENT_PARAMETER_BLOCK_EEPROM_EMPTY_VALUE			0xFFFFFFFF
/// The starting sequence of the parameter block in EEPROM
#define PARAMETER_MANAGEMENT_PARAMETER_BLOCK_END_SEQUENCE_0				0xABCDEF00
/// The ending sequence of the parameter block in EEPROM
#define PARAMETER_MANAGEMENT_PARAMETER_BLOCK_END_SEQUENCE_1				0x00FEDCBA

/// Helper macro to place the default parameter block into the FLASH .parameters section
#define PARAMETER_MANAGEMENT_PARAMETER_BLOCK_DEF_START()	\
	_Pragma("SET_DATA_SECTION(\".parameters\")");			\

/// Helper macro to restore the data section after defining the default parameter block
#define PARAMETER_MANAGEMENT_PARAMETER_BLOCK_DEF_END()		\
	_Pragma("SET_DATA_SECTION()");

/// Helper macro to place the runtime parameter block into the SRAM .runtime_parameters section
#define PARAMETER_MANAGEMENT_RUNTIME_PARAMETER_BLOCK_DEF_START()	\
	_Pragma("SET_DATA_SECTION(\".runtime_parameters\")");			\

/// Helper macro to restore the data section after defining the runtime parameter block
#define PARAMETER_MANAGEMENT_RUNTIME_PARAMETER_BLOCK_DEF_END()		\
	_Pragma("SET_DATA_SECTION()");

/// Helper macro to define the runtime parameter block
#define PARAMETER_MANAGEMENT_DEFINE_RUNTIME_PARAMETER_BLOCK(type, name)	\
	PARAMETER_MANAGEMENT_RUNTIME_PARAMETER_BLOCK_DEF_START()	\
	type name = { 0 };	\
	PARAMETER_MANAGEMENT_RUNTIME_PARAMETER_BLOCK_DEF_END()

#define PARAMETER_MANAGEMENT_REQUEST_MESSAGE_ID		0x700

#define PARAMETER_MANAGEMENT_RESPONSE_MESSAGE_ID	0x701

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

enum PMRequestType {
	PM_REQ_SETPARAM = 0,
	PM_REQ_GETPARAM,
	PM_REQ_SAVEPARAM,
	PM_REQ_LOADPARAM,
	NUM_PM_REQUESTS
};

enum PMResponseType {
	PM_RESP_GETPARAMRESP = 1
};

enum PMResponseError {
	PM_ERR_INVALID_PARAMID = 1,
	PM_ERR_INVALID_CHECKSUM
};

typedef struct PMRequest {
	/// The device ID
	uint8_t iDeviceID_u8;
	/// The request type
	uint8_t iRequestType_u8;
	/// The parameter id
	uint8_t iParamID_u8;
	/// The parameter value
	uint8_t iRequestData_pu8[4];
	/// The request checksum
	uint16_t iRequestChecksum_u16;
} PMRequest;

typedef struct PMResponse {
	/// The response command
	uint8_t iResponseType_u8;
	/// The response data
	uint8_t iResponseData_pu8[4];
} PMResponse;

// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------

int32_t parameterManagementInit(uint32_t* pDefaultParameterBlock_pu32, uint32_t* pRuntimeParameterBlock_pu32, uint32_t pParameterBlockSize_u32);
int32_t parameterManagementSave(void);
int32_t parameterManagementRead(uint32_t* pParameterBlock_pu32);
int32_t parameterManagementRestoreDefault(void);
bool parameterManagementIsEEPROMEmpty(IOHandlerEEPROMData* pEEPROMData_pst);

int32_t parameterManagementCANMessageHandler(IOHandlerCANMessage* pReceivedMessage_pst);


#endif /* DATAHANDLER_PARAMETERMANAGEMENT_PARAMETERMANAGEMENT_H_ */
