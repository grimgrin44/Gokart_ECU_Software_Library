/**
 * @file	DataHandlerCAN.h
 * @authors	Krisztian Enisz, Gabor Kormendy
 * @version	1.0
 * @brief	This file contains function and variable declaration for
 * 			the DataHandlerCAN module.
 */

#ifndef DATAHANDLER_CAN_DATAHANDLERPROCESSCANBUFFER_H_
#define DATAHANDLER_CAN_DATAHANDLERPROCESSCANBUFFER_H_

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------

#include "../../Application/Application.h"
#include "../../IOHandler/CAN/IOHandlerCAN.h"

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_ints.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"

#include "../../ECUConfiguration.h"

// ----------------------------------------------------------------------------
// Macro definitions.
// ----------------------------------------------------------------------------

/// Helper macro to create a parser list start
#define DATA_HANDLER_CAN_PARSER_LIST_START()	\
		const DataHandlerCANMessageParser gParserList_pst[] = {

/// Helper macro to create a parser entry
#define DATA_HANDLER_CAN_PARSER_LIST_ENTRY(msgId, parserFunction)	\
		{ msgId, parserFunction }

/// Helper macro to create a parser list end
#define DATA_HANDLER_CAN_PARSER_LIST_END()		\
		}; \
		const uint32_t gParserListSize_u32 = sizeof(gParserList_pst) / sizeof(gParserList_pst[0]);

// ----------------------------------------------------------------------------
// Type definitions.
// ----------------------------------------------------------------------------

/**
 * CAN data field parser function type
 */
typedef void (*CANMessageParser)(uint8_t* pRxData_pu8, uint8_t pDataLength_pu8);

/**
 * Structure for the ID filter in the data handler
 */
typedef struct DataHandlerCANMessageParser {
	/// The CAN message ID
	uint32_t iMsgID_u32;
	/// Function pointer to the message parser function
	CANMessageParser iMessageParser_pf;
} DataHandlerCANMessageParser;

// ----------------------------------------------------------------------------
// Variable declartions.
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
// Function declartions.
// ----------------------------------------------------------------------------

GOKART_API	void	dataHandlerInit(DataHandlerCANMessageParser* pParserArray_pst, uint32_t pParserArrayLength_u32);
GOKART_API	int32_t	dataHandlerCANMessageHandler(IOHandlerCANMessage* pCANMessage_pst);

#endif /* DATAHANDLER_CAN_DATAHANDLERPROCESSCANBUFFER_H_ */
