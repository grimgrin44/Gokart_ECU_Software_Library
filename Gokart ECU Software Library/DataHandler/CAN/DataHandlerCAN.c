/**
 * @file	DataHandlerCAN.c
 * @author	Krisztian Enisz, Gabor Kormendy
 * @version	1.0
 * @brief	This file contains function and variable definitions for the
 * 			DataHandlerCAN module. To use this module the MODULE_CAN_ENABLED
 * 			and the MODULE_DATA_HANDLER_CAN_ENABLED symbol must be defined.
 */

// ----------------------------------------------------------------------------
// Includes.
// ----------------------------------------------------------------------------
#include "DataHandlerCAN.h"

// ----------------------------------------------------------------------------
// Variable definitions.
// ----------------------------------------------------------------------------

/// The CAN message parser list pointer
static DataHandlerCANMessageParser* sgCANMessageParserList_pst = NULL;
/// The CAN message parser list length
static uint32_t sgCANMessageParserListLength_u32 = 0;


// ----------------------------------------------------------------------------
// Function definitions.
// ----------------------------------------------------------------------------

/**
 * @brief	This function sets up the parser list and registers the dataHandler CAN message handler.
 * @param	pParserArray_pst		Pointer to the parser array
 * @param	pParserArrayLength_u32	Length of the parser array
 * @return	none
 */
void dataHandlerInit(DataHandlerCANMessageParser* pParserArray_pst, uint32_t pParserArrayLength_u32) {
	__ASSERT(pParserArray_pst == NULL);

	sgCANMessageParserList_pst = pParserArray_pst;
	sgCANMessageParserListLength_u32 = pParserArrayLength_u32;

	ioHandlerCANAddMessageHandler(dataHandlerCANMessageHandler);
}

/**
 * @brief	Data handler CAN message handler function
 * @param	pCANMessage_pst		The CAN message object
 * @return	It returns 0 if the message is parsed
 */
int32_t	dataHandlerCANMessageHandler(IOHandlerCANMessage* pCANMessage_pst) {
	uint32_t lIndex_u32 = 0;
	for(lIndex_u32 = 0; lIndex_u32 < sgCANMessageParserListLength_u32; lIndex_u32++) {
		if(pCANMessage_pst->iID_u32 == sgCANMessageParserList_pst[lIndex_u32].iMsgID_u32) {
			sgCANMessageParserList_pst[lIndex_u32].iMessageParser_pf(pCANMessage_pst->iData, pCANMessage_pst->iDLC_u8);
			return 0;
		}
	}
	return -1;
}
