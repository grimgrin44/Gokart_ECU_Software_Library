/**
 * @file	ECUConfiguration.h
 * @author	Gabor Kormendy
 * @version	1.0
 * @brief	This is the main configuration file for every ECU. It must be modified according to the hardware and software requirements.
 */

#ifndef ECUCONFIGURATION_H_
#define ECUCONFIGURATION_H_

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_can.h"
#include "inc/hw_ints.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom_map.h"
#include "driverlib/gpio.h"
#include "driverlib/can.h"
#include "driverlib/interrupt.h"

#include "GlobalDefinitions.h"

/************************************************************************************************************************
 *
 * In this section, you can enable or disable assert
 *
 *************************************************************************************************************************/

/// Enables the debugging functions
#define ECU_CONFIGURATION_DEBUG_ON				TRUE

/************************************************************************************************************************
 *
 * In this section, you can configure node specific data
 *
 *************************************************************************************************************************/

/// Every device on the CAN bus has to have an unique device ID (1-254)
#define ECU_CONFIGURATION_DEVICE_ID				1

/************************************************************************************************************************
 *
 * In this section, you can configure system and clocks
 *
 *************************************************************************************************************************/

/// System clock configuration
#define ECU_CONFIGURATION_SYSTEM_CLOCK_PARAMETERS 			SYSCTL_SYSDIV_2_5|SYSCTL_USE_PLL|SYSCTL_OSC_MAIN|SYSCTL_XTAL_16MHZ
/// PWM module clock divider configuration
#define ECU_CONFIGURATION_SYSTEM_PWM_CLOCK_CONFIGURATION	SYSCTL_PWMDIV_32


/************************************************************************************************************************
 *
 * In this section, you can enable or disable sofware modules
 *
 *************************************************************************************************************************/

/// This enables the registration of the interupt handlers. If it's true, you don't have to place the interrupt handlers manually into the vector table.
#define ECU_CONFIGURATION_ENABLE_INT_HANDLER_REGISTRATION	TRUE

/// Enabling watchdog module prevents the software from freezing
#define ECU_CONFIGURATION_ENABLE_WATCHDOG					TRUE
/// CAN module enables sending and receiving messages on one CAN interface
#define ECU_CONFIGURATION_ENABLE_CAN						TRUE
/// UART module enables sending and receiving raw data on one UART interface
#define ECU_CONFIGURATION_ENABLE_UART						TRUE
/// DIO module enables digital input and output handling
#define ECU_CONFIGURATION_ENABLE_DIO						TRUE
/// SPI module enables the SSI module in 3 wire, master SPI mode
#define ECU_CONFIGURATION_ENABLE_SPI						TRUE
/// EEPROM module enables the internal EEPROM for data storage
#define ECU_CONFIGURATION_ENABLE_EEPROM						TRUE
/// PWM module enables the generation of PWM signals, and also used by the motor driver modules
#define ECU_CONFIGURATION_ENABLE_PWM						TRUE
/// Analog module enables the processing of analog signals
#define ECU_CONFIGURATION_ENABLE_ANALOG						FALSE

/// General Timer module enables small, low frequency tasks, that can be executed once or repeatedly
#define ECU_CONFIGURATION_ENABLE_GENERAL_TIMER				TRUE
/// Systick module contains a precision timer and the scheduler functions
#define ECU_CONFIGURATION_ENABLE_SYSTICK					TRUE
/// Motor driver module enables to driver stepper or DC motors with the appropriate driver [Dependency: SPI, DIO]
#define ECU_CONFIGURATION_ENABLE_MOTOR_DRIVER				FALSE
/// UDS module enables the diagnostic services [Dependency: CAN]
#define ECU_CONFIGURATION_ENABLE_UDS						FALSE
/// Remote Diagnostic Interface enables test and diagnostic functions over CAN [Dependency: CAN]
#define ECU_CONFIGURATION_ENABLE_RDI						TRUE

/// Enables the servo motor driver module
#define ECU_CONFIGURATION_ENABLE_MOTOR_DRIVER_SERVO			TRUE
/// Enables the DRV8711 motor driver module
#define ECU_CONFIGURATION_MOTOR_DRIVER_DRV8711				TRUE
/// Enables the BTS7960B motor driver module
#define ECU_CONFIGURATION_ENABLE_MOTOR_DRIVER_BTS7960B		TRUE



/************************************************************************************************************************
 *
 * In this section, you can configure the Watchdog module
 *
 *************************************************************************************************************************/

#if (ECU_CONFIGURATION_ENABLE_WATCHDOG == TRUE)

/// The hardware peripheral for the Watchdog module
#define ECU_CONFIGURATION_PERIPH_WATCHDOG				SYSCTL_PERIPH_WDOG0
/// The base address of the Watchdog module
#define ECU_CONFIGURATION_WATCHDOG_BASE					WATCHDOG0_BASE

#define ECU_CONFIGURATION_WATCHDOG_INTERRUPT			INT_WATCHDOG
/// Reload value of the Watchdog
#define ECU_CONFIGURATION_WATCHDOG_LOAD_VALUE			0xFEEFFEE
/// Enables the timeout logging into EEPROM for debug purpose
#define ECU_CONFIGURATION_WATCHDOG_LOG_EEPROM			TRUE
/// The base address of the log data in EEPROM
#define ECU_CONFIGURATION_WATCHDOG_LOG_EEPROM_ADDR		0x00000000
/// Enables the Watchdog clear in the scheduler
#define ECU_CONFIGURATION_WATCHDOG_SCHEDULER_CLEAR		TRUE
/// The Watchdog clear task id
#define ECU_CONFIGURATION_WATCHDOG_SCHEDULER_TASK_ID	0x000000FF
/// The Watchdog clear task scheduler mask
#define ECU_CONFIGURATION_WATCHDOG_SCHEDULER_MASK		0x001
/// Enables the debug mode (watchdog will stop with the CPU)
#define ECU_CONFIGURATION_WATCHDOG_DEBUG_MODE_ENABLED	TRUE
/// Enables the reset generation
#define ECU_CONFIGURATION_WATCHDOG_RESET_ENABLED		TRUE

// Check wether the EEPROM module enabled or not
#if (ECU_CONFIGURATION_WATCHDOG_LOG_EEPROM == TRUE && ECU_CONFIGURATION_ENABLE_EEPROM == FALSE)
#error EEPROM module has to be enabled in order to use the watchdog log function
#endif

#endif


/************************************************************************************************************************
 *
 * In this section, you can configure the General timer module
 *
 *************************************************************************************************************************/

#if (ECU_CONFIGURATION_ENABLE_GENERAL_TIMER == TRUE)

/// The timer peripheral
#define ECU_CONFIGURATION_TIMER_PERIPHERAL		SYSCTL_PERIPH_TIMER0
/// The base address of the timer
#define ECU_CONFIGURATION_TIMER_BASE			TIMER0_BASE
/// The timer submodule base address
#define ECU_CONFIGURATION_TIMER_SUBMODULE_BASE	TIMER_A
/// The interrupt of the timer
#define ECU_CONFIGURATION_TIMER_INTERRUPT		INT_TIMER0A
/// The divider of the system clock
#define ECU_CONFIGURATION_TIMER_DIVIDER			2000
/// The maximum number of active timer tasks
#define ECU_CONFIGURATION_TIMER_MAX_TASK_COUNT	16

#endif


/************************************************************************************************************************
 *
 * In this section, you can configure the EEPROM module
 *
 *************************************************************************************************************************/

#if (ECU_CONFIGURATION_ENABLE_EEPROM == TRUE)

/// The hardware peripheral for the EEPROM module
#define ECU_CONFIGURATION_PERIHP_EEPROM					SYSCTL_PERIPH_EEPROM0
/// The size of the EEPROM
#define ECU_CONFIGURATION_EEPROM_SIZE					0


#endif

/************************************************************************************************************************
 *
 * In this section, you can configure the CAN interface
 *
 *************************************************************************************************************************/

#if (ECU_CONFIGURATION_ENABLE_CAN == TRUE)

/// The hardware peripherial for the CAN port.
#define ECU_CONFIGURATION_PERIPH_CAN				SYSCTL_PERIPH_CAN0
/// The GPIO port for the CAN TX port.
#define ECU_CONFIGURATION_PERIPH_GPIO_CAN_RX		SYSCTL_PERIPH_GPIOE
/// The GPIO port for the CAN RX port
#define ECU_CONFIGURATION_PERIPH_GPIO_CAN_TX		SYSCTL_PERIPH_GPIOF
/// The GPIO peripherial settings for the CAN port function multiplexing (RX).
#define ECU_CONFIGURATION_GPIO_CANRX				GPIO_PE4_CAN0RX
/// The GPIO peripherial settings for the CAN port function multiplexing (TX).
#define ECU_CONFIGURATION_GPIO_CANTX				GPIO_PF3_CAN0TX
/// The GPIO base port for the CAN RX.
#define ECU_CONFIGURATION_GPIO_PORT_BASE_CAN_RX		GPIO_PORTE_BASE
/// The GPIO base port for the CAN TX.
#define ECU_CONFIGURATION_GPIO_PORT_BASE_CAN_TX 	GPIO_PORTF_BASE
/// The GPIO port pins for the CAN (RX).
#define	ECU_CONFIGURATION_GPIO_PIN_RX_CAN			GPIO_PIN_4
/// The GPIO port pins for the CAN (TX).
#define ECU_CONFIGURATION_GPIO_PIN_TX_CAN			GPIO_PIN_3
/// The base for the CAN.
#define ECU_CONFIGURATION_CAN_BASE					CAN0_BASE
/// The NVIC interrupt for the CAN interface.
#define ECU_CONFIGURATION_INT_CAN					INT_CAN0
/// The baudrate for the CAN interface (RX and TX).
#define ECU_CONFIGURATION_CAN_BITRATE				500000
/// The CAN message filter mask for the received frames.
#define ECU_CONFIGURATION_CAN_RX_MSG_FILTER			0xFFFF
/// Tha maximum number of messages that the CAN controller can handle
#define ECU_CONFIGURATION_CAN_MAX_MESSAGE_OBJ		32

/// The IDs of the received CAN messages (the count of the messages can not exceed 16).
#define ECU_CONFIGURATION_CAN_RX_MSG_IDS			{0700, 0x702, 0x270, 0x260, 0x261, 0x262, 0x263, 0x2B0, 0x60 }
/// The DLC of the received CAN messages (the count of the messages can not exceed 16). It must be in sync with the IDs above.
#define ECU_CONFIGURATION_CAN_RX_MSG_DLC			{8, 8, 1, 5, 5, 5, 5, 5, 8}
/// The mask of the received CAN messages. It must be in sync with the IDs above
#define ECU_CONFIGURATION_CAN_RX_MSG_FILTERS		{0xFFFF, 0xFFFF, 0xFFFF}

/// The size of the CAN RX buffer
#define ECU_CONFIGURATION_CAN_RX_BUFFER_SIZE		32
/// The size of the CAN TX buffer
#define ECU_CONFIGURATION_CAN_TX_BUFFER_SIZE		32

#endif //(ECU_CONFIGURATION_ENABLE_CAN == TRUE)


/************************************************************************************************************************
 *
 * In this section, you can configure the UART interface
 *
 *************************************************************************************************************************/

#if (ECU_CONFIGURATION_ENABLE_UART == TRUE)

/// The UART peripheral.
#define ECU_CONFIGURATION_PERIPH_UART				SYSCTL_PERIPH_UART0
/// The GPIO peripheral for UART RX.
#define ECU_CONFIGURATION_PERIPH_GPIO_UART_RX		SYSCTL_PERIPH_GPIOA
/// The GPIO peripheral for UART TX.
#define ECU_CONFIGURATION_PERIPH_GPIO_UART_TX		SYSCTL_PERIPH_GPIOA
/// The GPIO peripherial settings for the UART port function multiplexing (RX).
#define ECU_CONFIGURATION_GPIO_UARTRX				GPIO_PA0_U0RX
/// The GPIO peripherial settings for the UART port function multiplexing (TX).
#define ECU_CONFIGURATION_GPIO_UARTTX				GPIO_PA1_U0TX
/// The base address of the UART RX GPIO.
#define ECU_CONFIGURATION_GPIO_PORT_BASE_UART_RX	GPIO_PORTA_BASE
/// The base address of the UART RX GPIO.
#define ECU_CONFIGURATION_GPIO_PORT_BASE_UART_TX	GPIO_PORTA_BASE
/// The UART RX pin number
#define ECU_CONFIGURATION_GPIO_PIN_UART_RX			GPIO_PIN_0
/// The UART RX pin number
#define ECU_CONFIGURATION_GPIO_PIN_UART_TX			GPIO_PIN_1
/// The UART module base address
#define ECU_CONFIGURATION_UART_BASE					UART0_BASE
/// The UART module interrupt
#define ECU_CONFIGURATION_INT_UART					INT_UART0
/// The baudrate of the UART peripheral
#define ECU_CONFIGURATION_UART_BITRATE				115200
/// The data length on the bus
#define ECU_CONFIGURATION_UART_DATA_LENGTH			UART_CONFIG_WLEN_8
/// The parity configuration
#define ECU_CONFIGURATION_UART_PARITY				UART_CONFIG_PAR_NONE
/// The number of stop bits
#define ECU_CONFIGURATION_UART_STOP_BITS			UART_CONFIG_STOP_ONE
/// The receive buffer size in bytes
#define ECU_CONFIGURATION_UART_RX_BUFFER_SIZE		128
/// The transmit buffer size in bytes
#define ECU_CONFIGURATION_UART_TX_BUFFER_SIZE		256
/// Enables the standard terminal mode (PuTTY)
#define ECU_CONFIGURATION_UART_TERMINAL_MODE		TRUE

#endif //(ECU_CONFIGURATION_ENABLE_UART == TRUE)

/************************************************************************************************************************
 *
 * In this section, you can configure the DIO module
 *
 *************************************************************************************************************************/

#if (ECU_CONFIGURATION_ENABLE_DIO == TRUE)

/// This enables the isolated input 0
#define ECU_CONFIGURATION_DIO_ENABLE_INPUT_0			TRUE
/// This enables the isolated input 1
#define ECU_CONFIGURATION_DIO_ENABLE_INPUT_1			TRUE
/// This enables the isolated input 2
#define ECU_CONFIGURATION_DIO_ENABLE_INPUT_2			TRUE
/// This enables the isolated input 3
#define ECU_CONFIGURATION_DIO_ENABLE_INPUT_3			TRUE

/// This enables the isolated output 0
#define ECU_CONFIGURATION_DIO_ENABLE_OUTPUT_0			TRUE
/// This enables the isolated output 1
#define ECU_CONFIGURATION_DIO_ENABLE_OUTPUT_1			TRUE
/// This enables the isolated output 2
#define ECU_CONFIGURATION_DIO_ENABLE_OUTPUT_2			TRUE
/// This enables the isolated output 3
#define ECU_CONFIGURATION_DIO_ENABLE_OUTPUT_3			TRUE

#endif

/************************************************************************************************************************
 *
 * In this section, you can configure the SPI interface
 *
 *************************************************************************************************************************/

#if (ECU_CONFIGURATION_ENABLE_SPI == TRUE)

/// Base address of the SPI module
#define ECU_CONFIGURATION_SPI_BASE					SSI2_BASE
/// Peripheral base of the SPI module
#define ECU_CONFIGURATION_SPI_PERIPHERAL_BASE		SYSCTL_PERIPH_SSI2
/// SPI receive pin port peripheral
#define ECU_CONFIGURATION_SPI_PERIPH_GPIO_RX		SYSCTL_PERIPH_GPIOB
/// SPI transmit pin port peripheral
#define ECU_CONFIGURATION_SPI_PERIPH_GPIO_TX		SYSCTL_PERIPH_GPIOB
/// SPI clock pin port peripheral
#define ECU_CONFIGURATION_SPI_PERIPH_GPIO_SCK		SYSCTL_PERIPH_GPIOB
/// SPI receive pin port base
#define ECU_CONFIGURATION_SPI_GPIO_PORT_BASE_RX		GPIO_PORTB_BASE
/// SPI transmit pin port base
#define ECU_CONFIGURATION_SPI_GPIO_PORT_BASE_TX		GPIO_PORTB_BASE
/// SPI clock pin port base
#define ECU_CONFIGURATION_SPI_GPIO_PORT_BASE_SCK	GPIO_PORTB_BASE
/// SPI receive pin port config
#define ECU_CONFIGURATION_SPI_GPIO_RX_CONFIG		GPIO_PB6_SSI2RX
/// SPI transmit pin port config
#define ECU_CONFIGURATION_SPI_GPIO_TX_CONFIG		GPIO_PB7_SSI2TX
/// SPI clock pin port config
#define ECU_CONFIGURATION_SPI_GPIO_SCK_CONFIG		GPIO_PB4_SSI2CLK
/// SPI receive pin number
#define ECU_CONFIGURATION_SPI_GPIO_PIN_RX			GPIO_PIN_6
/// SPI transmit pin number
#define ECU_CONFIGURATION_SPI_GPIO_PIN_TX			GPIO_PIN_7
/// SPI clock pin number
#define ECU_CONFIGURATION_SPI_GPIO_PIN_SCK			GPIO_PIN_4
/// SPI interrupt
#define ECU_CONFIGURATION_SPI_INTERRUPT				INT_SSI2
/// SPI bitrate
#define ECU_CONFIGURATION_SPI_BITRATE				1000000
/// SPI mode
#define ECU_CONFIGURATION_SPI_MODE					SSI_MODE_MASTER
/// SPI protocol
#define ECU_CONFIGURATION_SPI_PROTOCOL				SSI_FRF_MOTO_MODE_0
/// SPI data width
#define ECU_CONFIGURATION_SPI_DATA_WIDTH			8


#endif //(ECU_CONFIGURATION_ENABLE_SPI == TRUE)

/************************************************************************************************************************
 *
 * In this section, you can configure the PWM module
 *
 *************************************************************************************************************************/

#if (ECU_CONFIGURATION_ENABLE_PWM == TRUE)

/// Enable PWM module 0 generator 0
#define ECU_CONFIGURATION_PWM_MODULE_0_PWM_GENERATOR_0_ENABLE			FALSE
/// Enable PWM module 0 generator 1
#define ECU_CONFIGURATION_PWM_MODULE_0_PWM_GENERATOR_1_ENABLE			FALSE
/// Enable PWM module 0 generator 2
#define ECU_CONFIGURATION_PWM_MODULE_0_PWM_GENERATOR_2_ENABLE			FALSE
/// Enable PWM module 0 generator 3
#define ECU_CONFIGURATION_PWM_MODULE_0_PWM_GENERATOR_3_ENABLE			TRUE
/// Enable PWM module 1 generator 0
#define ECU_CONFIGURATION_PWM_MODULE_1_PWM_GENERATOR_0_ENABLE			FALSE
/// Enable PWM module 1 generator 1
#define ECU_CONFIGURATION_PWM_MODULE_1_PWM_GENERATOR_1_ENABLE			FALSE
/// Enable PWM module 1 generator 2
#define ECU_CONFIGURATION_PWM_MODULE_1_PWM_GENERATOR_2_ENABLE			FALSE
/// Enable PWM module 1 generator 3
#define ECU_CONFIGURATION_PWM_MODULE_1_PWM_GENERATOR_3_ENABLE			FALSE
/// Use PD0 as M0PWM6 output
#define ECU_CONFIGURATION_PWM_MODULE_0_OUTPUT_6_ALTERNATE				FALSE
/// Use PD1 as M0PWM7 output
#define ECU_CONFIGURATION_PWM_MODULE_0_OUTPUT_7_ALTERNATE				FALSE
/// Use PE4 as M1PWM2 output
#define ECU_CONFIGURATION_PWM_MODULE_1_OUTPUT_2_ALTERNATE				FALSE
/// Use PE5 as M1PWM2 output
#define ECU_CONFIGURATION_PWM_MODULE_1_OUTPUT_3_ALTERNATE				FALSE

#endif


/************************************************************************************************************************
 *
 * In this section, you can configure the Analog module
 *
 *************************************************************************************************************************/

#if (ECU_CONFIGURATION_ENABLE_ANALOG == TRUE)


#define ECU_CONFIGURATION_ANALOG_ADC_MODULE_PERIPHERAL_BASE		SYSCTL_PERIPH_ADC0

#define ECU_CONFIGURATION_ANALOG_ADC_MODULE_BASE				ADC0_BASE


#define ECU_CONFIGURATION_ANALOG_INPUT_1_ENABLE					TRUE

#define ECU_CONFIGURATION_ANALOG_INPUT_2_ENABLE					TRUE

#define ECU_CONFIGURATION_ANALOG_INPUT_3_ENABLE					TRUE

#define ECU_CONFIGURATION_ANALOG_TEMP_SENSOR_ENABLE				TRUE


#define ECU_CONFIGURATION_ANALOG_INPUT_1_PORT_PERIPH_BASE		SYSCTL_PERIPH_GPIOE

#define ECU_CONFIGURATION_ANALOG_INPUT_1_PORT_BASE				GPIO_PORTE_BASE

#define ECU_CONFIGURATION_ANALOG_INPUT_1_PIN					GPIO_PIN_1

#define ECU_CONFIGURATION_ANALOG_INPUT_1_ADC_CHANNEL			ADC_CTL_CH2


#define ECU_CONFIGURATION_ANALOG_INPUT_2_PORT_PERIPH_BASE		SYSCTL_PERIPH_GPIOE

#define ECU_CONFIGURATION_ANALOG_INPUT_2_PORT_BASE				GPIO_PORTE_BASE

#define ECU_CONFIGURATION_ANALOG_INPUT_2_PIN					GPIO_PIN_2

#define ECU_CONFIGURATION_ANALOG_INPUT_2_ADC_CHANNEL			ADC_CTL_CH1


#define ECU_CONFIGURATION_ANALOG_INPUT_3_PORT_PERIPH_BASE		SYSCTL_PERIPH_GPIOE

#define ECU_CONFIGURATION_ANALOG_INPUT_3_PORT_BASE				GPIO_PORTE_BASE

#define ECU_CONFIGURATION_ANALOG_INPUT_3_PIN					GPIO_PIN_3

#define ECU_CONFIGURATION_ANALOG_INPUT_3_ADC_CHANNEL			ADC_CTL_CH0


#endif

/************************************************************************************************************************
 *
 * In this section, you can configure the UDS module
 *
 *************************************************************************************************************************/

#if (ECU_CONFIGURATION_ENABLE_UDS == TRUE)



#endif

/************************************************************************************************************************
 *
 * In this section, you can configure the servo motor driver module
 *
 *************************************************************************************************************************/

#if (ECU_CONFIGURATION_ENABLE_MOTOR_DRIVER_SERVO == TRUE)

/// Peripheral address of the servo PWM module
#define ECU_CONFIGURATION_SERVO_PWM_MODULE_PERIPHERAL_BASE			SYSCTL_PERIPH_PWM0
/// Base address of the servo PWM module
#define ECU_CONFIGURATION_SERVO_PWM_MODULE_BASE						PWM0_BASE
/// Servo PWM generator block
#define ECU_CONFIGURATION_SERVO_PWM_MODULE_PWM_GEN					PWM_GEN_3
/// Servo PWM frequency in Hz
#define ECU_CONFIGURATION_SERVO_PWM_PERIOD_IN_HZ					50
/// Servo PWM min pulse width in us
#define ECU_CONFIGURATION_SERVO_PWM_MIN_PULSE_WIDTH_IN_US			600
/// Servo PWM max pulse width in us
#define ECU_CONFIGURATION_SERVO_PWM_MAX_PULSE_WIDTH_IN_US			2400
/// Use PD0 as M0PWM6 output
#define ECU_CONFIGURATION_SERVO_PWM_MODULE_PWM0_OUT6_ALTERNATE		FALSE
/// Use PD1 as M0PWM7 output
#define ECU_CONFIGURATION_SERVO_PWM_MODULE_PWM0_OUT7_ALTERNATE		FALSE
/// Use PE4 as M1PWM2 output
#define ECU_CONFIGURATION_SERVO_PWM_MODULE_PWM1_OUT2_ALTERNATE		FALSE
/// Use PE5 as M1PWM2 output
#define ECU_CONFIGURATION_SERVO_PWM_MODULE_PWM1_OUT3_ALTERNATE		FALSE

#endif


/************************************************************************************************************************
 *
 * In this section, you can configure the motor driver module based on the DRV8711 driver
 *
 *************************************************************************************************************************/

#if (ECU_CONFIGURATION_MOTOR_DRIVER_DRV8711 == TRUE)

#define ECU_CONFIGURATION_MOTOR_DRIVER_DRV8711_ENABLE_STATUS_INTERRUPT			FALSE

#define ECU_CONFIGURATION_MOTOR_DRIVER_DRV8711_SENSE_RESISTOR_VALUE_IN_OHM		0.1

#endif

/************************************************************************************************************************
 *
 * In this section, you can configure the BTS7960B motor driver module
 *
 *************************************************************************************************************************/

#if (ECU_CONFIGURATION_ENABLE_MOTOR_DRIVER_BTS7960B == TRUE)

#define ECU_CONFIGURATION_BTS7960B_PWM_PERIOD_IN_HZ						20000

#define ECU_CONFIGURATION_BTS7960B_ENABLE_FORWARD_PIN_PERIPHERAL_BASE	SYSCTL_PERIPH_GPIOF

#define ECU_CONFIGURATION_BTS7960B_ENABLE_FORWARD_PIN_PORT				GPIO_PORTF_BASE

#define ECU_CONFIGURATION_BTS7960B_ENABLE_FORWARD_PIN					GPIO_PIN_1

#define ECU_CONFIGURATION_BTS7960B_ENABLE_REVERSE_PIN_PERIPHERAL_BASE	SYSCTL_PERIPH_GPIOF

#define ECU_CONFIGURATION_BTS7960B_ENABLE_REVERSE_PIN_PORT				GPIO_PORTF_BASE

#define ECU_CONFIGURATION_BTS7960B_ENABLE_REVERSE_PIN					GPIO_PIN_2

#define ECU_CONFIGURATION_BTS7960B_FORWARD_DRIVE_CURRENT_ALARM_PORT		GPIO_PORTD_BASE

#define ECU_CONFIGURATION_BTS7960B_FORWARD_DRIVE_CURRENT_ALARM_PIN		GPIO_PIN_6

#define ECU_CONFIGURATION_BTS7960B_REVERSE_DRIVE_CURRENT_ALARM_PORT		GPIO_PORTD_BASE

#define ECU_CONFIGURATION_BTS7960B_REVERSE_DRIVE_CURRENT_ALARM_PIN		GPIO_PIN_7

#endif

/************************************************************************************************************************
 *
 * DO NOT EDIT THIS SECTION!
 *
 *************************************************************************************************************************/

#if (ECU_CONFIGURATION_DEBUG_ON == TRUE)
#define	__ASSERT(x)	___ASSERT(x)
#else
#define __ASSERT(x)
#endif

#endif /* ECUCONFIGURATION_H_ */
